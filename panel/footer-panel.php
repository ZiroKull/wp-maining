    
    </div>	
	<?php wp_footer(); ?>
	
	<script type="text/javascript">
        $(document).ready(function () {
            $("#sha_amount, #dash_amount, #zcash_amount, #scrypt_amount, #ether_amount").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            }).keyup(function (e) {
                var slider = $("#slider-" + contractType);
                var id = $(this).attr("id");
                var contractType = id.split("_")[0];
        
                var value = parseInt($(this).val());
                var sliderMax = $("#slider-" + contractType).data("max");
                var sliderMin = $("#slider-" + contractType).data("step");
                if (isNaN(value)) {
                    value = 0;
                }
                
                value = Math.round(value / sliderMin) * sliderMin;
                if (value > sliderMax * 10000) {
                    value = sliderMax * 10000;
                } 
                
                if (value > $("#slider-" + contractType).data("step")) {
                   if (value !== $(this).val()) {
                        $(this).val(value)
                    }
                }
                return false;
        
                
                if (value < sliderMax && value >= sliderMin) { 
                    $("#slider-" + contractType).val(value);
                } else {
                    var price = parseFloat(Math.round(value) * $("#" + $("#slider-" + contractType).data("type") + "price").val()).toFixed(2);
                    $(".slider-" + contractType + "-usd").text(price);
                    $(".slider-" + contractType + "-btc").text((parseFloat(price) * parseFloat($("#usdprice").val())).toFixed(8));
                    $(".slider-" + contractType + "-total").val(Math.round(value));
                } 
            });
        
        	$("input[name=\"year_contract\"]").change(function() {
        		$("#product_years").val($(this).val());
        		if ($(this).val() == 3) {
        			$("#slider-sha").data("type", "sha3y");
        		} else {
        			$("#slider-sha").data("type", "sha");
        		}
        		$("#slider-sha").val($("#slider-sha").val());
        	})
        	
        	$("#reinvest-save").click(function(event) {
        	    event.preventDefault();
        
        	    var self = $(this);
        
        	    self.button("loading");
        
        	    var data = $("#reinvest-form").serialize();
        
        	    $.post("/panel/reinvest", data, function(response) {
        	        self.button("reset");
        
        	        document.location.reload();
        	    }, "json");
        	});
        
        	function labelFormatter(label, series) {
        		return "<div class='badge badge-white'>" + label + "</div>";
        	}
        
            function tooltipper(label, xval, yval, flotItem) {
        		return (yval.toFixed(8).replace(/([0-9]+(\.[0-9]+[1-9])?)(\.?0+$)/,"$1")) + " %s " + "  %x";
        	}
        
            var balanceoptions = {
                xaxis: {
                    mode: "time",
                    tickSize: [1, "day"],
                    tickLength: 0,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: "Arial",
                    axisLabelPadding: 10,
                    color: "#d5d5d5",
                    timeformat: "%d.%m"
                },
                yaxes: [
        	        {
        	            position: "left",
        	            //max: 1070,
        	            color: "#f5f5f5",
        	            axisLabelUseCanvas: true,
        	            axisLabelFontSizePixels: 12,
        	            axisLabelFontFamily: "Arial",
        	            axisLabelPadding: 3
        	        },
        	        {
        	            position: "right",
        	            color: "#f5f5f5",
        	            axisLabelUseCanvas: true,
        	            axisLabelFontSizePixels: 12,
        	            axisLabelFontFamily: "Arial",
        	            axisLabelPadding: 67
        	        }
                ],
        //        legend: {
        //            noColumns: 2,
        //            labelBoxBorderColor: "#000000",
        //            position: "nw"
        //        },
                legend: false,
                grid: {
                    hoverable: true,
                    borderWidth: 0
                },
                tooltip: true,
        	    tooltipOpts: {
        			content: tooltipper,
        			xDateFormat: "%d.%m.%y"
        	    }
            };
            var pooloptions = {
                series: {
        	        pie: {
        	            show: true,
        	            //radius: 1/2,
        	            radius: 1,
        	            label: {
        	                show: false,
        	                radius: 1,
        	                formatter: labelFormatter,
        	                threshold: 0.1
        	            },
        	            stroke: {
        	                width: 2
                        },
                        shadow: {
                            top: 5
                        }
        	        }
        	    },
        	    legend: {
        	        show: true
        	    },
        	    grid: {
                    hoverable: true
        	    },
                tooltip: true,
                tooltipOpts: {
                    content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                    shifts: {
                        x: 20,
                        y: 0
                    },
                    defaultTheme: true
                }
            };
            var profitoptions = {
                xaxis: {
                    mode: "time",
                    tickSize: [1, "day"],
                    tickLength: 0,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: "Arial",
                    axisLabelPadding: 10,
                    color: "#d5d5d5",
                    timeformat: "%d.%m"
                },
                yaxes: [
        	        {
        	            position: "left",
        	            color: "#f5f5f5",
        	            axisLabelUseCanvas: true,
        	            axisLabelFontSizePixels: 12,
        	            axisLabelFontFamily: "Arial",
        	            axisLabelPadding: 3
        	        }
                ],
                legend: false,
                grid: {
                    hoverable: true,
                    borderWidth: 0
                },
                tooltip: true,
        	    tooltipOpts: {
        			content: tooltipper,
        			xDateFormat: "%d.%m.%y"
        	    }
            };
        
        	var payouts = [];
        	var ETH_payouts = [];
        	var DASH_payouts = [];
        	var ZEC_payouts = [];
            var balances = [];
            var ETH_balances = [];
            var DASH_balances = [];
            var ZEC_balances = [];
        
        	var balancedata = [
                {
                    label: "BTC Выплата&nbsp;",
                    data: payouts,
                    color: "#84defb",
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 24 * 60 * 60 * 600,
                        lineWidth: 0,
                        fill: 1.0,
                        fillColor: "rgba(199,237,252,0.5)"
                    },
        			yaxis: 1,
        			highlightColor: "rgba(199,237,252,0.5)"
                }, {
                    label: "BTC Баланс",
                    data: balances,
                    yaxis: 2,
                    color: "#5A93c4",
                    lines: {
                        lineWidth: 2,
                        show: true,
                        fill: false,
                        fillColor: {
                            colors: [{
                                opacity: 0.2
                            }, {
                                opacity: 0
                            }]
                        }
                    },
                    splines: {
                        show: false,
                        tension: 0.6,
                        lineWidth: 1,
                        fill: 0.1
                    }
                }
            ];
        
        	var ETH_balancedata = [
                {
                    label: "ETH Выплата&nbsp;",
                    data: ETH_payouts,
                    color: "#84defb",
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 24 * 60 * 60 * 600,
                        lineWidth: 0,
                        fill: 1.0,
                        fillColor: "rgba(199,237,252,0.5)"
                    },
        			yaxis: 1,
        			highlightColor: "rgba(199,237,252,0.5)"
                }, {
                    label: "ETH Баланс",
                    data: ETH_balances,
                    yaxis: 2,
                    color: "#5A93c4",
                    lines: {
                        lineWidth: 2,
                        show: true,
                        fill: false,
                        fillColor: {
                            colors: [{
                                opacity: 0.2
                            }, {
                                opacity: 0
                            }]
                        }
                    },
                    splines: {
                        show: false,
                        tension: 0.6,
                        lineWidth: 1,
                        fill: 0.1
                    }
                }
            ];
        
        	var DASH_balancedata = [
                {
                    label: "DASH Выплата&nbsp;",
                    data: DASH_payouts,
                    color: "#84defb",
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 24 * 60 * 60 * 600,
                        lineWidth: 0,
                        fill: 1.0,
                        fillColor: "rgba(199,237,252,0.5)"
                    },
        			yaxis: 1,
        			highlightColor: "rgba(199,237,252,0.5)"
                }, {
                    label: "DASH Баланс",
                    data: DASH_balances,
                    yaxis: 2,
                    color: "#5A93c4",
                    lines: {
                        lineWidth: 2,
                        show: true,
                        fill: false,
                        fillColor: {
                            colors: [{
                                opacity: 0.2
                            }, {
                                opacity: 0
                            }]
                        }
                    },
                    splines: {
                        show: false,
                        tension: 0.6,
                        lineWidth: 1,
                        fill: 0.1
                    }
                }
            ];
        
        	var ZEC_balancedata = [
                {
                    label: "ZEC Выплата&nbsp;",
                    data: ZEC_payouts,
                    color: "#84defb",
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 24 * 60 * 60 * 600,
                        lineWidth: 0,
                        fill: 1.0,
                        fillColor: "rgba(199,237,252,0.5)"
                    },
        			yaxis: 1,
        			highlightColor: "rgba(199,237,252,0.5)"
                }, {
                    label: "ZEC Баланс",
                    data: ZEC_balances,
                    yaxis: 2,
                    color: "#5A93c4",
                    lines: {
                        lineWidth: 2,
                        show: true,
                        fill: false,
                        fillColor: {
                            colors: [{
                                opacity: 0.2
                            }, {
                                opacity: 0
                            }]
                        }
                    },
                    splines: {
                        show: false,
                        tension: 0.6,
                        lineWidth: 1,
                        fill: 0.1
                    }
                }
            ];
        
            $.plot($("#flot-balance"), balancedata, balanceoptions);
            if ($("#flot-eth-balance").length) $.plot($("#flot-eth-balance"), ETH_balancedata, balanceoptions);
            if ($("#flot-dash-balance").length) $.plot($("#flot-dash-balance"), DASH_balancedata, balanceoptions);
            if ($("#flot-zec-balance").length) $.plot($("#flot-zec-balance"), ZEC_balancedata, balanceoptions);
        
        	var shapools = [{data: [34], label: 'BW', color: '#6cb8f7'}, {data: [33], label: 'SL', color: '#A0F3BB'}, {data: [33], label: 'BC', color: '#9ad1ed'}];
            if ($("#flot-pools-sha").length) $.plot($("#flot-pools-sha"), shapools, pooloptions);
        	var scryptpools = [{data: [38], label: 'F2P', color: '#6cb8f7'}, {data: [29], label: 'LP', color: '#A0F3BB'}, {data: [33], label: 'PH', color: '#9ad1ed'}];
            if ($("#flot-pools-scrypt").length) $.plot($("#flot-pools-scrypt"), scryptpools, pooloptions);
        	var etherpools = [{data: [50], label: 'EP', color: '#6cb8f7'}, {data: [50], label: 'DP', color: '#A0F3BB'}];
            if ($("#flot-pools-ether").length) $.plot($("#flot-pools-ether"), etherpools, pooloptions);
        	var dashpools = [{data: [100], label: 'CM', color: '#6cb8f7'}];
            if ($("#flot-pools-dash").length) $.plot($("#flot-pools-dash"), dashpools, pooloptions);
        	var zcashpools = [{data: [100], label: 'FP', color: '#6cb8f7'}];
            if ($("#flot-pools-zcash").length) $.plot($("#flot-pools-zcash"), zcashpools, pooloptions);
            $(".form-pools.sha").each(function(){
                $(this).poolsAndSliders({
                    pools: [{"id":"5","title":"BW.com"},{"id":"6","title":"Slush"},{"id":"7","title":"BTCchina"},{"id":"8","title":"F2pool"},{"id":"18","title":"Antpool"}],
                    set: [{"id":"5","percentage":"34"},{"id":"6","percentage":"33"},{"id":"7","percentage":"33"}],
                    custom: {
                        type: "sha"
                    },
                    url: "/panel",
                    success: function(data) {
                        var result = jQuery.parseJSON(data);
                        if (result.status == "error") alert(result.reason);
                        else location.reload();
                    }
                });
            });
        
            $(".form-pools.scrypt").each(function(){
                $(this).poolsAndSliders({
                    pools: [{"id":"10","title":"F2Pool"},{"id":"14","title":"Litecoinpool"},{"id":"25","title":"Prohashing"},{"id":"26","title":"Antpool"},{"id":"27","title":"ViaLTC"}],
                    set: [{"id":"10","percentage":"38"},{"id":"14","percentage":"29"},{"id":"25","percentage":"33"}],
                    custom: {
                        type: "scrypt"
                    },
                    url: "/panel",
                    success: function(data) {
                        var result = jQuery.parseJSON(data);
                        if (result.status == "error") alert(result.reason);
                        else location.reload();
                    }
                });
            });
        
            $(".form-pools.ether").each(function(){
                $(this).poolsAndSliders({
                    pools: [{"id":"19","title":"EthereumPool.co"},{"id":"20","title":"Dwarfpool.com"}],
                    set: [{"id":"19","percentage":"50"},{"id":"20","percentage":"50"}],
                    custom: {
                        type: "ether"
                    },
                    url: "/panel",
                    success: function(data) {
                        var result = jQuery.parseJSON(data);
                        if (result.status == "error") alert(result.reason);
                        else location.reload();
                    }, maxPools: 2
                });
            });
        
            $(".form-pools.dash").each(function(){
                $(this).poolsAndSliders({
                    pools: [{"id":"23","title":"Coinmine.pl"}],
                    set: [{"id":"23","percentage":"100"}],
                    custom: {
                        type: "dash"
                    },
                    url: "/panel",
                    success: function(data) {
                        var result = jQuery.parseJSON(data);
                        if (result.status == "error") alert(result.reason);
                        else location.reload();
                    }
                });
            });
        
            $(".form-pools.zcash").each(function(){
                $(this).poolsAndSliders({
                    pools: [{"id":"24","title":"FlyPool"}],
                    set: [{"id":"24","percentage":"100"}],
                    custom: {
                        type: "zcash"
                    },
                    url: "/panel",
                    success: function(data) {
                        var result = jQuery.parseJSON(data);
                        if (result.status == "error") alert(result.reason);
                        else location.reload();
                    }
                });
            });
        });
    </script>
    
    <script type="text/javascript">
    	$(document).ready(function(){
    
            $('[data-toggle="tooltip"]').tooltip({
                content: function () {
                    return $(this).prop('title');
                },
                'html': true,
            });
    
    		$('.i-checks').iCheck({
    			checkboxClass: 'icheckbox_square-green',
    			radioClass: 'iradio_square-green'
    		});
    
    		$(".todo-list").sortable({
    			placeholder: "sort-highlight",
    			handle: ".handle",
    			forcePlaceholderSize: true,
    			zIndex: 999999
    		}).disableSelection();
    
            $(window).on('load',function(){
                $('#policy_modal').modal('show');
            });
    
            $('#policy_agree').change(function (e) {
                if ($(this).prop('checked')) {
                    $('#confirm_policy_button').removeAttr('disabled');
                } else {
                    $('#confirm_policy_button').attr('disabled', 'disabled');
                }
            })
    
            $("#policy_modal form").on("submit", function(e) {
                e.preventDefault();
                $form = $(this);
                $.ajax({
                    url: $form.attr('action'),
                    type: "POST",
                    dataType: "JSON",
                    data: $form.serialize(),
                    success: function(data) {
                        if (data.status === "success") {
                            $('#policy_modal').modal('hide');
                        }
                    },
                    error: function(data) {
    
                    }
                })
                return false;
            });
    
    	});
    </script>
    
    <div class="modal fade" id="reinvestModal" tabindex="-1" role="dialog" aria-labelledby="reinvestModalLabel" aria-hidden="true" data-replace="true">
       <div class="modal-dialog modal-md">
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Реинвестировать</h4>
             </div>
             <div class="modal-body">
                <p>Функция Реинвест позволяет Вам автоматически докупать мощность, если на Вашем балансе достаточно средств.</p>
                <p>При включении этой функции, все средства на Вашем балансе будут использованы для покупки мощности.</p>
                <p>После каждой выплаты, система проверит, достаточно ли средств на Балансе для покупки минимального количества мощности (10 GH/s для SHA-256 или 1 MH/s для Scrypt). Если средств достаточно, покупка будет создана и подтверждена автоматически.</p>
                <form action="/panel/reinvest" id="reinvest-form" method="post" accept-charset="utf-8">
                   <div style="display:none;"><input type="hidden" name="_method" value="POST">
                   <input type="hidden" name="data[_Token][key]" value="fedcbd409b230b2ee438e4927289f9fa387683a3" id="Token917327281"></div>
                   <div class="form-group">
                      <label class="sr-only" for="reinvest">Реинвестировать</label>
                      <select name="data[reinvest]" id="reinvest" class="form-control">
                         <option value="">Do not reinvest</option>
                         <option value="sha">Reinvest in SHA-256</option>
                         <option value="scrypt">Reinvest in Scrypt</option>
                      </select>
                   </div>
                   <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="adb244fe48d60b927230abfc801e511cf4ca74f4%3A" id="TokenFields841478557"><input type="hidden" name="data[_Token][unlocked]" value="amount%7Ccustom.type%7Cpools.0.id%7Cpools.0.percentage%7Cpools.1.id%7Cpools.1.percentage%7Cpools.2.id%7Cpools.2.percentage%7Cyears" id="TokenUnlocked1759340814"></div>
                </form>
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="reinvest-save">Сохранить</button>
             </div>
          </div>
       </div>
    </div>
    
    <div id="flotTip" style="display: none; position: absolute; background: rgb(255, 255, 255); z-index: 100; padding: 0.4em 0.6em; border-radius: 0.5em; font-size: 0.8em; border: 1px solid rgb(17, 17, 17); white-space: nowrap; left: 663px; top: 1337px;"></div>

</body>
</html>