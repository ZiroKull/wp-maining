    
    </div>	
	<?php wp_footer(); ?>
    
    <script type="text/javascript">
        $(function(){
            $('#UserPasswordOld, #UserPasswordUpdate').PassRequirements({
                rules: {
                    minlength: {
                        text: "Пароль слишком короткий",
                        minLength: 6,
                    },
                    containLowercase: {
                        text: "Пароль должен содержать как минимум 1 символ нижнего регистра",
                        minLength: 1,
                        regex: new RegExp('[^a-z]', 'g')
                    },
                    containUppercase: {
                        text: "Пароль должен содержать как минимум 1 символ верхнего регистра",
                        minLength: 1,
                        regex: new RegExp('[^A-Z]', 'g')
                    },
                    containNumbers: {
                        text: "Пароль должен содержать как минимум 1 цифру",
                        minLength: 1,
                        regex: new RegExp('[^0-9]', 'g')
                    }
                }
            });
        
            $('#UserPasswordConfirmUpdate').PassRequirements({
                rules: {
                    isSimilar: {
                        text: "Оба пароля должны совпадать"
                    }
                }
            });
        });
    </script>
    
    <script type="text/javascript">
        $(document).ready(function(){
        
               $('[data-toggle="tooltip"]').tooltip({
                   content: function () {
                       return $(this).prop('title');
                   },
                   'html': true,
               });
        
        	$('.i-checks').iCheck({
        		checkboxClass: 'icheckbox_square-green',
        		radioClass: 'iradio_square-green'
        	});
        
        	$(".todo-list").sortable({
        		placeholder: "sort-highlight",
        		handle: ".handle",
        		forcePlaceholderSize: true,
        		zIndex: 999999
        	}).disableSelection();
        
               $(window).on('load',function(){
                   $('#policy_modal').modal('show');
               });
        
               $('#policy_agree').change(function (e) {
                   if ($(this).prop('checked')) {
                       $('#confirm_policy_button').removeAttr('disabled');
                   } else {
                       $('#confirm_policy_button').attr('disabled', 'disabled');
                   }
               })
        
               $("#policy_modal form").on("submit", function(e) {
                   e.preventDefault();
                   $form = $(this);
                   $.ajax({
                       url: $form.attr('action'),
                       type: "POST",
                       dataType: "JSON",
                       data: $form.serialize(),
                       success: function(data) {
                           if (data.status === "success") {
                               $('#policy_modal').modal('hide');
                           }
                       },
                       error: function(data) {
        
                       }
                   })
                   return false;
               });
        
        });
    </script>
    
    <div class="modal fade" id="reinvestModal" tabindex="-1" role="dialog" aria-labelledby="reinvestModalLabel" aria-hidden="true" data-replace="true">
       <div class="modal-dialog modal-md">
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Реинвестировать</h4>
             </div>
             <div class="modal-body">
                <p>Функция Реинвест позволяет Вам автоматически докупать мощность, если на Вашем балансе достаточно средств.</p>
                <p>При включении этой функции, все средства на Вашем балансе будут использованы для покупки мощности.</p>
                <p>После каждой выплаты, система проверит, достаточно ли средств на Балансе для покупки минимального количества мощности (10 GH/s для SHA-256 или 1 MH/s для Scrypt). Если средств достаточно, покупка будет создана и подтверждена автоматически.</p>
                <form action="/panel/reinvest" id="reinvest-form" method="post" accept-charset="utf-8">
                   <div style="display:none;"><input type="hidden" name="_method" value="POST">
                   <input type="hidden" name="data[_Token][key]" value="fedcbd409b230b2ee438e4927289f9fa387683a3" id="Token917327281"></div>
                   <div class="form-group">
                      <label class="sr-only" for="reinvest">Реинвестировать</label>
                      <select name="data[reinvest]" id="reinvest" class="form-control">
                         <option value="">Do not reinvest</option>
                         <option value="sha">Reinvest in SHA-256</option>
                         <option value="scrypt">Reinvest in Scrypt</option>
                      </select>
                   </div>
                   <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="adb244fe48d60b927230abfc801e511cf4ca74f4%3A" id="TokenFields841478557"><input type="hidden" name="data[_Token][unlocked]" value="amount%7Ccustom.type%7Cpools.0.id%7Cpools.0.percentage%7Cpools.1.id%7Cpools.1.percentage%7Cpools.2.id%7Cpools.2.percentage%7Cyears" id="TokenUnlocked1759340814"></div>
                </form>
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="reinvest-save">Сохранить</button>
             </div>
          </div>
       </div>
    </div>
    
    <div id="flotTip" style="display: none; position: absolute; background: rgb(255, 255, 255); z-index: 100; padding: 0.4em 0.6em; border-radius: 0.5em; font-size: 0.8em; border: 1px solid rgb(17, 17, 17); white-space: nowrap; left: 663px; top: 1337px;"></div>

</body>
</html>