<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="apple-touch-icon" sizes="57x57" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/apple-touch-icon-57x57.png?v=5">
    <link rel="apple-touch-icon" sizes="60x60" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/apple-touch-icon-60x60.png?v=5">
    <link rel="apple-touch-icon" sizes="72x72" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/apple-touch-icon-72x72.png?v=5">
    <link rel="apple-touch-icon" sizes="76x76" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/apple-touch-icon-76x76.png?v=5">
    <link rel="apple-touch-icon" sizes="114x114" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/apple-touch-icon-114x114.png?v=5">
    <link rel="apple-touch-icon" sizes="120x120" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/apple-touch-icon-120x120.png?v=5">
    <link rel="apple-touch-icon" sizes="144x144" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/apple-touch-icon-144x144.png?v=5">
    <link rel="apple-touch-icon" sizes="152x152" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/apple-touch-icon-152x152.png?v=5">
    <link rel="apple-touch-icon" sizes="180x180" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/apple-touch-icon-180x180.png?v=5">
    <link rel="icon" type="image/png" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/favicon-32x32.png?v=5" sizes="32x32">
    <link rel="icon" type="image/png" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/favicon-194x194.png?v=5" sizes="194x194">
    <link rel="icon" type="image/png" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/favicon-96x96.png?v=5" sizes="96x96">
    <link rel="icon" type="image/png" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/android-chrome-192x192.png?v=5" sizes="192x192">
    <link rel="icon" type="image/png" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/favicon-16x16.png?v=5" sizes="16x16">
    <link rel="manifest" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/manifest.json?v=5">
    <link rel="shortcut icon" href="http://telekotp.beget.tech/wp-content/themes/maining/favicon/favicon.ico?v=5">
    <meta name="msapplication-TileColor" content="#1a1ee7">
    <meta name="msapplication-TileImage" content="http://telekotp.beget.tech/wp-content/themes/maining/favicon/mstile-144x144.png?v=5">
    <meta name="theme-color" content="#1a1ee7">
    <style>
    .flot-chart.small {
    	height: 172px;
    }
    </style>
	<?php wp_head(); ?>
	<!--[if lt IE 9]>
	    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style>
	
</head>
<body <?php body_class(); ?>>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <div class="m-b-xl">
                                <a href="/"><img src="http://telekotp.beget.tech/wp-content/themes/maining/img/logo.png" style="width:100%;"></a>
                            </div>
                            <span>
                            <img alt="angansasha" class="img-circle" src="https://www.gravatar.com/avatar/7cbcb7ca073bb178ddc4755cd2318d7e?s=128&amp;d=mm&amp;r=g">
                            </span>
                            <span class="text-xs block text-white hyphenate"><br>angansasha@mail.ru</span>
                            <span class="text-xs block text-white hyphenate kyc-message">
                                <br>
                                Не верифицирован <a href="https://hashflare.zendesk.com/hc/ru/articles/360000111900"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Узнать больше"></i></a>
                                <div class="progress-block">
                                    <span class="progress-step progress-step-active" data-toggle="tooltip" data-placement="right" title="" data-original-title="<table class=&quot;table table-bordered&quot; id=&quot;withdrawal-limits&quot;>
                                        <tr>
                                        <th>Валюта</th>
                                        <th>Дневной лимит</th>
                                        <th>30-дневный лимит</th>
                                        </tr>
                                        <tbody>
                                        <tr>
                                        <td>BTC</td>
                                        <td>0</td>
                                        <td>0</td>
                                        </tr>
                                        <tr>
                                        <td>DASH</td>
                                        <td>0</td>
                                        <td>0</td>
                                        </tr>
                                        <tr>
                                        <td>ETH</td>
                                        <td>0</td>
                                        <td>0</td>
                                        </tr>
                                        <tr>
                                        <td>ZEC</td>
                                        <td>0</td>
                                        <td>0</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                        "></span>
                                    <span class="progress-step" data-toggle="tooltip" data-placement="right" title="" data-original-title="<table class=&quot;table table-bordered&quot; id=&quot;withdrawal-limits&quot;>
                                        <tr>
                                        <th>Валюта</th>
                                        <th>Дневной лимит</th>
                                        <th>30-дневный лимит</th>
                                        </tr>
                                        <tbody>
                                        <tr>
                                        <td>BTC</td>
                                        <td>0.01</td>
                                        <td>0.05</td>
                                        </tr>
                                        <tr>
                                        <td>DASH</td>
                                        <td>0.2</td>
                                        <td>1</td>
                                        </tr>
                                        <tr>
                                        <td>ETH</td>
                                        <td>0.1</td>
                                        <td>0.5</td>
                                        </tr>
                                        <tr>
                                        <td>ZEC</td>
                                        <td>0.2</td>
                                        <td>1</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                        "></span>
                                    <span class="progress-step" data-toggle="tooltip" data-placement="right" title="" data-original-title="<table class=&quot;table table-bordered&quot; id=&quot;withdrawal-limits&quot;>
                                        <tr>
                                        <th>Валюта</th>
                                        <th>Дневной лимит</th>
                                        <th>30-дневный лимит</th>
                                        </tr>
                                        <tbody>
                                        <tr>
                                        <td>BTC</td>
                                        <td>0.15</td>
                                        <td>0.75</td>
                                        </tr>
                                        <tr>
                                        <td>DASH</td>
                                        <td>4</td>
                                        <td>20</td>
                                        </tr>
                                        <tr>
                                        <td>ETH</td>
                                        <td>2</td>
                                        <td>10</td>
                                        </tr>
                                        <tr>
                                        <td>ZEC</td>
                                        <td>4</td>
                                        <td>20</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                        "></span>
                                </div>
                            </span>
                        </div>
                        <div class="logo-element">
                            <a href="/"><img src="http://telekotp.beget.tech/wp-content/themes/maining/img/logomini.png" style="width:30%;"></a>
                        </div>
                    </li>
                    <li class="inactive">
                        <a href="#">
                        <i class="fa fa-shopping-cart pulse-orange"></i>
                        <span class="nav-label pulse-orange">Купить хэшрейт </span>
                        <span class="fa arrow pulse-orange"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li class="inactive"><a href="/panel/upgrade?target=ether#ether">ETHASH</a></li>
                            <input type="hidden" id="types" data-json="%5B%22ether%22%5D"> 
                        </ul>
                    </li>
                    <li class="inactive">
                        <a href="/panel">
                        <i class="fa fa-th-large"></i> <span class="nav-label">Личный Кабинет</span>
                        </a>
                    </li>
                    <li class="inactive">
                        <a href="/panel/history">
                        <i class="fa fa-bars"></i> <span class="nav-label">История</span>
                        </a>
                    </li>
                    <li class="inactive">
                        <a href="/panel/redeem">
                        <i class="fa fa-star"></i> <span class="nav-label">Ваучер</span>
                        </a>
                    </li>
                    <li class="inactive">
                        <a>
                        <i class="fa fa-arrows-alt"></i>
                        <span class="nav-label">Рефералы</span>
                        <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li class="inactive">
                                <a href="/panel/referrals-new">
                                Инструменты </a>
                            </li>
                            <li class="inactive">
                                <a href="/panel/materials">
                                Материалы </a>
                            </li>
                        </ul>
                    </li>
                    <li class="inactive">
                        <a href="#">
                        <i class="fa fa-cogs"></i>
                        <span class="nav-label">Настройки</span>
                        <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li class="inactive">
                                <a href="/panel/profile">
                                Профиль </a>
                            </li>
                            <li class="inactive">
                                <a href="/panel/settings">
                                Настройки </a>
                            </li>
                            <li class="inactive">
                                <a href="/panel/verification">
                                Верификация </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                        <i class="fa fa-question"></i>
                        <span class="nav-label">Помощь</span>
                        <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li class="inactive">
                                <a href="/panel/limits">
                                Лимиты по выводам </a>
                            </li>
                            <li>
                                <a href="https://hashflare.zendesk.com/hc/ru/articles/360000652109" target="_blank">
                                Руководство</a>
                            </li>
                        </ul>
                    </li>
                    <li class="visible-xs">
                        <a href="/logout">
                        <i class="fa fa-sign-out"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>