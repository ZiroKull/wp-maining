<?php

//add_filter('show_admin_bar', '__return_false');

add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

function enqueue_styles() {
    
    if ( !is_page( array( 'reset', 'singin', 'register', 'panel', 'upgrade', 'history', 'redeem', 'referrals-new', 'materials', 'profile', 'settings', 'verification', 'limits', 'guide', 'buy power', 'my orders', 'panel settings' )) )
    {
        wp_enqueue_style( 'whitesquare-style', get_stylesheet_uri());
        wp_register_style('font-style', 'https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,700,700italic&subset=latin,cyrillic-ext');
        wp_enqueue_style( 'font-style');
        wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() .'/css/bootstrap.min.css' );
        wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() .'/css/font-awesome.min.css' );
        wp_enqueue_style( 'animate', get_stylesheet_directory_uri() .'/css/animate.min.css' );
        wp_enqueue_style( 'default', get_stylesheet_directory_uri() .'/css/default.css' );
        wp_enqueue_style( 'common-css', get_stylesheet_directory_uri() .'/css/common.css' );
        wp_enqueue_style( 'blueimp-gallery', get_stylesheet_directory_uri() .'/css/blueimp-gallery.min.css' );
        wp_enqueue_style( 'bootstrap-image-gallery', get_stylesheet_directory_uri() .'/css/bootstrap-image-gallery.min.css' );
        wp_enqueue_style( 'bootstrap-datepicker', get_stylesheet_directory_uri() .'/css/bootstrap-datepicker.min.css' );
        wp_enqueue_style( 'jquery-bxslider', get_stylesheet_directory_uri() .'/js/bxslider/dist/jquery.bxslider.css' );
        
        
    }
    else if ( is_page( array( 'panel', 'buy power', 'my orders', 'panel settings' )) )
    {
        wp_register_style('font-style', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&lang=en');
        wp_enqueue_style( 'font-style');
        wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() .'/css/bootstrap.min.css' );
        wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() .'/css/font-awesome.min.css' );
        wp_enqueue_style( 'morris', get_stylesheet_directory_uri() .'/css/dashboard/morris.css' );
        wp_enqueue_style( 'bootstrap-select', get_stylesheet_directory_uri() .'/css/dashboard/bootstrap-select.min.css' );
        wp_enqueue_style( 'style_dash', get_stylesheet_directory_uri() .'/css/dashboard/style_dash.min.css' );
        wp_enqueue_style( 'custom', get_stylesheet_directory_uri() .'/css/dashboard/custom.css' );
        wp_enqueue_style( 'style_dash_ru', get_stylesheet_directory_uri() .'/css/dashboard/style_dash_ru.css' );
        wp_enqueue_style( 'common-css', get_stylesheet_directory_uri() .'/css/common.css' );
    }
    else if ( is_page( array( 'upgrade', 'history', 'redeem', 'referrals-new', 'materials', 'settings', 'limits', 'guide' )) )
    {
        
        wp_register_style('font-style', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&lang=en');
        wp_enqueue_style( 'font-style');
        wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() .'/css/bootstrap.min.css' );
        wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() .'/css/font-awesome.min.css' );
        wp_enqueue_style( 'animate', get_stylesheet_directory_uri() .'/css/animate.min.css' );
        wp_enqueue_style( 'morris', get_stylesheet_directory_uri() .'/css/panel/morris-0.4.3.min.css' );
        wp_enqueue_style( 'gritter', get_stylesheet_directory_uri() .'/css/panel/jquery.gritter.css' );
        wp_enqueue_style( 'icheck-custom', get_stylesheet_directory_uri() .'/css/panel/custom.css' );
        wp_enqueue_style( 'jquery-nouislider', get_stylesheet_directory_uri() .'/css/panel/jquery.nouislider.css' );
        wp_enqueue_style( 'jquery-nouislider-pips', get_stylesheet_directory_uri() .'/css/panel/jquery.nouislider.pips.min.css' );
        wp_enqueue_style( 'jquery-steps', get_stylesheet_directory_uri() .'/css/panel/jquery.steps.css' );
        wp_enqueue_style( 'bootstrap-datepicker', get_stylesheet_directory_uri() .'/css/bootstrap-datepicker.min.css' );
        wp_enqueue_style( 'styles', get_stylesheet_directory_uri() .'/css/panel/styles.css' );
    }
    else if ( is_page( array( 'profile' )) )
    {
        
        wp_register_style('font-style', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&lang=en');
        wp_enqueue_style( 'font-style');
        wp_enqueue_style( 'intlTelInput', get_stylesheet_directory_uri() .'/css/panel/intlTelInput.min.css' );
        wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() .'/css/bootstrap.min.css' );
        wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() .'/css/font-awesome.min.css' );
        wp_enqueue_style( 'animate', get_stylesheet_directory_uri() .'/css/animate.min.css' );
        wp_enqueue_style( 'morris', get_stylesheet_directory_uri() .'/css/panel/morris-0.4.3.min.css' );
        wp_enqueue_style( 'gritter', get_stylesheet_directory_uri() .'/css/panel/jquery.gritter.css' );
        wp_enqueue_style( 'icheck-custom', get_stylesheet_directory_uri() .'/css/panel/custom.css' );
        wp_enqueue_style( 'jquery-nouislider', get_stylesheet_directory_uri() .'/css/panel/jquery.nouislider.css' );
        wp_enqueue_style( 'jquery-nouislider-pips', get_stylesheet_directory_uri() .'/css/panel/jquery.nouislider.pips.min.css' );
        wp_enqueue_style( 'jquery-steps', get_stylesheet_directory_uri() .'/css/panel/jquery.steps.css' );
        wp_enqueue_style( 'bootstrap-datepicker', get_stylesheet_directory_uri() .'/css/bootstrap-datepicker.min.css' );
        wp_enqueue_style( 'styles', get_stylesheet_directory_uri() .'/css/panel/styles.css' );
    }
    else if ( is_page( array( 'verification' )) )
    {
        
        wp_register_style('font-style', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&lang=en');
        wp_enqueue_style( 'font-style');
        wp_enqueue_style( 'kyc_verification', get_stylesheet_directory_uri() .'/css/panel/kyc_verification.css' );
        wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() .'/css/bootstrap.min.css' );
        wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() .'/css/font-awesome.min.css' );
        wp_enqueue_style( 'animate', get_stylesheet_directory_uri() .'/css/animate.min.css' );
        wp_enqueue_style( 'morris', get_stylesheet_directory_uri() .'/css/panel/morris-0.4.3.min.css' );
        wp_enqueue_style( 'gritter', get_stylesheet_directory_uri() .'/css/panel/jquery.gritter.css' );
        wp_enqueue_style( 'icheck-custom', get_stylesheet_directory_uri() .'/css/panel/custom.css' );
        wp_enqueue_style( 'jquery-nouislider', get_stylesheet_directory_uri() .'/css/panel/jquery.nouislider.css' );
        wp_enqueue_style( 'jquery-nouislider-pips', get_stylesheet_directory_uri() .'/css/panel/jquery.nouislider.pips.min.css' );
        wp_enqueue_style( 'jquery-steps', get_stylesheet_directory_uri() .'/css/panel/jquery.steps.css' );
        wp_enqueue_style( 'bootstrap-datepicker', get_stylesheet_directory_uri() .'/css/bootstrap-datepicker.min.css' );
        wp_enqueue_style( 'styles', get_stylesheet_directory_uri() .'/css/panel/styles.css' );
    }
    
    
}
add_action('wp_enqueue_scripts', 'enqueue_styles');


function prefix_add_footer_styles() {
    
    if ( !is_page( array( 'reset', 'singin', 'register', 'panel', 'upgrade', 'history', 'redeem', 'referrals-new', 'materials', 'profile', 'settings', 'verification', 'limits', 'guide', 'buy power', 'my orders', 'panel settings' )) )
    {
        wp_enqueue_script( 'jquery-2.1.1', get_stylesheet_directory_uri() .'/js/jquery-2.1.1.js' );
    	wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() .'/js/bootstrap.min.js' );
    	wp_enqueue_script( 'jquery-easing', get_stylesheet_directory_uri() .'/js/jquery.easing.min.js' );
    	wp_enqueue_script( 'scroll', get_stylesheet_directory_uri() .'/js/scroll.js' );
    	wp_enqueue_script( 'wow', get_stylesheet_directory_uri() .'/js/wow.min.js' );
    	wp_enqueue_script( 'custom', get_stylesheet_directory_uri() .'/js/custom.js' );
    	wp_enqueue_script( 'jquery-range', get_stylesheet_directory_uri() .'/js/jquery.range.js' );
    	wp_enqueue_script( 'default', get_stylesheet_directory_uri() .'/js/default.js' );
    	wp_enqueue_script( 'blueimp-gallery', get_stylesheet_directory_uri() .'/js/jquery.blueimp-gallery.min.js' );
    	wp_enqueue_script( 'bootstrap-image-gallery', get_stylesheet_directory_uri() .'/js/bootstrap-image-gallery.min.js' );
    	wp_enqueue_script( 'bootstrap-datepicker', get_stylesheet_directory_uri() .'/js/bootstrap-datepicker.min.js' );
    	wp_enqueue_script( 'PassRequirements', get_stylesheet_directory_uri() .'/js/PassRequirements.js' );
    	wp_enqueue_script( 'jquery-bxslider', get_stylesheet_directory_uri() .'/js/bxslider/dist/jquery.bxslider.js' );
    }
    else if ( is_page( array( 'reset', 'singin', 'register' )) )
    {
        wp_enqueue_script( 'jquery-2.1.1', get_stylesheet_directory_uri() .'/js/jquery-2.1.1.js' );
    	wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() .'/js/bootstrap.min.js' );
        wp_enqueue_script( 'formValidation', get_stylesheet_directory_uri() .'/js/validator/formValidation.min.js' );
        wp_enqueue_script( 'bootstrap-validator', get_stylesheet_directory_uri() .'/js/validator/bootstrap-validator.min.js' );
        wp_enqueue_script( 'wp-register-loginc', get_stylesheet_directory_uri() .'/js/wp-register-loginc.js' );
    }
    else if ( is_page( array( 'panel', 'upgrade', 'history', 'redeem', 'referrals-new', 'materials', 'verification', 'limits', 'guide', 'buy power', 'my orders', 'panel settings' )) )
    {
        wp_enqueue_script( 'moment', get_stylesheet_directory_uri() .'/js/panel/moment.min.js' );
        wp_enqueue_script( 'jquery-2.1.1', get_stylesheet_directory_uri() .'/js/jquery-2.1.1.js' );
        wp_enqueue_script( 'jquery-ui-1.10.4', get_stylesheet_directory_uri() .'/js/panel/jquery-ui-1.10.4.min.js' );
        wp_enqueue_script( 'jquery-ui.custom', get_stylesheet_directory_uri() .'/js/panel/jquery-ui.custom.min.js' );
        wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() .'/js/bootstrap.min.js' );
        wp_enqueue_script( 'jquery-metisMenu', get_stylesheet_directory_uri() .'/js/panel/jquery.metisMenu.js' );
        wp_enqueue_script( 'slimscroll', get_stylesheet_directory_uri() .'/js/panel/jquery.slimscroll.min.js' );
        wp_enqueue_script( 'pace', get_stylesheet_directory_uri() .'/js/panel/pace.min.js' );
        wp_enqueue_script( 'icheck', get_stylesheet_directory_uri() .'/js/panel/icheck.min.js' );
        wp_enqueue_script( 'steps', get_stylesheet_directory_uri() .'/js/panel/jquery.steps.min.js' );
        wp_enqueue_script( 'nouislider', get_stylesheet_directory_uri() .'/js/panel/jquery.nouislider.all.min.js' );
        wp_enqueue_script( 'flot', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.js' );
        wp_enqueue_script( 'flot-pie', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.pie.js' );
        wp_enqueue_script( 'flot-tooltip', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.tooltip.min.js' );
        wp_enqueue_script( 'flot-stackpercent', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.stackpercent.js' );
        wp_enqueue_script( 'flot-resize', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.resize.js' );
        wp_enqueue_script( 'flot-time', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.time.js' );
        wp_enqueue_script( 'peity', get_stylesheet_directory_uri() .'/js/panel/jquery.peity.min.js' );
        wp_enqueue_script( 'easypiechart', get_stylesheet_directory_uri() .'/js/panel/jquery.easypiechart.js' );
        wp_enqueue_script( 'sparkline', get_stylesheet_directory_uri() .'/js/panel/jquery.sparkline.min.js' );
        wp_enqueue_script( 'dataTables', get_stylesheet_directory_uri() .'/js/panel/jquery.dataTables.js' );
        wp_enqueue_script( 'dataTables-bootstrap', get_stylesheet_directory_uri() .'/js/panel/dataTables.bootstrap.js' );
        wp_enqueue_script( 'dataTables-tableTools', get_stylesheet_directory_uri() .'/js/panel/dataTables.tableTools.min.js' );
        wp_enqueue_script( 'datetime-moment', get_stylesheet_directory_uri() .'/js/panel/datetime-moment.js' );
        wp_enqueue_script( 'bootstrap-datepicker', get_stylesheet_directory_uri() .'/js/bootstrap-datepicker.min.js' );
        wp_enqueue_script( 'inspinia', get_stylesheet_directory_uri() .'/js/panel/inspinia.js' );
        wp_enqueue_script( 'poolsAndSliders', get_stylesheet_directory_uri() .'/js/panel/poolsAndSliders.js' );
        wp_enqueue_script( 'panel', get_stylesheet_directory_uri() .'/js/panel/panel.js' );
        wp_enqueue_script( 'flot-selection', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.selection.min.js' );
    }
    else if ( is_page( array( 'profile' )) )
    {
        wp_enqueue_script( 'moment', get_stylesheet_directory_uri() .'/js/panel/moment.min.js' );
        wp_enqueue_script( 'jquery-2.1.1', get_stylesheet_directory_uri() .'/js/jquery-2.1.1.js' );
        wp_enqueue_script( 'jquery-ui-1.10.4', get_stylesheet_directory_uri() .'/js/panel/jquery-ui-1.10.4.min.js' );
        wp_enqueue_script( 'jquery-ui.custom', get_stylesheet_directory_uri() .'/js/panel/jquery-ui.custom.min.js' );
        wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() .'/js/bootstrap.min.js' );
        wp_enqueue_script( 'jquery-metisMenu', get_stylesheet_directory_uri() .'/js/panel/jquery.metisMenu.js' );
        wp_enqueue_script( 'slimscroll', get_stylesheet_directory_uri() .'/js/panel/jquery.slimscroll.min.js' );
        wp_enqueue_script( 'pace', get_stylesheet_directory_uri() .'/js/panel/pace.min.js' );
        wp_enqueue_script( 'icheck', get_stylesheet_directory_uri() .'/js/panel/icheck.min.js' );
        wp_enqueue_script( 'steps', get_stylesheet_directory_uri() .'/js/panel/jquery.steps.min.js' );
        wp_enqueue_script( 'nouislider', get_stylesheet_directory_uri() .'/js/panel/jquery.nouislider.all.min.js' );
        wp_enqueue_script( 'flot', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.js' );
        wp_enqueue_script( 'flot-pie', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.pie.js' );
        wp_enqueue_script( 'flot-tooltip', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.tooltip.min.js' );
        wp_enqueue_script( 'flot-stackpercent', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.stackpercent.js' );
        wp_enqueue_script( 'flot-resize', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.resize.js' );
        wp_enqueue_script( 'flot-time', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.time.js' );
        wp_enqueue_script( 'peity', get_stylesheet_directory_uri() .'/js/panel/jquery.peity.min.js' );
        wp_enqueue_script( 'easypiechart', get_stylesheet_directory_uri() .'/js/panel/jquery.easypiechart.js' );
        wp_enqueue_script( 'sparkline', get_stylesheet_directory_uri() .'/js/panel/jquery.sparkline.min.js' );
        wp_enqueue_script( 'dataTables', get_stylesheet_directory_uri() .'/js/panel/jquery.dataTables.js' );
        wp_enqueue_script( 'dataTables-bootstrap', get_stylesheet_directory_uri() .'/js/panel/dataTables.bootstrap.js' );
        wp_enqueue_script( 'dataTables-tableTools', get_stylesheet_directory_uri() .'/js/panel/dataTables.tableTools.min.js' );
        wp_enqueue_script( 'datetime-moment', get_stylesheet_directory_uri() .'/js/panel/datetime-moment.js' );
        wp_enqueue_script( 'bootstrap-datepicker', get_stylesheet_directory_uri() .'/js/bootstrap-datepicker.min.js' );
        wp_enqueue_script( 'inspinia', get_stylesheet_directory_uri() .'/js/panel/inspinia.js' );
        wp_enqueue_script( 'poolsAndSliders', get_stylesheet_directory_uri() .'/js/panel/poolsAndSliders.js' );
        wp_enqueue_script( 'panel', get_stylesheet_directory_uri() .'/js/panel/panel.js' );
        wp_enqueue_script( 'flot-selection', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.selection.min.js' );
        wp_enqueue_script( 'utils', get_stylesheet_directory_uri() .'/js/panel/utils.js' );
        wp_enqueue_script( 'intlTelInput', get_stylesheet_directory_uri() .'/js/panel/intlTelInput.min.js' );
    }
    else if ( is_page( array( 'settings' )) )
    {
        wp_enqueue_script( 'moment', get_stylesheet_directory_uri() .'/js/panel/moment.min.js' );
        wp_enqueue_script( 'jquery-2.1.1', get_stylesheet_directory_uri() .'/js/jquery-2.1.1.js' );
        wp_enqueue_script( 'jquery-ui-1.10.4', get_stylesheet_directory_uri() .'/js/panel/jquery-ui-1.10.4.min.js' );
        wp_enqueue_script( 'jquery-ui.custom', get_stylesheet_directory_uri() .'/js/panel/jquery-ui.custom.min.js' );
        wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() .'/js/bootstrap.min.js' );
        wp_enqueue_script( 'jquery-metisMenu', get_stylesheet_directory_uri() .'/js/panel/jquery.metisMenu.js' );
        wp_enqueue_script( 'slimscroll', get_stylesheet_directory_uri() .'/js/panel/jquery.slimscroll.min.js' );
        wp_enqueue_script( 'pace', get_stylesheet_directory_uri() .'/js/panel/pace.min.js' );
        wp_enqueue_script( 'icheck', get_stylesheet_directory_uri() .'/js/panel/icheck.min.js' );
        wp_enqueue_script( 'steps', get_stylesheet_directory_uri() .'/js/panel/jquery.steps.min.js' );
        wp_enqueue_script( 'nouislider', get_stylesheet_directory_uri() .'/js/panel/jquery.nouislider.all.min.js' );
        wp_enqueue_script( 'flot', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.js' );
        wp_enqueue_script( 'flot-pie', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.pie.js' );
        wp_enqueue_script( 'flot-tooltip', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.tooltip.min.js' );
        wp_enqueue_script( 'flot-stackpercent', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.stackpercent.js' );
        wp_enqueue_script( 'flot-resize', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.resize.js' );
        wp_enqueue_script( 'flot-time', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.time.js' );
        wp_enqueue_script( 'peity', get_stylesheet_directory_uri() .'/js/panel/jquery.peity.min.js' );
        wp_enqueue_script( 'easypiechart', get_stylesheet_directory_uri() .'/js/panel/jquery.easypiechart.js' );
        wp_enqueue_script( 'sparkline', get_stylesheet_directory_uri() .'/js/panel/jquery.sparkline.min.js' );
        wp_enqueue_script( 'dataTables', get_stylesheet_directory_uri() .'/js/panel/jquery.dataTables.js' );
        wp_enqueue_script( 'dataTables-bootstrap', get_stylesheet_directory_uri() .'/js/panel/dataTables.bootstrap.js' );
        wp_enqueue_script( 'dataTables-tableTools', get_stylesheet_directory_uri() .'/js/panel/dataTables.tableTools.min.js' );
        wp_enqueue_script( 'datetime-moment', get_stylesheet_directory_uri() .'/js/panel/datetime-moment.js' );
        wp_enqueue_script( 'bootstrap-datepicker', get_stylesheet_directory_uri() .'/js/bootstrap-datepicker.min.js' );
        wp_enqueue_script( 'inspinia', get_stylesheet_directory_uri() .'/js/panel/inspinia.js' );
        wp_enqueue_script( 'poolsAndSliders', get_stylesheet_directory_uri() .'/js/panel/poolsAndSliders.js' );
        wp_enqueue_script( 'panel', get_stylesheet_directory_uri() .'/js/panel/panel.js' );
        wp_enqueue_script( 'flot-selection', get_stylesheet_directory_uri() .'/js/panel/jquery.flot.selection.min.js' );
        wp_enqueue_script( 'email-decode', get_stylesheet_directory_uri() .'/js/panel/email-decode.min.js' );
        wp_enqueue_script( 'PassRequirements', get_stylesheet_directory_uri() .'/js/panel/PassRequirements.js' );
    }
	
};
add_action( 'get_footer', 'prefix_add_footer_styles' );

function crunchify_stop_loading_wp_embed_and_jquery() {
	if (!is_admin()) {
		wp_deregister_script('wp-embed');
		wp_deregister_script('jquery');  
	}
}
add_action('init', 'crunchify_stop_loading_wp_embed_and_jquery');

// Remove the REST API endpoint.
remove_action( 'rest_api_init', 'wp_oembed_register_route' );
 
// Turn off oEmbed auto discovery.
add_filter( 'embed_oembed_discover', '__return_false' );
 
// Don't filter oEmbed results.
remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
 
// Remove oEmbed discovery links.
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
 
// Remove oEmbed-specific JavaScript from the front-end and back-end.
remove_action( 'wp_head', 'wp_oembed_add_host_js' );
 
// Remove all embeds rewrite rules.
add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );

if (function_exists('add_theme_support')) {
	add_theme_support('menus');
}

add_action('init', function(){

	global $wpdb, $PasswordHash, $current_user, $user_ID;
	
	$err = '';
	$success = '';

	if(isset($_POST['task']) && $_POST['task'] == 'login' )
	{
		//We shall SQL escape all inputs to avoid sql injection.
		$username = $wpdb->escape($_POST['Email']);
		$password = $wpdb->escape($_POST['Password']);
		$remember = $wpdb->escape($_POST['RememberMe']);
		
		if (filter_var($username, FILTER_VALIDATE_EMAIL)) //Invalid Email
		{ 
			$user = get_user_by('email', $username);
		} else {
			$user = get_user_by('login', $username);
		}
		 
		if ($user && wp_check_password( $password, $user->data->user_pass, $user->ID))
		{
			$creds = array('user_login' => $user->data->user_login, 'user_password' => $password, 'remember' => $remember);
			$user2 = wp_signon( $creds, is_ssl() );
			
			wp_set_auth_cookie($user2->data->ID);
			wp_set_current_user($user2->data->ID, $user2->data->user_login);
			
			do_action('set_current_user');
			
			//header('Location: ' . "http://telekotp.beget.tech/panel/");
			
			$custom_page_url = 'http://telekotp.beget.tech/panel/';

            wp_redirect( $custom_page_url );
            exit;
		}
	}

	if(isset($_POST['task']) && $_POST['task'] == 'register' )
	{
		$email = $wpdb->escape(trim($_POST['Email']));
		$pwd1 = $wpdb->escape(trim($_POST['Password']));
		
		if(email_exists($email) ) 
		{
			$err = 'Email уже зарегистрирован.';
		}
		else 
		{
			$userdata = array(
				'user_login' => apply_filters('pre_user_login', $email),
                'user_pass' => apply_filters('pre_user_pass', $pwd1),
                'user_email' => apply_filters('pre_user_email', $email),
                'role' => get_option('default_role'),
                'user_registered' => date('Y-m-d H:i:s')
            );
			
			$user_id = wp_insert_user($userdata);
			
			 // checking for errors while user registration
            if (is_wp_error($user_id)) 
			{
                $err = $user_id->get_error_message();
            } 
			else 
			{
				do_action('user_register', $user_id);
				
				$user  = get_user_by('email', $email);
				$creds = array('user_login' => $user->data->user_login, 'user_password' => $pwd1);
				$user2 = wp_signon( $creds, is_ssl() );
			
				wp_set_auth_cookie($user2->data->ID);
				wp_set_current_user($user2->data->ID);
				
				do_action('set_current_user');
				
				//header('Location: ' . "http://telekotp.beget.tech/panel/");
				
				$custom_page_url = 'http://telekotp.beget.tech/panel/';

                wp_redirect( $custom_page_url );
                exit;
			}
		}
	}	
});

/**
 * Настройки темы при инициализации
 */
 
function sparadai_setup() {
	
	/**
	 * Регистрация всех меню
	 */
	register_nav_menus( array(
		'primary' => __( 'Main Menu', 'maining' ),
		'footer-left' => __( 'Footer Left', 'maining' ),
		'footer-center' => __( 'Footer Center', 'maining' ),
	) );
	
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );
}

add_action( 'after_setup_theme', 'sparadai_setup' );