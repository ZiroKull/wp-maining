    
    </div>	
	<?php wp_footer(); ?>
    
    <div class="modal fade" id="reinvestModal" tabindex="-1" role="dialog" aria-labelledby="reinvestModalLabel" aria-hidden="true" data-replace="true">
       <div class="modal-dialog modal-md">
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Реинвестировать</h4>
             </div>
             <div class="modal-body">
                <p>Функция Реинвест позволяет Вам автоматически докупать мощность, если на Вашем балансе достаточно средств.</p>
                <p>При включении этой функции, все средства на Вашем балансе будут использованы для покупки мощности.</p>
                <p>После каждой выплаты, система проверит, достаточно ли средств на Балансе для покупки минимального количества мощности (10 GH/s для SHA-256 или 1 MH/s для Scrypt). Если средств достаточно, покупка будет создана и подтверждена автоматически.</p>
                <form action="/panel/reinvest" id="reinvest-form" method="post" accept-charset="utf-8">
                   <div style="display:none;"><input type="hidden" name="_method" value="POST">
                   <input type="hidden" name="data[_Token][key]" value="fedcbd409b230b2ee438e4927289f9fa387683a3" id="Token917327281"></div>
                   <div class="form-group">
                      <label class="sr-only" for="reinvest">Реинвестировать</label>
                      <select name="data[reinvest]" id="reinvest" class="form-control">
                         <option value="">Do not reinvest</option>
                         <option value="sha">Reinvest in SHA-256</option>
                         <option value="scrypt">Reinvest in Scrypt</option>
                      </select>
                   </div>
                   <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="adb244fe48d60b927230abfc801e511cf4ca74f4%3A" id="TokenFields841478557"><input type="hidden" name="data[_Token][unlocked]" value="amount%7Ccustom.type%7Cpools.0.id%7Cpools.0.percentage%7Cpools.1.id%7Cpools.1.percentage%7Cpools.2.id%7Cpools.2.percentage%7Cyears" id="TokenUnlocked1759340814"></div>
                </form>
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="reinvest-save">Сохранить</button>
             </div>
          </div>
       </div>
    </div>
    
    <div id="flotTip" style="display: none; position: absolute; background: rgb(255, 255, 255); z-index: 100; padding: 0.4em 0.6em; border-radius: 0.5em; font-size: 0.8em; border: 1px solid rgb(17, 17, 17); white-space: nowrap; left: 663px; top: 1337px;"></div>

</body>
</html>