    
    </div>	
	<?php wp_footer(); ?>
	
    <script type="text/javascript">
        $(document).ready(function () {
        	//$.fn.dataTable.moment('DD.MM.YY HH:mm');
        
        	$('.dataTable').DataTable({
        		responsive: true,
        		'dom': "<'clear'>lrtfp",
        		'tableTools': {
        			'sSwfPath': <?php get_stylesheet_directory_uri() ?> '/js/panel/copy_csv_xls_pdf.swf'
        		},
        		'order': [
        		    [0, 'desc']
        		],
        		'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
        		language: {
        			search: '_INPUT_',
        	        searchPlaceholder: 'Поиск',
        	        'emptyTable':     'Данные отсутствуют',
        		    'info':           'Показаны _START_ - _END_ из _TOTAL_ записей',
        		    'infoEmpty':      'Показаны 0-0 из 0 записей ',
        		    'infoFiltered':   '(отфильтровано от _MAX_ записей )',
        		    'infoPostFix':    '',
        		    'thousands':      ',',
        		    'lengthMenu':     'Показать записи _MENU_ ',
        		    'loadingRecords': 'Загрузка ...',
        		    'processing':     'Обработка ...',
        		    'zeroRecords':    'Не найдено ни одной записи ',
        		    'paginate': {
        		        'first':      'Сначала ',
        		        'last':       'Последняя ',
        		        'next':       'След. ',
        		        'previous':   'Пред. '
        		    },
        		    'aria': {
        		        'sortAscending':  ': активировать для сортировки столбца по возрастанию ',
        		        'sortDescending': ': активировать для сортировки столбца по убыванию '
        		    }
        	    },
        	    'columnDefs': [
                       { 'width': '20%', 'targets': 0 },
                       {   
                           'targets': ['Start','Expiry'],
                           'render': $.fn.dataTable.moment('DD.MM.YYYY')
                       },
                       {   
                           'targets': ['Time'],
                           'render': $.fn.dataTable.moment('DD.MM.YYYY HH:mm')
                       }
                   ]
        	});
        	
        	$('.dataTableLog').DataTable({
        	    'destroy': true,
        		'order': [
        		    [1, 'desc']
        		]
        	});
        	
        	// UserLogs
        	var userLogTable = $('.dataTableLog').DataTable();
        	console.log(userLogTable.page.info().pages);
        	if (userLogTable.page.info().pages <= 1){
        	    $('.user_logs_history_link').show();
        	}
        	
        	userLogTable.on('page.dt', function () {
                   var info = userLogTable.page.info()
                   if ((info.page + 1) == info.pages) {
                       $('.user_logs_history_link').show();
                   } else {
                       $('.user_logs_history_link').hide();
                   }
               } );
        	// End
        });
    </script>
    
    <script type="text/javascript">
    	$(document).ready(function(){
    
            $('[data-toggle="tooltip"]').tooltip({
                content: function () {
                    return $(this).prop('title');
                },
                'html': true,
            });
    
    		$('.i-checks').iCheck({
    			checkboxClass: 'icheckbox_square-green',
    			radioClass: 'iradio_square-green'
    		});
    
    		$(".todo-list").sortable({
    			placeholder: "sort-highlight",
    			handle: ".handle",
    			forcePlaceholderSize: true,
    			zIndex: 999999
    		}).disableSelection();
    
            $(window).on('load',function(){
                $('#policy_modal').modal('show');
            });
    
            $('#policy_agree').change(function (e) {
                if ($(this).prop('checked')) {
                    $('#confirm_policy_button').removeAttr('disabled');
                } else {
                    $('#confirm_policy_button').attr('disabled', 'disabled');
                }
            })
    
            $("#policy_modal form").on("submit", function(e) {
                e.preventDefault();
                $form = $(this);
                $.ajax({
                    url: $form.attr('action'),
                    type: "POST",
                    dataType: "JSON",
                    data: $form.serialize(),
                    success: function(data) {
                        if (data.status === "success") {
                            $('#policy_modal').modal('hide');
                        }
                    },
                    error: function(data) {
    
                    }
                })
                return false;
            });
    
    	});
    </script>
    
    <div class="modal fade" id="reinvestModal" tabindex="-1" role="dialog" aria-labelledby="reinvestModalLabel" aria-hidden="true" data-replace="true">
       <div class="modal-dialog modal-md">
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Реинвестировать</h4>
             </div>
             <div class="modal-body">
                <p>Функция Реинвест позволяет Вам автоматически докупать мощность, если на Вашем балансе достаточно средств.</p>
                <p>При включении этой функции, все средства на Вашем балансе будут использованы для покупки мощности.</p>
                <p>После каждой выплаты, система проверит, достаточно ли средств на Балансе для покупки минимального количества мощности (10 GH/s для SHA-256 или 1 MH/s для Scrypt). Если средств достаточно, покупка будет создана и подтверждена автоматически.</p>
                <form action="/panel/reinvest" id="reinvest-form" method="post" accept-charset="utf-8">
                   <div style="display:none;"><input type="hidden" name="_method" value="POST">
                   <input type="hidden" name="data[_Token][key]" value="fedcbd409b230b2ee438e4927289f9fa387683a3" id="Token917327281"></div>
                   <div class="form-group">
                      <label class="sr-only" for="reinvest">Реинвестировать</label>
                      <select name="data[reinvest]" id="reinvest" class="form-control">
                         <option value="">Do not reinvest</option>
                         <option value="sha">Reinvest in SHA-256</option>
                         <option value="scrypt">Reinvest in Scrypt</option>
                      </select>
                   </div>
                   <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="adb244fe48d60b927230abfc801e511cf4ca74f4%3A" id="TokenFields841478557"><input type="hidden" name="data[_Token][unlocked]" value="amount%7Ccustom.type%7Cpools.0.id%7Cpools.0.percentage%7Cpools.1.id%7Cpools.1.percentage%7Cpools.2.id%7Cpools.2.percentage%7Cyears" id="TokenUnlocked1759340814"></div>
                </form>
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="reinvest-save">Сохранить</button>
             </div>
          </div>
       </div>
    </div>
    
    <div id="flotTip" style="display: none; position: absolute; background: rgb(255, 255, 255); z-index: 100; padding: 0.4em 0.6em; border-radius: 0.5em; font-size: 0.8em; border: 1px solid rgb(17, 17, 17); white-space: nowrap; left: 663px; top: 1337px;"></div>

</body>
</html>