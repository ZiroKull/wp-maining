<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<link rel="icon" type="image/ico" href="http://telekotp.beget.tech/wp-content/themes/maining/img/faviconpmr.ico" sizes="32x32">
    <link rel="icon" type="image/ico" href="http://telekotp.beget.tech/wp-content/themes/maining/img/faviconpmr.ico" sizes="16x16">
    <link rel="shortcut icon" href="http://telekotp.beget.tech/wp-content/themes/maining/img/faviconpmr.ico?v=5">
    
    <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/style-crlr.css">
    
	<?php wp_head(); ?>
	<!--[if lt IE 9]>
	    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>
<body>
