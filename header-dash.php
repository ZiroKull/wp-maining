<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<link rel="icon" type="image/ico" href="http://telekotp.beget.tech/wp-content/themes/maining/img/faviconpmr.ico" sizes="32x32">
    <link rel="icon" type="image/ico" href="http://telekotp.beget.tech/wp-content/themes/maining/img/faviconpmr.ico" sizes="16x16">
    <link rel="shortcut icon" href="http://telekotp.beget.tech/wp-content/themes/maining/img/faviconpmr.ico?v=5">
    
    <meta name="theme-color" content="#1a1ee7">
    
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    
    <header>
        <nav class="navbar navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-menu">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/panel" title="PMR Mining">
                    PMR Mining
                </a>
            </div>
            <div class="navbar-header-container">
                <div class="container">
                    <div class="mma">
                        <div class="row">
                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                <h1>ГЛАВНАЯ ПАНЕЛЬ</h1>
                                <p>Обзор последних данных</p>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                                <div class="limiter">
                                    <p>
                                        <a class="nav-logout" href="<?php echo wp_logout_url( home_url() ); ?>">
                                            <svg class="gm-icon gm-icon-logout">
                                                <title>Выйти</title>
                                                <use xlink:href="<?php echo bloginfo( 'template_url' ); ?>/img/dashboard/sprite_dash.svg#gm-icon-logout"></use>
                                            </svg>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="main-menu">
                <div class="nav-user-info">
                    <h2>С возвращением!            </h2>
                    <span class="fa fa-user"></span>
                </div>
                <ul class="nav navbar-nav ">
                    <li class=""> <!-- active -->
                        <a class="navbar-link" href="<?php echo site_url(); ?>">
                            <span class="fa">
                                <svg class="gm-icon gm-icon-dashboard">
                                    <use xlink:href="<?php echo bloginfo( 'template_url' ); ?>/img/dashboard/sprite_dash.svg#gm-icon-dashboard"></use>
                                </svg>
                            </span>
                            <b class="nav-text">На главную</b>
                        </a>
                    </li>
                    <li class=""> <!-- active -->
                        <a class="navbar-link" href="/panel">
                            <span class="fa">
                                <svg class="gm-icon gm-icon-dashboard">
                                    <use xlink:href="<?php echo bloginfo( 'template_url' ); ?>/img/dashboard/sprite_dash.svg#gm-icon-dashboard"></use>
                                </svg>
                            </span>
                            <b class="nav-text">Главная панель</b>
                        </a>
                    </li>
                    <li class="">
                    <a class="navbar-link" href="/panel/buy-power">
                        <span class="fa">
                            <svg class="gm-icon gm-icon-hashpower">
                                <use xlink:href="<?php echo bloginfo( 'template_url' ); ?>/img/dashboard/sprite_dash.svg#gm-icon-hashpower"></use>
                            </svg>
                        </span>
                        <b class="nav-text">купить генерируемую мощность</b></a>
                    </li>
                    <li class="">
                        <a class="navbar-link" href="/panel/my-orders">
                            <span class="fa">
                                <svg class="gm-icon gm-icon-my-orders">
                                    <use xlink:href="<?php echo bloginfo( 'template_url' ); ?>/img/dashboard/sprite_dash.svg#gm-icon-my-orders"></use>
                                </svg>
                            </span>
                            <b class="nav-text">Мои заказы</b>
                        </a>
                    </li>
                    <li class="">
                        <a class="navbar-link" href="/panel/settings">
                            <span class="fa fa-cogs"></span>
                            <b class="nav-text">Настройки</b>
                        </a>
                    </li>
                    <li class="emptys">&nbsp;</li>
                </ul>
            </div>
        </nav>
    </header>