if (typeof jQuery === 'undefined') {
  throw new Error('PassRequirements requires jQuery')
}

+(function ($) {

    $.fn.PassRequirements = function (options) {
        var defaults = {
            defaults: false
        };

        var dom_selector = this;
        var selector = this.selector;
        
        if(
            !options ||                     //if no options are passed                                  /*
            options.defaults == true     //if default option is passed with defaults set to true      * Extend options with default ones
        ){
            if(!options){                   //if no options are passed,
                options = {};               //create an options object
            }
            defaults.rules = $.extend({
                minlength: {
                    text: "be at least minLength characters long",
                    minLength: 8,
                },
                containLowercase: {
                    text: "Your input should contain at least minLength lower case character",
                    minLength: 1,
                    regex: new RegExp('[^a-z]', 'g')
                },
                containUppercase: {
                    text: "Your input should contain at least minLength upper case character",
                    minLength: 1,
                    regex: new RegExp('[^A-Z]', 'g')
                },
                containNumbers: {
                    text: "Your input should contain at least minLength number",
                    minLength: 1,
                    regex: new RegExp('[^0-9]', 'g')
                }
            }, options.rules);
        }else{
            defaults[selector] = options;     //if options are passed with defaults === false
        }
        
        var i = 0;

        return this.each(function () {
            if(!defaults[selector].rules){
                return false;
            }
            
            var requirementList = "";
            $(this).data('pass-req-id', i++);

            $(this).keyup(function () {
                var this_ = $(this);
                Object.getOwnPropertyNames(defaults[selector].rules).forEach(function (val, idx, array) {
                    if(val === 'isSimilar'){
                        if(dom_selector.closest('.form-group').prev().find('input').val() === this_.val()){
                            this_.next('.popover').find('#' + val).css('text-decoration', 'line-through');
                        }else{
                            this_.next('.popover').find('#' + val).css('text-decoration', 'none');
                        }
                    }else {
                        if (this_.val().replace(defaults[selector].rules[val].regex, "").length > defaults[selector].rules[val].minLength - 1) {
                            this_.next('.popover').find('#' + val).css('text-decoration', 'line-through');
                        } else {
                            this_.next('.popover').find('#' + val).css('text-decoration', 'none');
                        }
                    }

                })
            });

            Object.getOwnPropertyNames(defaults[selector].rules).forEach(function (val, idx, array) {
                requirementList += (("<li id='" + val + "'>" + defaults[selector].rules[val].text).replace("minLength", defaults[selector].rules[val].minLength));
            })
            try{
            $(this).popover({
                title: 'Password Requirements',
                trigger: options.trigger ? options.trigger : 'focus',
                html: true,
                placement: options.popoverPlacement ? options.popoverPlacement : 'bottom',
                content: '<ul>' + requirementList + '</ul>'
            });
            }catch(e){
                throw new Error('PassRequirements requires Bootstraps Popover plugin');
            }
            $(this).focus(function () {
                $(this).keyup();
            });
        });
    };

}(jQuery));
