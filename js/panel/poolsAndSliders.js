(function($) {
    'use strict';

    $.fn.poolsAndSliders = function(options) {
        if (!options) throw new Error('Pools and URL is required');
        if (!options['pools']) throw new Error('Pools options is required');
        if (!options['url']) throw new Error('URL is required');
        if (!options['maxPools']) options['maxPools'] = 3;

        // this will be used inside of other objects
        var self = this;

        // helper to work with selected IDs
        var selected = {
            ids: function() {
                return self.find('.pools select').map(function(){
                    return parseInt($(this).val());
                }).get();
            },
            contains: function(id) {
                id = parseInt(id);

                if (isNaN(id)) throw new Error('Cannot convert ID to integer');

                return this.ids().indexOf(id) !== -1;
            },
            length: function() {
                return this.ids().length;
            }
        };

        // helper to work with available pools
        var available = {
            all: function() {
                var pools = options['pools'];

                return pools.filter(function(item){
                    return !selected.contains(item.id);
                });
            },
            first: function() {
                return this.all()[0] || null;
            },
            byId: function(id) {
                id = parseInt(id);

                var pools = options['pools'];

                for(var i = 0; i < pools.length; i++) {
                    if (parseInt(pools[i].id) === id) return pools[i];
                }

                return null;
            }
        };

        // helper to work with sliders
        var sliders = {
            stack: (function() {
                var stack = [];

                return {
                    add: function(slider) {
                        // if slider is already in stack remove it and place it in the end
                        var index = stack.indexOf(slider);
                        if (index !== -1) stack.splice(index, 1);
                        stack.push(slider);
                    },
                    first: function() {
                        return stack[0];
                    },
                    shift: function() {
                        stack.shift();
                        return this;
                    },
                    refresh: function() {
                        stack = self.find('.pools .pool .slider').map(function() {
                            return this;
                        }).get();
                    }
                }
            }()),
            autobalance: function(except) {
                // count total sliders percentage
                var percentage = 0;

                self.find('.pools .pool .slider').each(function() {
                    percentage += parseInt($(this).val());
                });

                // if total percentage is 100 do nothing
                if (percentage === 100) return;

                // while there is a difference add or substract to sliders from stack
                var difference = 100 - percentage;

                while (difference) {
                    var slider = this.stack.first();
                    var value = parseInt($(slider).val());

                    if ((value === 100 && difference > 0) || (value === 0 && difference < 0)) {
                        slider = this.stack.shift().first();
                        continue;
                    }

                    if (difference > 0) {
                        $(slider).val(value + 1);
                        difference--;
                    }
                    else {
                        $(slider).val(value - 1);
                        difference++;
                    }
                }
            },
            count: function() {
                return self.find('.pools .pool .slider').length;
            }
        };

        var addButtonShowOrHide = function() {
            var addButton = self.find('.add');

            // if (options['pools'].length === selected.length()) {
            //if (selected.length() >= 3) {
            if (selected.length() >= options['maxPools']) {
                addButton.hide();
            }
            else {
                addButton.show();
            }
        };

        var saveButtonShowOrHide = function() {
            var addButton = self.find('.save');

            if (sliders.count()) {
                addButton.show();
            }
            else {
                addButton.hide();
            }
        };

        var sliderEnableOrDisable = function() {
            if (self.find('.pools .pool .slider').length === 1) {
                self.find('.pools .pool .slider').attr('disabled', true);
            }
            else {
                self.find('.pools .pool .slider').removeAttr('disabled');
            }
        };

        var addPool = function(id, sliderPercentageValue) {
            // append template
            self.find('.pools').append(self.find('.template').html());

            var pool = self.find('.pools .pool').last();

            // populate select with first available option
            var select = pool.find('select');
            var item = id ? available.byId(id) : available.first();
            select.append('<option value="' + item.id + '">' + item.title + '</option>');

            // add slider
            var percentage = 0;

            self.find('.slider').each(function() {
                percentage += parseInt($(this).val()) || 0;
            });

            var slider = pool.find('.slider');

            slider.noUiSlider({
                animate: false,
                start: sliderPercentageValue || (100 - percentage),
                step: 1,
                range: {
                    min: 0,
                    max: 100
                },
                format: wNumb({
                    decimals: 0,
                    postfix: '%'
                })
            });

            slider.Link('lower').to(pool.find('.slider-value'));

            slider.on({
                slide: function() {
                    sliders.stack.add(this);
                    sliders.autobalance(this);
                }
            });

            sliders.stack.add(slider.get(0));
        };

        var refreshSelectBoxes = function() {
            // refresh each select
            self.find('.pools select').each(function() {
                var select = $(this);

                // remove every option except selected one
                $('option', this).each(function() {
                    var option = $(this);
                    if (select.val() === option.val()) return;
                    option.remove();
                });

                // add all available ones
                $.each(available.all(), function(index, item) {
                    select.append('<option value="' + item.id + '">' + item.title + '</option>');
                });
            });
        };

        // if user has already some settings
        if (options['set']) {
            for (var i = 0; i < options['set'].length; i++) {
                addPool(options['set'][i].id, options['set'][i].percentage);
            }

            refreshSelectBoxes();
            sliderEnableOrDisable();
        }

        // first time initialization
        addButtonShowOrHide();
        saveButtonShowOrHide();

        // add button click event listener
        this.find('.add').click(function() {
            addPool();
            refreshSelectBoxes();
            addButtonShowOrHide();
            saveButtonShowOrHide();
            sliderEnableOrDisable();
        });

        // select change event listener
        $(this).on('change', '.pools select', refreshSelectBoxes);

        // remove button click event listener
        $(this).on('click', '.pools .remove', function() {
            $(this).parents('.pool').remove();
            refreshSelectBoxes();
            sliders.stack.refresh();
            sliders.autobalance();
            addButtonShowOrHide();
            saveButtonShowOrHide();
            sliderEnableOrDisable();
        });

        $(this).on('click', '.save', function(event) {
            event.preventDefault();

            var button = $(this);

            button.button('loading');

            var data = {
                pools: [],
                custom: options.custom || null
            };

            self.find('.pools .pool').each(function() {
                var id = $(this).find('select').val();
                var percentage = parseInt($(this).find('.slider').val());
                data.pools.push({
                    id: id,
                    percentage: percentage
                });
            });

            var fields = ['_method', 'data[_Token][key]', 'data[_Token][fields]', 'data[_Token][unlocked]'];

            for (var field = 0, fieldsCount = fields.length; field < fieldsCount; field++) {
                data[fields[field]] = self.find('[name="' + fields[field] + '"]').val();
            }

            $.post(options.url, data, function(data) {
                button.button('reset');

                if(options.success) {
                    options.success(data);
                }
            });
        });
    }
}(jQuery));
