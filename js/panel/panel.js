$(document).ready(function () {
    $('form.one-submit').submit(function (e) {
        var submitBtn = $(this).find('[type=submit]');
        var spinner = '<i class="fa fa-spinner fa-pulse fa-fw"></i>';
        submitBtn.attr('disabled', true);
        submitBtn.html(spinner + ' ' + submitBtn.html());
    });

    // survey starts
    $("#points a").on("click", function () {
        $("#points a").removeClass('active');
        $("#survey #send").prop('disabled', false);
        $(this).addClass('active');

        var points = $('#points a.active').index();
        $("input[name*='points']").val(points);
    });

    $("#survey .close").on("click", function () {
        $("#survey").remove();
    });

    $("#survey #send").on("click", function () {
        $("#survey #send").prop('disabled', true);
        $("#survey #skip").prop('disabled', true);
        $.ajax({
            url: "/panel/survey",
            type: "POST",
            dataType: "JSON",
            async : false,
            data: $("#survey form").serialize(),
            success: function (data) {
                $("#survey").remove();
                if (data.status === "success") {
                    $("#panelindex").prepend(
                        '<div class="alert alert-success alert-dismissible" role="alert">' +
                        '<button type="button" class="close" data-dismiss="alert">' +
                        '<span aria-hidden="true">×</span></button>' +
                        data.message +
                        '</div>');
                }
                if (data.status === "fail") {
                    $("#panelindex").prepend(
                        '<div class="alert alert-danger alert-dismissible" role="alert">' +
                        '<button type="button" class="close" data-dismiss="alert">' +
                        '<span aria-hidden="true">×</span></button>' +
                        data.message +
                        '</div>');
                }
            },
            error: function () {
                $("#survey").remove();
            }
        });
        return false;
    });

    $("#survey textarea").on("keyup", function () {
        var len = this.value.length;
        if (len >= 1000) {
            this.value = this.value.substring(0, 1000);
            $('#charNum').text(0);
            $('#textSize').addClass('text-danger');
        } else {
            $('#charNum').text(1000 - len);
            $('#textSize').removeClass('text-danger');
        }
    });

    $("#survey #skip").on("click", function () {
        $("#survey #send").prop('disabled', true);
        $("#survey #skip").prop('disabled', true);
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: "/panel/surveyskip",
            async : false,
            success: function () {
                $("#survey").remove();
            },
            error: function () {
                $("#survey").remove();
            }
        });
        return false;
    });
    // survey ends

    $('body').tooltip({
        selector: '[rel="tooltip"]'
    }).popover({
        selector: '[rel="popover"]'
    });

    $(".hide-on-click").click(function() {
        $(this).hide();
    });

    $(".link-trigger").on("click", function () {
        var self = $(this);
        var target = $(self.data("target"));
        var link = "";
        var prefix = self.data("prefix");
        if (typeof prefix == "undefined") prefix = "";
        else prefix += "/";

        if ($(target.html()).is("a")) {
            link = document.createElement("a");
            link.href = $(target.html()).attr("href");
            target.html(link.pathname.substring(1).replace(prefix, ""));
            removeTextSelections();
        } else {
            link = location.protocol + "//" + location.host + "/" + prefix + target.text();
            target.html('<a href="' + link + '">' + link + '</a>');
            var a = target.children().first();
            a.popover({
                placement: 'auto',
                content: 'Press Ctrl+C (Command+C on Mac) to copy',
                trigger: 'focus'
            });
            a.focus();
            a = document.getElementById(self.data("target").substring(1)).children[0];
            selectElementText(a);
        }
    });

    setTimeout(function() {
        $('.alert-auto-hide').fadeOut();
    }, 10000);

    function withdrawModal (withdrawForm, currentInput, amountInput, remainingInput, commissionInput, minimumInput, maximumInput) {
        // submit button
        var submitButton = withdrawForm.find('button[type="submit"]');

        // init slider
        var slider = withdrawForm.find('.slider');
        slider.noUiSlider({
            start: [0],
            range: {
                'min': 0,
                'max': 100
            }
        });

        // helpers
        var currentValue = function() {
            return parseFloat(currentInput.val());
        };

        var amountValue = function () {
            var amount = amountInput.val();
            if (!amount) {
                return 0;
            }

            return parseFloat(amountInput.val());
        };

        var remainingValue = function () {
            var remaining = remainingInput.val();
            if (!remaining) {
                return 0;
            }

            return parseFloat(remainingInput.val());
        };

        var commissionValue = function() {
            return parseFloat(commissionInput.val());
        };

        var minimumValue = function() {
            return parseFloat(minimumInput.val());
        };

        var maximumValue = function() {
            return parseFloat(maximumInput.val());
        };

        var validate = function() {
            var current = currentValue();
            var amount = amountValue();
            var commission = commissionValue();
            var minimum = minimumValue();
            var maximum = maximumValue();

            if(current < amount){
                return false;
            }

            if (current > maximum + commission) {
                return !(!amount || amount < minimum || amount > maximum);
            }
            return !(!amount || amount < minimum || amount > (maximum - commission));

        };

        var check = function() {
            if (validate()) {
                submitButton.attr('disabled', false);
            } else {
                submitButton.attr('disabled', true);
            }
        };

        // calculation
        var calculateRemaining = function() {
            var amount = amountValue();
            var commission = commissionValue();
            var maximum = maximumValue();
            var remaining = maximum - amount - commission;
            if (currentValue() > maximum + commission) {
                remaining = maximum - amount;
            }

            if (remaining < 0) {
                remaining = 0;
                amountInput.val(maximum);
            }

            if (isNaN(remaining)) {
                remaining = maximum;
            }
            return remaining;
        };

        var calculateAmount = function() {
            var remaining = remainingValue();
            var maximum = maximumValue();
            var amount = maximum - remaining - commissionValue();
            if (currentValue() > maximum + commissionValue()) {
                amount = maximum - remaining;
            }

            if (amount < 0) {
                amount = 0;
                remainingInput.val(maximum);
            }

            if (isNaN(amount)) {
                amount = maximum;
            }

            return amount;
        };

        // events
        var amountChanged = function () {
            remainingInput.val((calculateRemaining().toFixed(8)));
            var percentage = amountValue() / maximumValue() * 100;
            slider.val(percentage);
            check();
        };

        var remainingChanged = function () {
            amountInput.val((calculateAmount().toFixed(8)));
            var percentage = amountValue() / maximumValue() * 100;
            slider.val(percentage);
            check();
        };

            var sliderChanged = function() {
            var percentage = parseFloat(slider.val());
            var amount = maximumValue() * (percentage / 100) - commissionValue();
            if (currentValue() > maximumValue() + commissionValue()) {
                amount = maximumValue() * (percentage / 100);
            }
            if (amount <= 0) {
                amount = 0;
            }
            amountInput.val(Number(amount.toFixed(8)));

            var remaining = calculateRemaining();
            remainingInput.val(Number(remaining.toFixed(8)));
            check();
        };

        // set input listeners
        amountInput.on("paste", function (event) {
            event.preventDefault();
        });

        remainingInput.on("paste", function (event) {
            event.preventDefault();
        });

        amountInput.on("keypress keyup blur", function (event) {
            if (validateInputData($(this), event)) {
                amountChanged();
            } else {
                event.preventDefault();
            }

        });

        remainingInput.on("keypress keyup blur", function (event) {
            if (validateInputData($(this), event)) {
                remainingChanged();
            } else {
                event.preventDefault();
            }
        });

        function validateInputData(input, e) {
            input.val(input.val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || input.val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                return false;
            }
            return true;
        }


        // set slider listeners
        slider.on({
            slide: sliderChanged
        });
    }

    // withdraw modals
    // BTC
    var withdrawForm = $('#withdraw-form');
    var currentInput = withdrawForm.find('#WithdrawCurrent');
    var amountInput = withdrawForm.find('#WithdrawAmount');
    var remainingInput = withdrawForm.find('#WithdrawRemaining');
    var commissionInput = withdrawForm.find('#WithdrawCommission');
    var minimumInput = withdrawForm.find('#WithdrawMinimum');
    var maximumInput = withdrawForm.find('#WithdrawMaximum');
    withdrawModal(withdrawForm, currentInput, amountInput, remainingInput, commissionInput, minimumInput, maximumInput);

    // ETH
    withdrawForm = $('#eth-withdraw-form');
    currentInput = withdrawForm.find('#EthWithdrawCurrent');
    amountInput = withdrawForm.find('#EthWithdrawAmount');
    remainingInput = withdrawForm.find('#EthWithdrawRemaining');
    commissionInput = withdrawForm.find('#EthWithdrawCommission');
    minimumInput = withdrawForm.find('#EthWithdrawMinimum');
    var maximumInput = withdrawForm.find('#EthWithdrawMaximum');
    withdrawModal(withdrawForm, currentInput, amountInput, remainingInput, commissionInput, minimumInput, maximumInput);

    // DASH
    withdrawForm = $('#dash-withdraw-form');
    currentInput = withdrawForm.find('#DashWithdrawCurrent');
    amountInput = withdrawForm.find('#DashWithdrawAmount');
    remainingInput = withdrawForm.find('#DashWithdrawRemaining');
    commissionInput = withdrawForm.find('#DashWithdrawCommission');
    minimumInput = withdrawForm.find('#DashWithdrawMinimum');
    var maximumInput = withdrawForm.find('#DashWithdrawMaximum');
    withdrawModal(withdrawForm, currentInput, amountInput, remainingInput, commissionInput, minimumInput, maximumInput);

    // DASH
    withdrawForm = $('#zec-withdraw-form');
    currentInput = withdrawForm.find('#ZecWithdrawCurrent');
    amountInput = withdrawForm.find('#ZecWithdrawAmount');
    remainingInput = withdrawForm.find('#ZecWithdrawRemaining');
    commissionInput = withdrawForm.find('#ZecWithdrawCommission');
    minimumInput = withdrawForm.find('#ZecWithdrawMinimum');
    var maximumInput = withdrawForm.find('#ZecWithdrawMaximum');
    withdrawModal(withdrawForm, currentInput, amountInput, remainingInput, commissionInput, minimumInput, maximumInput);

    // pay with payeer
    (function() {
        var button = $('#pay-with-payeer');
        var form = $('#payeer');

        button.click(function(event) {
            event.preventDefault();
            button.button('loading');
            var href = button.attr('href');
            var parser = function(data) {
                // get needed data and create input elements
                for (var key in data) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = key;
                    input.value = data[key];
                    form.append(input);
                }

                form.submit();
            };
            $.ajaxSetup({
                cache: false,
                error: function(response, e, type) {
                    var text = response.responseText;
                    var json = response.responseJSON;

                    if (text.length > 0 && typeof json == 'object') parser(json);
                    else console.log('payeer creation error', type);
                }
            });
            $.get(href, parser, 'json');
        });
    })();

    // google analytics event tracking
    (function() {
        // collect data about clicks
        $('a[data-gae]').click(function(event) {
            if (typeof _gaq === 'undefined') return true;

            event.preventDefault();

            var self = $(this);

            // get event data and send it
            var category = self.data('gae-category') || 'Panel';
            var action = self.data('gae-action') || 'Undefined action';
            var label = self.data('gae-label') || 'Undefined label';
            _gaq.push(['_trackEvent', category, action, label]);

            setTimeout(function() {
                document.location = self.attr('href');
            }, 200);
        });
    })();

    var $index = $("#panelindex");

    function calculateTotalDed() {
        var withdrawAmount = parseFloat($("#WithdrawAmount").val());
        var withdrawCommission = parseFloat($("#WithdrawCommission").val());
        var totalDed = withdrawAmount + withdrawCommission;
        $("#TotalDed").text(parseFloat(totalDed.toFixed(8)));
    }

    calculateTotalDed();

    $("#WithdrawAmount").keyup(calculateTotalDed);

    $('#birthday').datepicker(
        {
            endDate: '-18y',
            startDate: '-100y',
            autoclose: true,
            format: "dd.mm.yyyy",
            startView: 2,
        }
    );

    // auto collapse if #hash set in url
    if (window.location.hash) {
        var hash = window.location.hash.slice(1);
        var rows = JSON.parse(decodeURIComponent($('#types').data('json')));
        rows.forEach(function(row) {
            if (row != hash) {
                $("#" + row + "-row").removeClass("in");
                $("#" + row + " a.header-toggle").addClass("collapsed");
            }
        });
    }

    $index.find(".purchase-toggle").on("click", function() {
        var $header = $(this).siblings(".header-toggle").first();
        var $target = $($header.data("target"));

        if ($target.hasClass("in") === false) $target.collapse("show");
    });
    $index.find(".header-toggle").on("click", function() {
        var $purchase = $(this).siblings(".purchase-toggle").first();
        var $target = $($purchase.data("target"));

        if ($target.hasClass("in") !== false) $target.collapse("hide");
    });
    $index.find(".code-target").hide();

    $index.find(".code-toggle").on("click", function() {
        $(this).closest(".code-toggle-parent").hide();
        $("#" + $(this).attr("id") + "-code").fadeIn("fast");
    });

    var sliders = $index.find(".uislider");

    sliders.each(function () {
        var self = $(this);
        var $btn = $("#" + self.attr("id") + "-btn");
        self.noUiSlider({
            start: self.data("start"),
            behaviour: "tap",
            connect: "lower",
            step: self.data("step"),
            range: {
                "min": self.data("min"),
                "25%": [self.data("max") * 0.01, self.data("step") * 10],
                "50%": [self.data("max") * 0.05, self.data("step") * 50],
                "75%": [self.data("max") * 0.25, self.data("step") * 100],
                "max": self.data("max")
            },
            format: wNumb({
                decimals: 0,
                encoder: function (number) {
                    return number >= 1000 ? number / 1000 : number;
                },
                edit: function (text, value) {
                    if (value == 0) $btn.prop("disabled", true).addClass("disabled");
                    else $btn.prop("disabled", false).removeClass("disabled");

                    var baseUnit = 'MH/s';
                    var kUnit = 'GH/s';

                    if (self.data('unit') == 'giga') {
                        baseUnit = 'GH/s';
                        kUnit = 'TH/s';
                    }
                    else if (self.data('unit') == 'kilo') {
                        baseUnit = 'KH/s';
                        kUnit = 'MH/s';
                    }
                    else if (self.data('unit') == 'hash') {
                        baseUnit = 'H/s';
                        kUnit = 'KH/s';
                    }
                    else if (self.data('unit') == 'milli') {
                        baseUnit = 'mH/s';
                        kUnit = 'H/s';
                    }

                    var postfix = value >= 1000 ? kUnit : baseUnit;
                    var price = parseFloat(Math.round(value) * $("#" + self.data("type") + "price").val()).toFixed(2);

                    $("." + self.attr("id") + "-usd").text(price);
                    $("." + self.attr("id") + "-btc").text((parseFloat(price) * parseFloat($("#usdprice").val())).toFixed(8));

                    $("." + self.attr("id") + "-total").val(Math.round(value));

                    return (value >= 1000 ? parseFloat((value / 1000).toFixed(2)) : text) + " " + postfix;
                }
            })
        });

        self.Link("lower").to("-inline-<div></div>", function (value) {
            $(this).attr("class", "tooltip fade bottom in");
            $(this).attr("role", "tooltip");
            $(this).attr("style", "top:22px;left:-22px;width:76px;");
            $(this).html(
                "<div style=\'left: 50%;\' class=\'tooltip-arrow\'></div><div class=\'tooltip-inner\'>" + value + "</div>"
            );
        });

        var step = self.data("step");
        self.noUiSlider_pips({
            mode: "steps",
            density: step * 10,
            filter: function (value) {
                if (value > (2500 * step)) {
                    return value % (self.data("step") * 1000) == 0 ? 1 : 0;
                } else if (value >= (500 * step)) {
                    return value % (self.data("step") * 500) == 0 ? 1 : value % (self.data("step") * 250) == 0 ? 2 : 0;
                } else if (value >= (100 * step)) {
                    return value % (self.data("step") * 100) == 0 ? 1 : value % (self.data("step") * 25) == 0 ? 2 : 0;
                } else return value % (self.data("step") * 100) == 0 ? 1 : value % (self.data("step") * 10) == 0 ? 2 : 0;
            },
            format: wNumb({
                decimals: 0,
                edit: function (number) {
                    if (number >= 1000) return number / 1000;
                    return number;
                }
            })
        });
    });

    $(".noUi-pips").addClass("hidden-xs hidden-sm");
});

// Selects text inside an element node.
function selectElementText(el) {
    removeTextSelections();
    var range = new Range();

    if (document.selection) {
        range = document.body.createTextRange();
        range.moveToElementText(el);
        range.select();
    } else if (window.getSelection) {
        range = document.createRange();
        range.selectNode(el);
        window.getSelection().addRange(range);
    }
}

// Deselects all text in the page.
function removeTextSelections() {
    if (document.selection) document.selection.empty();
    else if (window.getSelection) window.getSelection().removeAllRanges();
}

function calcSlidePercentage(self) {
    var values = self.val();

    var $minselect = $("#" + self.attr("id") + "-min-selection");
    var $maxselect = $("#" + self.attr("id") + "-max-selection");
    var $leftselect = $("#" + self.attr("id") + "-leftover-selection");

    var $min = $("#" + self.attr("id") + "-min");
    var $max = $("#" + self.attr("id") + "-max");
    var $left = $("#" + self.attr("id") + "-leftover");

    var $confIdZ = $("." + self.attr("id") + "-confirm-id-0");
    var $confPZ = $("." + self.attr("id") + "-confirm-percentage-0");
    var $confIdO = $("." + self.attr("id") + "-confirm-id-1");
    var $confPO = $("." + self.attr("id") + "-confirm-percentage-1");
    var $confIdT = $("." + self.attr("id") + "-confirm-id-2");
    var $confPT = $("." + self.attr("id") + "-confirm-percentage-2");

    // Calculate the pool percentages
    if ($.isArray(self.val())) {

        if ($minselect.val() != "" && $minselect.val() != 0) {
            $min.text(values[0]);
            $confIdZ.val($minselect.val());
            $confPZ.val(values[0]);
        } else {
            $min.text(0);
            $confIdZ.val(0);
            $confPZ.val(0);
        }
        if ($maxselect.val() != "" && $maxselect.val() != 0) {
            $max.text(values[1] - values[0]);
            $confIdO.val($maxselect.val());
            $confPO.val(values[1] - values[0]);
        } else {
            $max.text(0);
            $confIdO.val(0);
            $confPO.val(0);
        }
        if ($leftselect.val() != "" && $leftselect.val() != 0) {
            $left.text(100 - values[1]);
            $confIdT.val($leftselect.val());
            $confPT.val(100 - values[1]);
        } else {
            $left.text(0);
            $confIdT.val(0);
            $confPT.val(0);
        }
    } else {
        if ($minselect.val() != "" && $minselect.val() != 0) {
            $min.text(parseInt(values));
            $confIdZ.val($minselect.val());
            $confPZ.val(parseInt(values));
        } else {
            $min.text(0);
            $confIdZ.val(0);
            $confPZ.val(0);
        }
        // If not array, only 2 pools selected - one or the other should be available
        if ($maxselect.val() != "" && $maxselect.val() != 0) {
            $max.text(100 - parseInt(values));
            $confIdO.val($maxselect.val());
            $confPO.val(100 - parseInt(values));
        } else {
            $max.text(0);
            $confIdO.val(0);
            $confPO.val(0);
        }
        if ($leftselect.val() != "" && $leftselect.val() != 0) {
            $left.text(100 - parseInt(values));
            $confIdT.val($leftselect.val());
            $confPT.val(100 - parseInt(values));
        } else {
            $left.text(0);
            $confIdT.val(0);
            $confPT.val(0);
        }
    }
}
