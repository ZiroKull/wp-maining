(function($) {
    'use strict';

    $(document).ready(initScript);

    function initScript() {

        wpcrlValidateAndProcessLoginForm();
        // validating registration form request
        wpcrlValidateAndProcessRegisterForm();
        // validating reset password form request
        wpcrlValidateAndProcessResetPasswordForm();

    }

    // Validate login form
    function wpcrlValidateAndProcessLoginForm() {
        $('#wpcrlLoginForm').formValidation({
            message: 'Это значение недействительно',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Email: {
                    validators: {
                        notEmpty: {
                            message: 'Email обязателен для заполнения'
                        },
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'Значение не является допустимым адресом электронной почты'
                        }
                    }
                },
                Password: {
                    validators: {
                        notEmpty: {
                            message: 'Пароль обязателен для заполнения'
                        },
                        stringLength: {
                            min: 6,
                            message: 'Пароль должен содержать более 6 символов.'
                        }
                    }
                }
            }
        });
    }

    // Validate registration form
    function wpcrlValidateAndProcessRegisterForm() {
        $('#wpcrlRegisterForm').formValidation({
            message: 'Это значение недействительно',
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Email: {
                    validators: {
                        notEmpty: {
                            message: 'Email обязателен для заполнения'
                        },
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'Значение не является допустимым адресом электронной почты'
                        }
                    }
                },
                Password: {
                    validators: {
                        notEmpty: {
                            message: 'Пароль обязателен для заполнения'
                        },
                        stringLength: {
                            min: 6,
                            message: 'Пароль должен содержать более 6 символов.'
                        }
                    }
                },
                ConfirmPassword: {
                    validators: {
                        notEmpty: {
                            message: 'Пароль обязателен для заполнения'
                        },
                        identical: {
                            field: 'Password',
                            message: 'Пароль и его подтверждение не совпадают'
                        },
                        stringLength: {
                            min: 6,
                            message: 'Пароль должен содержать более 6 символов.'
                        }
                    }
                }
            }
        });
    }

    // Validate reset password form
    //Neelkanth
    function wpcrlValidateAndProcessResetPasswordForm() {

        $('#wpcrlResetPasswordForm').formValidation({
            message: 'Это значение недействительно',
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Email: {
                    validators: {
                        notEmpty: {
                            message: 'Email обязателен для заполнения'
                        },
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'Значение не является допустимым адресом электронной почты'
                        }
                    }
                }
            }
        });
    }
})(jQuery);
