function initHome() {}

$(function(){
    var sliderPointer;
    var thsProfit = parseFloat($('#how-to-earn .thsProfit').val());
    var usdPerBtc = parseFloat($('#how-to-earn .usdPerBtc').val());
    var thsProfitUsd = thsProfit * usdPerBtc - 1;
    var sumContainer = $('#how-to-earn .sum.day');
    var sumMonthContainer = $('#how-to-earn .sum.month');
    var sumYearContainer = $('#how-to-earn .sum.year');
    var sumYear5Container = $('#how-to-earn .sum.year5');

    $('#how-to-earn .calc .single-slider').jRange({
        from: 0,
        to: 10000,
        step: 1,
        format: '%s',
        width: 930,
        showLabels: false,
        showScale: false,
        onstatechange: function(value) {
            sliderPointer.text(value + ' GH/s');
            setProfit(value);
        }
    });

    setTimeout(function() {
        sliderPointer = $('#how-to-earn .calc .slider-container .pointer');
        sliderPointer.text('1000 GH/s');
        setProfit(1000);
    }, 500);
    
    var settings = {
		slideWidth:360,
		minSlides:1,
		maxSlides: 3,
		moveSlides:1,
		speed:600,
		useCSS: false,
		pager:!1,
		pause:3000,
		infiniteLoop: true,
		autoControls: false,
		controls:!0,
		slideMargin: 40,
		touchEnabled: false,
		auto:!0
		//hideControlOnEnd: true
	};

	$( '.bxslider' ).bxSlider( settings );

    function getProfit(ghs) {
        var ths = ghs / 1000;
        return thsProfitUsd * ths;
    }

    function setProfit(ghs) {
        var profit = getProfit(ghs);
        sumContainer.text(profit.toFixed(2) + ' $');
        sumMonthContainer.text((profit * 30).toFixed(2) + ' $');
        sumYearContainer.text((profit * 365).toFixed(2) + ' $');
        sumYear5Container.text((profit * 365 * 5).toFixed(2) + ' $');
    }
});

(function($) {
    'use strict';

    $.fn.scrollTrigger = function(args) {
        var self = this;

        var trigger = $(args.trigger);
        var doc = $(document);
        var navbar = $('.navbar');

        if (!trigger.length) {
            return;
        }

        var refresh = function() {
            if (trigger.position().top - navbar.outerHeight() < doc.scrollTop()) {
                if (!self.hasClass('navbar-fixed-top')) {

                    $('body').css('padding-top', navbar.outerHeight() + 'px');
                    self.addClass('navbar-fixed-top navbar-inverse').removeClass('navbar-custom');
                }
            } else {
                $('body').css('padding-top', '0');
                self.removeClass('navbar-fixed-top navbar-inverse').addClass('navbar-custom');
            }
        };

        refresh();

        $(document).scroll(refresh);
        $(window).resize(refresh);

        return this;
    };

    $(document).ready(function() {
        $('.navbar').scrollTrigger({
            trigger: '.scroll-trigger'
        });
    });
}(jQuery));

(function($) {
    'use strict';

    $(document).ready(function() {
        var navBar = $('.navbar');

        $('.nav .dropdown').on('shown.bs.dropdown', function () {
            navBar.addClass('nav-dropdown');
        });

        $('.nav .dropdown').on('hidden.bs.dropdown', function () {
            navBar.removeClass('nav-dropdown');
        });

        $('#birthday').datepicker(
            {
                endDate: '-18y',
                startDate: '-100y',
                autoclose: true,
                format: "dd.mm.yyyy",
                startView: 2,
            }
        );
    });
}(jQuery));

$(document).ready(function () {
    $('form.one-submit').submit(function (e) {
        var submitBtn = $(this).find('[type=submit]');
        var spinner = '<i class="fa fa-spinner fa-pulse fa-fw"></i>';
        submitBtn.attr('disabled', true);
        submitBtn.html(spinner + ' ' + submitBtn.html());
    });
});
