<?php
/**
 * Template Name: Panel Materials
 */
 
get_header('panel'); 
?>
        
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize btn btn-primary " href="#">
                    <i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/logout" class="no-mobile hidden-xs">
                        <i class="fa fa-sign-out"></i> Выйти </a>
                    </li>
                </ul>
            </nav>
        </div>
        
        <div class="wrapper wrapper-content">
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">×</span><span class="sr-only">Закрыть</span>
                </button>
                Если Вы не видите баннеры на этой странице, пожалуйста, отключите все блокировки на JavaScript и рекламу
            </div>
            <h1>English</h1>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>160x600</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/realno_160x600_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/realno_160x600_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/f1_160x600_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/f1_160x600_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/h3_160x600_1btn_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/h3_160x600_1btn_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/h4_160x600_2btn_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/h4_160x600_2btn_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hashflare_m_160x600_eng.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hashflare_m_160x600_eng.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hashflare_w_160x600_eng.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hashflare_w_160x600_eng.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hi160x600Eng-Ko.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hi160x600Eng-Ko.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/i1_160x600_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/i1_160x600_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/mario_beg_160x600_eng_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/mario_beg_160x600_eng_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/profit_160x600_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/profit_160x600_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>240x400</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/realno_240x400_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/realno_240x400_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/ET-240x400Eng-Ko.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/ET-240x400Eng-Ko.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hashflare_m_240x400_eng.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hashflare_m_240x400_eng.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hashflare_w_240x400_eng.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hashflare_w_240x400_eng.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/profit_240x400_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/profit_240x400_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/mario_beg_240x400_eng_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/mario_beg_240x400_eng_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>250x250</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/realno_250x250_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/realno_250x250_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/f1_250x250_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/f1_250x250_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/i1_250x250_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/i1_250x250_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/profit_250x250_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/profit_250x250_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/mario_beg_250x250_eng_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/mario_beg_250x250_eng_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>300x250</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/realno_300x250_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/realno_300x250_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/ET-300x250Eng-Ko.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/ET-300x250Eng-Ko.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/f1_300x250_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/f1_300x250_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/h3_300x250_1btn_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/h3_300x250_1btn_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/h4_300x250_2btn_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/h4_300x250_2btn_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hashflare_m_300x250_eng.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hashflare_m_300x250_eng.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hashflare_w_300x250_eng.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hashflare_w_300x250_eng.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hi300x250Eng-Ko.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hi300x250Eng-Ko.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/i1_300x250_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/i1_300x250_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/mario_beg_300x250_eng_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/mario_beg_300x250_eng_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/profit_300x250_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/profit_300x250_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>336x280</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/f1_336x280_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/f1_336x280_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/profit_336x280_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/profit_336x280_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>468x60</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/realno_468x60_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/realno_468x60_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/ET-468x60Eng-Ko.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/ET-468x60Eng-Ko.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/f1_468x60_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/f1_468x60_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/h3_468x60_1btn_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/h3_468x60_1btn_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/h4_468x60_2btn_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/h4_468x60_2btn_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hi468x60Eng-Ko.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hi468x60Eng-Ko.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/i1_468x60_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/i1_468x60_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/mario_beg_468x60_eng_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/mario_beg_468x60_eng_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/profit_468x60_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/profit_468x60_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>600x200</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/realno_600x200_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/realno_600x200_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/i1_600x200_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/i1_600x200_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/mario_beg_600x200_eng_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/mario_beg_600x200_eng_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/profit_600x200_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/profit_600x200_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>728x90</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/realno_728x90_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/realno_728x90_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/ET-728x90Eng-Ko.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/ET-728x90Eng-Ko.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/f1_728x90_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/f1_728x90_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/h3_728x90_1btn_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/h3_728x90_1btn_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/h4_728x90_2btn_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/h4_728x90_2btn_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hashflare_m_728x90_eng.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hashflare_m_728x90_eng.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hashflare_w_728x90_eng.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hashflare_w_728x90_eng.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/Hi728x90Eng-Ko.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/Hi728x90Eng-Ko.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/i1_728x90_en.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/i1_728x90_en.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/mario_beg_728x90_eng_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/mario_beg_728x90_eng_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/profit_728x90_eng.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/profit_728x90_eng.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>1280x628</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/1280+628_2_eng.png" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/1280+628_2_eng.png" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/1280x628_1_eng.png" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/1280x628_1_eng.png" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/FB3_en.jpg" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/FB3_en.jpg" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/FB4_en.jpg" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/FB4_en.jpg" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/en/FB1-3klicks-Ko-ENG.jpg?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/en/FB1-3klicks-Ko-ENG.jpg?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <h1>Русский</h1>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>160x600</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/realno_160x600_rus.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/realno_160x600_rus.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/f1_160x600_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/f1_160x600_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hashflare_m_160x600_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hashflare_m_160x600_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hashflare_w_160x600_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hashflare_w_160x600_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hi160x600Ru-Ko.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hi160x600Ru-Ko.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/i1_160x600_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/i1_160x600_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/mario_beg_160x600_ru_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/mario_beg_160x600_ru_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/profit_160x600_ru.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/profit_160x600_ru.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>240x400</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/realno_240x400_rus.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/realno_240x400_rus.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/ET-240x400Rus-Ko.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/ET-240x400Rus-Ko.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hashflare_m_240x400_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hashflare_m_240x400_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hashflare_w_240x400_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hashflare_w_240x400_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/mario_beg_240x400_ru_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/mario_beg_240x400_ru_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/profit_240x400_ru.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/profit_240x400_ru.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>250x250</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/realno_250x250_rus.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/realno_250x250_rus.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/f1_250x250_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/f1_250x250_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/g1_250x250_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/g1_250x250_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/h1_250x250_2btn_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/h1_250x250_2btn_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/h2_250x250_1btn_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/h2_250x250_1btn_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hi250x250Ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hi250x250Ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/i1_250x250_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/i1_250x250_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/mario_beg_250x250_ru_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/mario_beg_250x250_ru_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/profit_250x250_ru.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/profit_250x250_ru.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>300x250</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/realno_300x250_rus.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/realno_300x250_rus.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/ET-300x250Rus-Ko.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/ET-300x250Rus-Ko.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/f1_300x250_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/f1_300x250_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/g1_300x250_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/g1_300x250_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/h1_300x250_2btn_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/h1_300x250_2btn_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/h2_300x250_1btn_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/h2_300x250_1btn_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hashflare_m_300x250_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hashflare_m_300x250_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hashflare_w_300x250_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hashflare_w_300x250_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hi300x250Ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hi300x250Ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/i1_300x250_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/i1_300x250_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/mario_beg_300x250_ru_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/mario_beg_300x250_ru_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/profit_300x250_ru.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/profit_300x250_ru.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>336x280</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/f1_336x280_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/f1_336x280_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/profit_336x280_ru.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/profit_336x280_ru.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>468x60</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/realno_468x60_rus.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/realno_468x60_rus.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/f1_468x60_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/f1_468x60_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hi468x60Ru-Ko.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hi468x60Ru-Ko.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/i1_468x60_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/i1_468x60_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/mario_beg_468x60_ru_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/mario_beg_468x60_ru_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/profit_468x60_ru.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/profit_468x60_ru.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>600x200</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/realno_600x200_rus.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/realno_600x200_rus.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/g1_600x200_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/g1_600x200_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/h1_600x200_2btn_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/h1_600x200_2btn_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/h2_600x200_1btn_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/h2_600x200_1btn_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hi600x200Ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hi600x200Ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/i1_600x200_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/i1_600x200_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/mario_beg_600x200_ru_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/mario_beg_600x200_ru_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/profit_600x200_ru.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/profit_600x200_ru.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>728x90</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/realno_728x90_rus.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/realno_728x90_rus.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/ET-728x90Rus-Ko.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/ET-728x90Rus-Ko.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/f1_728x90_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/f1_728x90_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hashflare_m_728x90_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hashflare_m_728x90_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hashflare_w_728x90_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hashflare_w_728x90_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/Hi728x90Ru-K.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/Hi728x90Ru-K.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/i1_728x90_ru.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/i1_728x90_ru.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/mario_beg_728x90_ru_J.gif" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/mario_beg_728x90_ru_J.gif" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/profit_728x90_ru.gif?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/profit_728x90_ru.gif?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title collapse-link" style="cursor: pointer;">
                    <h5>1280x628</h5>
                    <div class="ibox-tools">
                        <a class="">
                        <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content text-center" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/1280+628_2_ru.png" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/1280+628_2_ru.png" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/1280x628_1_ru.png" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/1280x628_1_ru.png" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/FB3.jpg" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/FB3.jpg" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/FB4.jpg" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/FB4.jpg" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                        <div class="col-md-4">
                            <img src="https://cdn.hashflare.eu/banners/ru/FB1-3klicks-Ko.jpg?v=2" alt="" class="m-b" style="max-width:100%;border:1px solid #E7EAEC;">
                            <textarea class="form-control m-b-md" rows="5" readonly="">&lt;a href="https://hashflare.io/r/"&gt;&lt;img src="https://cdn.hashflare.eu/banners/ru/FB1-3klicks-Ko.jpg?v=2" alt="HashFlare"&gt;&lt;/a&gt;</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="footer">
            <div class="row">
                <div class="col-md-5">
                    Авторские права © HashCoins OÜ, Все права защищены. 2014-2018 
                </div>
                <div class="col-md-4">
                    Время на сервере: 13.10.2018 21:08:35 UTC 00:00 
                </div>
                <div class="col-md-3" style="text-align:right">
                    Последнее обновление: 07.09.2018 
                </div>
            </div>
        </div>
    </div>
	
<?php get_footer('mpanel'); ?>


