<?php
/**
 * Template Name: Panel Settings
 */
 
get_header('panel'); 
?>
        
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize btn btn-primary " href="#">
                    <i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/logout" class="no-mobile hidden-xs">
                        <i class="fa fa-sign-out"></i> Выйти </a>
                    </li>
                </ul>
            </nav>
        </div>
        
        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Профиль</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-horizontal">
                                        <div class="form-group required">
                                            <label for="UserEmail" class="col-sm-3 control-label control-label">Электронная почта</label>
                                            <div class="col-sm-9"><input name="data[User][email]" class="form-control disabled" disabled="disabled" maxlength="150" type="email" value="" id="UserEmail"><span class="help-block m-b-none text-warning">Если вы хотите изменить адрес Вашей электронной почты, пожалуйста, обратитесь в службу поддержки</span></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Avatar</label>
                                            <div class="col-sm-9">
                                                <p class="form-control-static">
                                                    Изображение пользователя работает через Gravatar, для настройки нажмите на ссылку - <a href="https://gravatar.com/" target="_blank">Gravatar</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                    <form action="/panel/settings" class="form-horizontal one-submit" id="user_btc_wallet" method="post" accept-charset="utf-8">
                                        <div style="display:none;"><input type="hidden" name="_method" value="PUT"><input type="hidden" name="data[_Token][key]" value="cd80871ece53627ca9c99b51ec93b0e093dcec20" id="Token385368957"></div>
                                        <input type="hidden" name="data[User][chk]" class="form-control" value="aXdZ+yhkHbhaw/TM78LykBJhCmvoLiRAaH9spf9eIps=" id="chk_btc">
                                        <div class="form-group">
                                            <label for="UserWallet" class="col-sm-3 control-label control-label">BTC Кошелёк</label>
                                            <div class="col-sm-9"><input name="data[User][wallet]" class="form-control" maxlength="64" type="text" id="UserWallet"><span class="help-block m-b-none text-warning">Смена адреса кошелька блокирует вывод средств на срок 2 недели после подтверждения, исходя из соображений безопасности</span><br><span class="help-block m-b-none text-info"><a target="_blank" href="https://hashflare.zendesk.com/hc/ru/articles/206691629">Как создать Bitcoin кошелек?</a></span></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-3">
                                                <button class="btn btn-primary" type="submit">Сохранить</button> 
                                            </div>
                                        </div>
                                        <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="4aeb9124136e6780b46d88a169938086fb7a7eb9%3AUser.chk" id="TokenFields1825489130"><input type="hidden" name="data[_Token][unlocked]" value="" id="TokenUnlocked1759630917"></div>
                                    </form>
                                    <hr>
                                    <form action="/panel/settings" class="form-horizontal one-submit" id="user_eth_wallet" method="post" accept-charset="utf-8">
                                        <div style="display:none;"><input type="hidden" name="_method" value="PUT"><input type="hidden" name="data[_Token][key]" value="cd80871ece53627ca9c99b51ec93b0e093dcec20" id="Token1534489636"></div>
                                        <input type="hidden" name="data[User][chk]" class="form-control" value="aXdZ+yhkHbhaw/TM78LykBJhCmvoLiRAaH9spf9eIps=" id="chk_eth">
                                        <div class="form-group">
                                            <label for="UserEthWallet" class="col-sm-3 control-label control-label">ETH Кошелёк</label>
                                            <div class="col-sm-9"><input name="data[User][eth_wallet]" class="form-control" maxlength="64" type="text" id="UserEthWallet"><span class="help-block m-b-none text-warning">Смена адреса кошелька блокирует вывод средств на срок 2 недели после подтверждения, исходя из соображений безопасности</span><br><span class="help-block m-b-none text-info"><a target="_blank" href="https://hashflare.zendesk.com/hc/ru/articles/207923705">Как создать Ethereum кошелек?</a></span></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-3">
                                                <button class="btn btn-primary" type="submit">Сохранить</button> 
                                            </div>
                                        </div>
                                        <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="c65640e737ef6194448528e621fafc5fc58f1319%3AUser.chk" id="TokenFields505924057"><input type="hidden" name="data[_Token][unlocked]" value="" id="TokenUnlocked2093044274"></div>
                                    </form>
                                    <hr>
                                    <form action="/panel/settings" class="form-horizontal one-submit" id="user_dash_wallet" method="post" accept-charset="utf-8">
                                        <div style="display:none;"><input type="hidden" name="_method" value="PUT"><input type="hidden" name="data[_Token][key]" value="cd80871ece53627ca9c99b51ec93b0e093dcec20" id="Token1688598222"></div>
                                        <input type="hidden" name="data[User][chk]" class="form-control" value="aXdZ+yhkHbhaw/TM78LykBJhCmvoLiRAaH9spf9eIps=" id="chk_dash">
                                        <div class="form-group">
                                            <label for="UserDashWallet" class="col-sm-3 control-label control-label">DASH Кошелёк</label>
                                            <div class="col-sm-9"><input name="data[User][dash_wallet]" class="form-control" maxlength="100" type="text" id="UserDashWallet"><span class="help-block m-b-none text-warning">Смена адреса кошелька блокирует вывод средств на срок 2 недели после подтверждения, исходя из соображений безопасности</span><br><span class="help-block m-b-none text-info"><a target="_blank" href="https://hashflare.zendesk.com/hc/ru/articles/213286065-%D0%9A%D0%B0%D0%BA-%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D1%82%D1%8C-DASH-%D0%BA%D0%BE%D1%88%D0%B5%D0%BB%D0%B5%D0%BA-">Как создать DASH кошелек?</a></span></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-3">
                                                <button class="btn btn-primary" type="submit">Сохранить</button> 
                                            </div>
                                        </div>
                                        <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="9fe3bf45473be944c4d5a4cbb9c795fef9594e3a%3AUser.chk" id="TokenFields787434134"><input type="hidden" name="data[_Token][unlocked]" value="" id="TokenUnlocked158394269"></div>
                                    </form>
                                    <hr>
                                    <form action="/panel/settings" class="form-horizontal one-submit" id="user_zec_wallet" method="post" accept-charset="utf-8">
                                        <div style="display:none;"><input type="hidden" name="_method" value="PUT"><input type="hidden" name="data[_Token][key]" value="cd80871ece53627ca9c99b51ec93b0e093dcec20" id="Token1202533309"></div>
                                        <input type="hidden" name="data[User][chk]" class="form-control" value="aXdZ+yhkHbhaw/TM78LykBJhCmvoLiRAaH9spf9eIps=" id="chk_zec">
                                        <div class="form-group">
                                            <label for="UserZcashWallet" class="col-sm-3 control-label control-label">ZEC Кошелёк</label>
                                            <div class="col-sm-9"><input name="data[User][zcash_wallet]" class="form-control" maxlength="100" type="text" id="UserZcashWallet"><span class="help-block m-b-none text-warning">Смена адреса кошелька блокирует вывод средств на срок 2 недели после подтверждения, исходя из соображений безопасности</span><br><span class="help-block m-b-none text-info"><a target="_blank" href="https://hashflare.zendesk.com/hc/ru/articles/213534269-Zcash-%D0%B0%D0%B4%D1%80%D0%B5%D1%81-%D0%B2%D1%8B%D0%B2%D0%BE%D0%B4%D1%8B-%D0%B8-%D0%BA%D0%BE%D1%88%D0%B5%D0%BB%D1%91%D0%BA">Как создать Zcash кошелек?</a></span></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-3">
                                                <button class="btn btn-primary" type="submit">Сохранить</button> 
                                            </div>
                                        </div>
                                        <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="bb757d44d1a8e574effb856f838202fc2dfc07ab%3AUser.chk" id="TokenFields1986377370"><input type="hidden" name="data[_Token][unlocked]" value="" id="TokenUnlocked1570740704"></div>
                                    </form>
                                    <hr>
                                </div>
                            </div>
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Пароль</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <form action="/panel/settings" class="form-horizontal one-submit" id="user_password" method="post" accept-charset="utf-8">
                                        <div style="display:none;"><input type="hidden" name="_method" value="PUT"><input type="hidden" name="data[_Token][key]" value="cd80871ece53627ca9c99b51ec93b0e093dcec20" id="Token1822278766"></div>
                                        <input type="hidden" name="data[User][chk]" class="form-control" value="aXdZ+yhkHbhaw/TM78LykBJhCmvoLiRAaH9spf9eIps=" id="chk6">
                                        <div class="form-group">
                                            <label for="UserPasswordOld" class="col-sm-3 control-label control-label">Старый пароль</label>
                                            <div class="col-sm-9"><input name="data[User][password_old]" class="form-control" required="required" autocomplete="off" type="password" id="UserPasswordOld"></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="UserPasswordUpdate" class="col-sm-3 control-label control-label">Пароль</label>
                                            <div class="col-sm-9"><input name="data[User][password_update]" class="form-control" required="required" autocomplete="off" type="password" id="UserPasswordUpdate"></div>
                                        </div>
                                        <div class="form-group required">
                                            <label for="UserPasswordConfirmUpdate" class="col-sm-3 control-label control-label">Повторить</label>
                                            <div class="col-sm-9"><input name="data[User][password_confirm_update]" class="form-control" required="required" autocomplete="off" type="password" id="UserPasswordConfirmUpdate"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-3">
                                                <button class="btn btn-primary" type="submit">Сохранить</button> 
                                            </div>
                                        </div>
                                        <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="2d91a9344babeba87095870c8eb14bda363a498e%3AUser.chk" id="TokenFields329193712"><input type="hidden" name="data[_Token][unlocked]" value="" id="TokenUnlocked1147908986"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6"></div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="footer">
            <div class="row">
                <div class="col-md-5">
                    Авторские права © HashCoins OÜ, Все права защищены. 2014-2018 
                </div>
                <div class="col-md-4">
                    Время на сервере: 13.10.2018 21:08:35 UTC 00:00 
                </div>
                <div class="col-md-3" style="text-align:right">
                    Последнее обновление: 07.09.2018 
                </div>
            </div>
        </div>
    </div>
	
<?php get_footer('spanel'); ?>


