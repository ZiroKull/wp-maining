<?php
/**
 * Template Name: Panel Verification
 */
 
get_header('panel'); 
?>
        
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize btn btn-primary " href="#">
                    <i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/logout" class="no-mobile hidden-xs">
                        <i class="fa fa-sign-out"></i> Выйти </a>
                    </li>
                </ul>
            </nav>
        </div>
        
        <div class="wrapper wrapper-content">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">×</span><span class="sr-only">Закрыть</span>
                </button>
                <strong>Успех!</strong> Профиль успешно сохранен
            </div>
            <div class="row animated fadeInRight">
                <div class="col-xs-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Форма верификации</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div id="verificationBlock">
                                    <div class="verificationBlock-wrapper">
                                        <form action="/panel/verification/upload" class="form-horizontal one-submit" id="verificationForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                            <div style="display:none;"><input type="hidden" name="_method" value="POST"><input type="hidden" name="data[_Token][key]" value="3bcb1931e068d39353535ea154259cd350fb3237" id="Token227295906"></div>
                                            <div class="form-group">
                                                <label>Чтобы завершить процесс верификации, пожалуйста, подтвердите свою личность</label>
                                                <p>
                                                    Успешно пройденная верификация личности необходима для полноценного доступа ко всем услугам сервиса HashFlare, включая повышение лимита на вывод средств.<br>
                                                    Определенные юридические обязательства, в частности обеспечение соответствия стандартам KYC (Know Your Customer), требуют от нас идентифицкации наших пользователей до предоставления им доступа к использованию всего возможного спектра услуг. 
                                                </p>
                                                <a href="https://hashflare.zendesk.com/hc/ru/articles/360000111900" target="_blank">Как мне подтвердить свою личность?</a>
                                            </div>
                                            <div class="form-group">
                                                <label for="doc-type" class="control-label">Тип документа:</label>
                                                <select name="data[doc_type]" class="form-control" required="required" id="doc-type">
                                                    <option value="passport">Паспорт</option>
                                                    <option value="id_card">Ид. карта</option>
                                                    <option value="driving_licence">Водительское удостоверение</option>
                                                </select>
                                            </div>
                                            <div class="form-group"><label for="doc-number" class="control-label">Идентификационный номер документа:</label><input name="data[doc_number]" class="form-control" required="required" placeholder="Введите идентификационный номер документа" id="doc-number" maxlength="200" type="text"></div>
                                            <div class="form-group">
                                                <div class="alert alert-info alert-dismissible" role="alert">
                                                    <strong>Документ:</strong>
                                                    <ul>
                                                        <li>Прикрепленное изображение должно быть в формате <strong>JPEG</strong>.</li>
                                                        <li>Убедитесь, что выбранный вами документ действителен.</li>
                                                        <li>Убедитесь, что ваше фото или отсканированное изображение ясное, и все данные на нем читаемы.</li>
                                                        <li>Пожалуйста, не открывайте изображение с помощью графических редакторов, потому что это может быть расценено как мошеннический акт.</li>
                                                        <li>Предоставленные изображения не должны быть изменены, обрезаны или отредактированы каким-либо другим образом.</li>
                                                    </ul>
                                                </div>
                                                <div class="alert alert-info alert-dismissible" role="alert">
                                                    <strong>Селфи с документом:</strong>
                                                    <ul>
                                                        <li>Прикрепленное изображение должно быть в формате <strong>JPEG</strong>.</li>
                                                        <li>Ваш взгляд должен быть направлен прямо в камеру.</li>
                                                        <li>Пожалуйста, убедитесь, что каждая деталь документа хорошо видна.</li>
                                                        <li>Пальцы не должны закрывать какую-либо важную информацию на документе.</li>
                                                        <li>Не надевайте головные уборы, очки и не закрывайте лицо.</li>
                                                    </ul>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="box">
                                                            <div class="js--image-preview add-photo document-block" id="passport_first"></div>
                                                            <div class="upload-options">
                                                                <small class="add-photo">добавить фотографию</small>
                                                                <label>
                                                                <span>Обложка паспорта</span>
                                                                <input type="file" name="data[first_photo]" class="image-upload" required="required" maxlength="200" accept=".jpg" id="first_photo"> </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="box">
                                                            <div class="js--image-preview add-photo document-block" id="passport_second"></div>
                                                            <div class="upload-options">
                                                                <small class="add-photo">добавить фотографию</small>
                                                                <label>
                                                                <span>Разворот паспорта</span>
                                                                <input type="file" name="data[second_photo]" class="image-upload" required="required" maxlength="200" accept=".jpg" id="second_photo"> </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="box">
                                                            <div class="js--image-preview add-photo" id="selfie"></div>
                                                            <div class="upload-options">
                                                                <small class="add-photo">добавить фотографию</small>
                                                                <label>
                                                                <span>Селфи с документом</span>
                                                                <input type="file" name="data[third_photo]" class="image-upload" required="required" maxlength="200" accept=".jpg" id="third_photo"> </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary pull-right" id="send-app">Отправить</button> 
                                            </div>
                                            <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="a9f1f1c46db26826303dcab215d14529b25fcd2b%3A" id="TokenFields463622274"><input type="hidden" name="data[_Token][unlocked]" value="" id="TokenUnlocked1192122537"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="footer">
            <div class="row">
                <div class="col-md-5">
                    Авторские права © HashCoins OÜ, Все права защищены. 2014-2018 
                </div>
                <div class="col-md-4">
                    Время на сервере: 13.10.2018 21:08:35 UTC 00:00 
                </div>
                <div class="col-md-3" style="text-align:right">
                    Последнее обновление: 07.09.2018 
                </div>
            </div>
        </div>
    </div>
	
<?php get_footer('vpanel'); ?>


