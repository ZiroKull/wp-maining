<?php
/**
 * Template Name: Panel Referrals Tools
 */
 
get_header('panel'); 
?>
        
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize btn btn-primary " href="#">
                    <i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/logout" class="no-mobile hidden-xs">
                        <i class="fa fa-sign-out"></i> Выйти </a>
                    </li>
                </ul>
            </nav>
        </div>
        
        <div class="wrapper wrapper-content">
            <div class="row animated fadeInRight">
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>
                                Обзор <small id="flot-data-info"></small>
                            </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content" style="height: 250px;">
                            <small>&nbsp;</small>
                            <p>Нет Информация</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>
                                Всего покупок / регистраций 
                            </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content" style="height: 230px;">
                            <h1 class="no-margins">
                                <strong>0 / 0</strong>
                            </h1>
                            <small>&nbsp;<br>&nbsp;</small>
                            <h2 class="no-margins">
                                Конверсия: 0 %
                                <small class="text-muted">0 / 0</small>
                            </h2>
                            <small>&nbsp;<br>&nbsp;</small>
                            <h2 class="no-margins">
                                Суммарная прибыль: 0 BTC
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Коды</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div style="border-bottom:none !important;margin-bottom:5px;" class="ibox">
                                <a class="btn btn-success btn-sm collapse-link">Добавить новый код</a>
                                <div style="background-color:#f4f6fa;display:none" class="ibox-content">
                                    <h2 class="inline m-n" style="min-width:200px;width:33%;vertical-align:middle;color:#5a93c4">Добавить новый код</h2>
                                    <form action="/panel/add-code" class="form-inline inline one-submit" style="width:65%" id="referralsNewForm" method="post" accept-charset="utf-8">
                                        <div style="display:none;"><input type="hidden" name="_method" value="POST"><input type="hidden" name="data[_Token][key]" value="97773404b47b739b323aea1189b275340ddfa4c5" id="Token365710858"></div>
                                        <div class=" form-group m-r-sm"><input name="data[name]" class="form-control m-r-sm" placeholder="Имя" type="text" id="name"></div>
                                        <div class="form-group">
                                            <div class="input-group"><span class="input-group-addon">7EEC0695 -</span><input name="data[code]" class="form-control m-r-sm" placeholder="Код" maxlength="10" minlength="3" data-toggle="tooltip" title="Valid characters: A-Z a-z 0-9 `-`" type="text" id="code"></div>
                                        </div>
                                        <div class="form-group m-n pull-right">
                                            <button class="btn btn-success m-n btn-w-l" type="submit">Сохранить</button> 
                                        </div>
                                        <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="e7878e0e2f04bce28afae00062b97e1ca78012d8%3A" id="TokenFields574809668"><input type="hidden" name="data[_Token][unlocked]" value="" id="TokenUnlocked466709942"></div>
                                    </form>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Имя</th>
                                            <th>Код</th>
                                            <th>Посещения</th>
                                            <th>Регистрации</th>
                                            <th>Покупки</th>
                                            <th>Конверсия</th>
                                            <th>Заработок</th>
                                            <th>Создано</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                Referral code 
                                                <div style="bottom: 0px;">
                                                    <a href="/panel/referrals/7EEC0695" class="btn btn-info btn-xs pull-right" style="margin-left:5px;"><i class="fa fa-search"></i></a> <a href="#" class="btn btn-info btn-xs pull-right link-trigger" data-target="#code2707612" data-prefix="r" style="margin-left:5px;"><i class="fa fa-eye"></i></a> 
                                                </div>
                                            </td>
                                            <td>
                                                <span id="code2707612">7EEC0695</span>
                                            </td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0%</td>
                                            <td>0 BTC</td>
                                            <td>2018-07-26 14:29:39</td>
                                            <td>
                                                <a class="btn btn-sm disabled">Выключить</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Имя</th>
                                            <th>Код</th>
                                            <th>Посещения</th>
                                            <th>Регистрации</th>
                                            <th>Покупки</th>
                                            <th>Конверсия</th>
                                            <th>Заработок</th>
                                            <th>Создано</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-info" role="alert">
                        <strong>Внимание!</strong>
                        Selected campaign 'Referral code' (7EEC0695). 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Покупки</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Имя</th>
                                            <th>Код</th>
                                            <th>Покупка</th>
                                            <th>Заработок</th>
                                            <th>Источник</th>
                                            <th>Время</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Имя</th>
                                            <th>Код</th>
                                            <th>Покупка</th>
                                            <th>Заработок</th>
                                            <th>Источник</th>
                                            <th>Время</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="footer">
            <div class="row">
                <div class="col-md-5">
                    Авторские права © HashCoins OÜ, Все права защищены. 2014-2018 
                </div>
                <div class="col-md-4">
                    Время на сервере: 13.10.2018 21:08:35 UTC 00:00 
                </div>
                <div class="col-md-3" style="text-align:right">
                    Последнее обновление: 07.09.2018 
                </div>
            </div>
        </div>
    </div>
	
<?php get_footer('rpanel'); ?>


