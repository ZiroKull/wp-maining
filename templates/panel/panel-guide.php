<?php
/**
 * Template Name: Panel Guide
 */
 
get_header('epanel'); 
?>
        
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize btn btn-primary " href="#">
                    <i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/logout" class="no-mobile hidden-xs">
                        <i class="fa fa-sign-out"></i> Выйти </a>
                    </li>
                </ul>
            </nav>
        </div>
        
        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <div class="content-body article-body">
                                <p><span class="wysiwyg-color-black">Чтобы начать добывать криптовалюту с помощью сервиса облачного майнинга <strong>HashFlare</strong>, необходимо завести личный кабинет, зарегистрировавшись на сайте. Регистрация быстрая и простая: введите адрес электронной почты, на который вам будет выслано подтверждение, страну, пароль и дату рождения. После заполнения всех необходимых полей и нажатия кнопки «<strong><a href="https://hashflare.io/register/?lang=rus">Регистрация</a></strong>» вы будете перенаправлены на главную страницу вашего личного кабинета, где можно найти последние новости и подсказки о работе сервиса, а также посмотреть статистику майнинга.</span></p>
                                <p><span class="wysiwyg-color-black">На главной странице личного кабинета происходит вся основная работа. На панели слева находятся дополнительные вкладки, о которых мы расскажем подробнее ниже.</span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-font-size-large wysiwyg-color-black"><strong>Вкладка «Купить хэшрейт»</strong></span></p>
                                <p><span class="wysiwyg-color-black">Это быстрая ссылка, позволяющая моментально перейти к покупке <strong>хэшрейта</strong>, необходимого для майнинга желаемой криптовалюты. HashFlare предлагает следующие виды майнинг-контрактов: <strong>SHA-256</strong>, <strong>Scrypt</strong>, <strong>ETHASH</strong>, <strong>Equihash</strong>, с помощью которых осуществляется майнинг <strong>Bitcoin</strong>, <strong>Litecoin</strong>, <strong>Ethereum</strong> и <strong>Zcash</strong>.</span></p>
                                <p><span class="wysiwyg-color-black">Для покупки хэшрейта необходимо с помощью бегунка на шкале выбрать желаемое количество мощности (1). При перемещении бегунка в специальном окне под шкалой (2) вы увидите стоимость вашей покупки, рассчитанную в <strong>USD</strong> и <strong>BTC</strong>.</span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-color-black"><img src="http://telekotp.beget.tech/wp-content/themes/maining/img/RIS1.jpg" alt="RIS1.jpg" width="635" height=""></span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-color-black">После выбора желаемого количества мощности и нажатия кнопки «<strong>Дальше</strong>» (3) вы будете перемещены на страницу покупки, где можно еще раз изменить количество покупаемых единиц мощности, активировать код со скидкой, посмотреть конечную стоимость покупки, отменить или осуществить платеж.</span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-color-black"><img src="http://telekotp.beget.tech/wp-content/themes/maining/img/RIS2.jpg" alt="RIS2.jpg" width="634" height=""></span></p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-color-black">Для того чтобы совершить покупку, нажмите кнопку «<strong>Выберите способ оплаты</strong>». Сервис HashFlare позволяет осуществлять покупку несколькими способами и в разной валюте: переводом <strong>BTC</strong>, или оплатив счет в USD с помощью банковского перевода, кредитной карты или сервиса <strong>Payeer</strong>.</span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-font-size-large wysiwyg-color-black"><strong>Пулы и Доходность</strong></span></p>
                                <p><span class="wysiwyg-color-black">Купленную мощность вы можете разделить между доступными пулами, выбрав пропорции по своему усмотрению. Пул представляет собой сервер, занимающийся распределением работы по нахождению блока между всеми участниками. Это своего рода совместный майнинг, подход, при котором все участники пула вносят свой вклад в генерацию блока, после чего происходит разделение дохода с полученного блока в соответствии со вложенным количеством мощности. Различные комбинации мощности, распределяемой по пулам, дают различные значения прибыльности. Вы можете выбрать до трех пулов за раз, однако изменить распределение мощности можно не чаще одного раза в день.</span></p>
                                <p><span class="wysiwyg-color-black">Чтобы выбрать желаемые пулы, нажмите на изображение шестеренки (1). В открывшемся меню вы можете распределить имеющуюся у вас мощность по доступным пулам с помощью бегунков на шкале (2). Процентные показатели распределяемой мощности вы можете увидеть справа. Здесь же вы можете удалить пулы (3) или, наоборот, добавить новые из доступных (4). Чтобы выбранные вами настройки вступили в силу, нажмите «<strong>Сохранить</strong>». Нажмите <strong>«Отменить»</strong> (5) для сброса всех изменений.</span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-color-black"><img src="http://telekotp.beget.tech/wp-content/themes/maining/img/RIS3.jpg" alt="RIS3.jpg" width="636" height=""></span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-color-black">Рядом с пулами вы можете увидеть приблизительные показатели доходности. Система записывает расположение пулов, которое учитывается при выплатах, поэтому обновление статистики происходит раз в сутки:</span></p>
                                <p><span class="wysiwyg-color-black"><strong>Доходность за единицу мощности</strong> — это примерный размер вашей ежедневной выплаты, который рассчитывается в зависимости от выбранного вами распределения хэшрейта по пулам. Значение обновляется ежедневно.</span></p>
                                <p><span class="wysiwyg-color-black"><strong>Прогноз доходности</strong> — это размеры вашего дохода в BTC, ETH, ZEC (в зависимости от того, какую валюту вы решили добывать) и USD за день, неделю, месяц, полгода и год. Показатели приблизительны, поскольку рассчитываются с условием, что <a href="https://cryptor.net/finansy/chto-vliyaet-na-dohodnost-vashego-mayninga">курс криптовалют и сложность майнинга останутся неизменны</a>. Обратите внимание, что данные указываются без учета платы за обслуживание.</span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-font-size-large wysiwyg-color-black"><strong>Вывод средств и Кошельки</strong></span></p>
                                <p><span class="wysiwyg-color-black">Чтобы вывести полученные средства, нажмите кнопку «Вывод» в меню баланса. Для вывода необходимо иметь на балансе минимальное доступное количество криптовалюты, а также соответствующие кошельки, адреса которых надо добавить в своем профиле. См. подробнее о том, как:</span></p>
                                <ul>
                                    <li><span class="wysiwyg-color-black"><a href="/hc/ru/articles/206691629">создать Bitcoin кошелек</a></span></li>
                                    <li><span class="wysiwyg-color-black"><a href="/hc/ru/articles/207923705">создать Ethereum кошелек</a></span></li>
                                    <li><span class="wysiwyg-color-black"><a href="/hc/ru/articles/213534269-Zcash-%D0%B0%D0%B4%D1%80%D0%B5%D1%81-%D0%B2%D1%8B%D0%B2%D0%BE%D0%B4%D1%8B-%D0%B8-%D0%BA%D0%BE%D1%88%D0%B5%D0%BB%D1%91%D0%BA">создать кошелек Zcash</a></span></li>
                                </ul>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-color-black"><img src="http://telekotp.beget.tech/wp-content/themes/maining/img/RIS4.jpg" alt="RIS4.jpg" width="637" height=""></span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-font-size-large wysiwyg-color-black"><strong>Реинвест</strong></span></p>
                                <p><span class="wysiwyg-color-black">Для BTC помимо вывода доступна функция «<strong>Реинвестировать</strong>», которая позволяет автоматически реинвестировать BTC в мощность при условии достаточного количества средств на вашем балансе. При включении функции для покупки мощности будут использованы все BTC, имеющиеся на балансе. После каждой выплаты система проверит, достаточно ли средств на балансе для покупки минимального количества мощности (10 GH/s для SHA-256 или 1 MH/s для Scrypt) и, если средств хватает, покупка будет создана и подтверждена автоматически.</span></p>
                                <p><span class="wysiwyg-color-black">Чтобы включить функцию, выберите тип мощности, в которую вы желаете реинвестировать, и нажмите кнопку «<strong>Сохранить</strong>». Чтобы отключить автоматическое реинвестирование, выберите в выпадающем меню опцию «<strong>Do not reinvest</strong>» и вновь нажмите кнопку «<strong>Сохранить</strong>». </span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-color-black"><img src="http://telekotp.beget.tech/wp-content/themes/maining/img/RIS5.jpg" alt="RIS5.jpg" width="635" height=""></span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-font-size-large wysiwyg-color-black"><strong>Вкладка «История»</strong></span></p>
                                <p><span class="wysiwyg-color-black">Здесь сохраняется вся история заключенных вами контрактов, покупки и выводов криптовалюты, а также логи, в которых отражаются все действия с балансом (поступления намайненных монет, удержание платы за обслуживание и проч.), а также, например, история изменений в распределении мощности.</span></p>
                                <p><span class="wysiwyg-color-black">Вы можете самостоятельно изменить количество записей, которое будет показываться на одной странице, сортировать их, а также осуществлять поиск. </span></p>
                                <p>&nbsp;</p>
                                <p><span class="wysiwyg-font-size-large wysiwyg-color-black"><strong>Вкладка «Настройки»</strong></span></p>
                                <p><span class="wysiwyg-color-black">В этой вкладке вы можете добавить адреса BTC, ETH и ZEC кошельков, на которые будет осуществляться вывод средств. Здесь же можно установить аватар, изменить свои личные и контактные данные, адрес электронной почты или пароль, а также включить функцию двухфакторной авторизации. Более полная личная и контактная информация, как и включенная двухфакторная авторизация, повышают безопасность вашей учетной записи.</span></p>
                                <p><span class="wysiwyg-color-black"><strong>Обратите внимание</strong>: в случае изменения таких данных, как пароль и адрес кошелька (любого), вы автоматически получите двухнедельный статус <strong>Hold</strong>. В этот период вы не сможете выводить средства с баланса, однако майнинг будет продолжать работать в обычном режиме. Это необходимая мера предосторожности, направленная против возможной кражи аккаунта.</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        <div class="footer">
            <div class="row">
                <div class="col-md-5">
                    Авторские права © HashCoins OÜ, Все права защищены. 2014-2018 
                </div>
                <div class="col-md-4">
                    Время на сервере: 13.10.2018 21:08:35 UTC 00:00 
                </div>
                <div class="col-md-3" style="text-align:right">
                    Последнее обновление: 07.09.2018 
                </div>
            </div>
        </div>
    </div>
	
<?php get_footer('epanel'); ?>


