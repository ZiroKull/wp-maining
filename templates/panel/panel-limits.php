<?php
/**
 * Template Name: Panel Limits
 */
 
get_header('panel'); 
?>
        
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize btn btn-primary " href="#">
                    <i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/logout" class="no-mobile hidden-xs">
                        <i class="fa fa-sign-out"></i> Выйти </a>
                    </li>
                </ul>
            </nav>
        </div>
        
        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Не верифицирован</h5>
                                </div>
                                <div class="ibox-content">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th>Валюта</th>
                                                <th>Дневной лимит</th>
                                                <th>30-дневный лимит</th>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td>BTC</td>
                                                <td>0</td>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <td>DASH</td>
                                                <td>0</td>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <td>ETH</td>
                                                <td>0</td>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <td>ZEC</td>
                                                <td>0</td>
                                                <td>0</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Уровень верификации: 1</h5>
                                </div>
                                <div class="ibox-content">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th>Валюта</th>
                                                <th>Дневной лимит</th>
                                                <th>30-дневный лимит</th>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td>BTC</td>
                                                <td>0.01</td>
                                                <td>0.05</td>
                                            </tr>
                                            <tr>
                                                <td>DASH</td>
                                                <td>0.2</td>
                                                <td>1</td>
                                            </tr>
                                            <tr>
                                                <td>ETH</td>
                                                <td>0.1</td>
                                                <td>0.5</td>
                                            </tr>
                                            <tr>
                                                <td>ZEC</td>
                                                <td>0.2</td>
                                                <td>1</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Уровень верификации: 2</h5>
                                </div>
                                <div class="ibox-content">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th>Валюта</th>
                                                <th>Дневной лимит</th>
                                                <th>30-дневный лимит</th>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td>BTC</td>
                                                <td>0.15</td>
                                                <td>0.75</td>
                                            </tr>
                                            <tr>
                                                <td>DASH</td>
                                                <td>4</td>
                                                <td>20</td>
                                            </tr>
                                            <tr>
                                                <td>ETH</td>
                                                <td>2</td>
                                                <td>10</td>
                                            </tr>
                                            <tr>
                                                <td>ZEC</td>
                                                <td>4</td>
                                                <td>20</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="footer">
            <div class="row">
                <div class="col-md-5">
                    Авторские права © HashCoins OÜ, Все права защищены. 2014-2018 
                </div>
                <div class="col-md-4">
                    Время на сервере: 13.10.2018 21:08:35 UTC 00:00 
                </div>
                <div class="col-md-3" style="text-align:right">
                    Последнее обновление: 07.09.2018 
                </div>
            </div>
        </div>
    </div>
	
<?php get_footer('lpanel'); ?>


