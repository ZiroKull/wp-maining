<?php
/**
 * Template Name: Panel Main
 */
 
get_header('panel'); 
?>
        
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize btn btn-primary " href="#">
                    <i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/logout" class="no-mobile hidden-xs">
                        <i class="fa fa-sign-out"></i> Выйти </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="wrapper wrapper-content">
            <div id="panelindex">
                <input type="hidden" id="usdprice" value="0.0001596">
                <input type="hidden" id="btcprice" value="6264.9850287">
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">×</span><span class="sr-only">Закрыть</span>
                    </button>
                    <strong>Предупреждение!</strong>
                    Ваша личность не верифицирована. Пожалуйста, заполните профиль и верифицируйте свою личность. <br>
                    Подробнее о том, как верифицировать аккаунт, читайте в <a href="https://hashflare.zendesk.com/hc/ru/articles/360000111900" rel="nofollow" target="_blank">Частые вопросы</a>. Заполнить свою учетную запись вы можете в меню “Профиль” в панели пользователя, подтвердить свою учетную запись вы можете в меню “Верификация” в панели пользователя. 
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="well">
                            <h1>Добро пожаловать на HashFlare.io!</h1>
                            <p>Чтобы начать майнинг, совершите по крайней мере одну покупку хэшрейта.</p>
                            <p>После того как вы закончили покупку вы можете распределить мощность по пулам, нажав на шестеренку в панели Пулы.</p>
                            <p>Чтобы успешно вывести средства, пожалуйста, добавьте адрес кошелька BTC в своем профиле.</p>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">×</span><span class="sr-only">Закрыть</span>
                    </button>
                    <strong> </strong>
                    Чтобы успешно вывести средства, пожалуйста, добавьте адрес кошелька BTC <a href="/panel/settings">в своем профиле</a>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="widget no-padding">
                            <div class="p-sm collapse  redeem" style="border: 1px solid #dee1e2;">
                                <form action="/panel/code" class="one-submit" id="CampaignIndexForm" method="post" accept-charset="utf-8">
                                    <div style="display:none;"><input type="hidden" name="_method" value="POST"><input type="hidden" name="data[_Token][key]" value="cdf8da62297914199ce25a14b8096fb4f25c63d1" id="Token31154600"></div>
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-4">
                                            <h2 style="color:#5a93c4;" class="m-t-none m-b-xs">
                                                Активировать код 
                                            </h2>
                                        </div>
                                        <div class="col-sm-6 col-lg-4">
                                            <div class="form-group required m-b-xs required"><input name="data[Campaign][code]" class="form-control m-n" placeholder="Код" maxlength="25" type="text" id="CampaignCode" required="required"></div>
                                        </div>
                                        <div class="col-sm-12 col-lg-4">
                                            <div class="form-group m-b-xs">
                                                <div class="submit"><input class="btn btn-success btn-block" type="submit" value="Активировать"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="f1d607b7df922450c1bce2040a206accac44a2a6%3A" id="TokenFields862530270"><input type="hidden" name="data[_Token][unlocked]" value="" id="TokenUnlocked1104818421"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>BTC Баланс </h5>
                                <a href="#" class="btn btn-primary btn-xs m-l-sm pull-right" data-toggle="modal" data-target="#reinvestModal">Реинвестировать</a> <span class="btn btn-white btn-xs m-l-sm pull-right" data-toggle="popover" data-placement="left" data-container="body" tabindex="0" data-trigger="focus" data-html="true" data-content="Чтобы успешно вывести средства, необходимо добавить адрес BTC кошелька в профиле">
                                Вывод </span>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <ul class="stat-list">
                                            <li>
                                                <div class="m-t-n-sm" style="position:absolute;top:50%;">
                                                    <span class="btn btn-xs p-xxs" style="background-color:#5A93c4;cursor:default;"></span>
                                                </div>
                                                <h2 class="m-l-md m-b-none" style="font-weight:400;">0 BTC</h2>
                                                <small class="m-l-md">Баланс</small>
                                            </li>
                                            <li>
                                                <hr>
                                            </li>
                                            <li>
                                                <div class="m-t-n-sm" style="position:absolute;top:50%;">
                                                    <span class="btn btn-xs p-xxs" style="background-color:#c2dfe9;cursor:default;"></span>
                                                </div>
                                                <h3 class="m-l-md m-b-none" style="font-weight:400;">0 BTC</h3>
                                                <small class="m-l-md">Последняя SHA-256 Выплата</small>
                                            </li>
                                            <li>
                                                <div class="m-t-n-sm" style="position:absolute;top:50%;">
                                                    <span class="btn btn-xs p-xxs" style="background-color:#c2dfe9;cursor:default;"></span>
                                                </div>
                                                <h3 class="m-l-md m-b-none" style="font-weight:400;">0 BTC</h3>
                                                <small class="m-l-md">Последняя Scrypt Выплата</small>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-9 hidden-xxs">
                                        <div class="flot-chart">
                                            <div class="flot-chart-content" id="flot-balance"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="0.0600" id="shaprice">
                <input type="hidden" value="" id="sha3yprice">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="widget style1">
                            <div class="row">
                                <h2 id="sha">
                                    <a class="header-toggle" data-target="#sha-row" data-toggle="collapse" aria-expanded="false" aria-controls="sha-row">
                                    <span class="fa fa-angle-down"></span>
                                    <span class="fa fa-angle-right"></span>
                                    SHA-256 </a>
                                    <a class="purchase-toggle" data-target=".purchase-sha" data-toggle="collapse" aria-expanded="false" aria-controls="purchase-sha">
                                    <i class="fa fa-shopping-cart text-success"></i>
                                    </a>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="collapse in" id="sha-row">
                    <div class="row collapse " id="collapse-sha-pool">
                        <div class="col-xs-12">
                            <div class="widget slider-widget" style="border: 1px solid #dee1e2;">
                                <div class="row m-l-xs m-r-xs m-t-n-md m-b-sm">
                                    <div class="col-xs-12">
                                        <h3 class="text-center p-sm" style="color:#5a93c4;">
                                            Пулы 
                                        </h3>
                                        <form action="/panel" class="form-horizontal form-pools sha" method="post" accept-charset="utf-8">
                                            <div style="display:none;"><input type="hidden" name="_method" value="POST"><input type="hidden" name="data[_Token][key]" value="cdf8da62297914199ce25a14b8096fb4f25c63d1" id="Token1813619443"></div>
                                            <div class="pools"></div>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <button type="button" class="btn btn-primary add m-b-sm" style="display:none;"><span class="glyphicon glyphicon-plus-sign"></span> Добавить пул</button><button type="submit" class="btn btn-success save m-b-sm" data-loading-text="Saving..." style="display:none;"><span class="glyphicon glyphicon-ok"></span> Сохранить</button><button type="button" class="btn btn-link m-b-sm cancel" data-target="#collapse-sha-pool" data-toggle="collapse" aria-expanded="true" aria-controls="" collapse-sha"=""><span class="glyphicon glyphicon-remove"></span> Отменить</button> 
                                                </div>
                                            </div>
                                            <div class="template" style="display:none;">
                                                <div class="form-group pool">
                                                    <div class="col-xs-10">
                                                        <select class="form-control"></select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <button type="button" class="btn btn-danger remove">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                        </button>
                                                    </div>
                                                    <div class="col-xs-10">
                                                        <div class="slider"></div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <div class="slider-value"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="a216e1f52b9731e9efd39a2b1a1d3c8a6c9f8a4f%3A" id="TokenFields489110783"><input type="hidden" name="data[_Token][unlocked]" value="custom.type%7Cpools.0.id%7Cpools.0.percentage%7Cpools.1.id%7Cpools.1.percentage%7Cpools.2.id%7Cpools.2.percentage" id="TokenUnlocked1722630101"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="widget lazur-bg p-lg text-center m-n b-t b-b" style="background-color:#9ad1ed;margin-bottom:25px !important;">
                                <div class="m-b-xl m-t-md">
                                    <br><br>
                                    <i class="fa fa-flash fa-4x"></i>
                                    <h1 class="m-xs">0 TH/s</h1>
                                    <h3 class="font-bold no-margins">
                                        Хэшрейт<br><br>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Пулы</h5>
                                    <div class="ibox-tools">
                                        <a data-target="#collapse-sha-pool" data-toggle="collapse" aria-expanded="false" class="pull-right" aria-controls="collapse-sha-pool">
                                        <i class="fa fa-cog fa-lg"></i>
                                        </a>
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Хэшрейт" data-content="Выберите до 3-х пулов, по которым хотите распределить мощность. Различные комбинации обеспечивают различные значения прибыльности. Вы можете изменить распределение один раз в день.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="flot-chart small">
                                        <div class="flot-chart-content" id="flot-pools-sha"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>
                                        Доходность за 1 TH/s 
                                    </h5>
                                    <div class="ibox-tools">
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Прибыльность" data-content="Это число показывает, какую примерную выплату вы получаете в день с данным распределением хэшрейта. Значение обновляется ежедневно.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div style="height: 170px;">
                                        <p class="well m-xs">
                                            У Вас еще не было начислений, невозможно отобразить доходность.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Прогноз доходности</h5>
                                    <div class="ibox-tools">
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Прогноз доходности" data-content="If the BTC price and mining difficulty do not change, then this is how big would be your payout.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <p class="p-n m-t-n-xxs">
                                        <span class="pull-right badge badge-info" style="background-color: #9ad1ed" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 день">
                                        1д </span>
                                        0 BTC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-primary" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 неделя">
                                        1н </span>
                                        0 BTC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-success" style="background-color: #A0F3BB" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 месяц">
                                        1м </span>
                                        0 BTC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-danger" rel="tooltip" data-toggle="tooltip" data-placement="left" title="6 месяцев">
                                        6м </span>
                                        0 BTC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n m-b-n-xxs">
                                        <span class="pull-right badge badge-warning" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 год">
                                        1г </span>
                                        0 BTC = 0.00 USD
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="1.8000" id="scryptprice">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="widget style1">
                            <div class="row">
                                <h2 id="scrypt">
                                    <a class="header-toggle" data-target="#scrypt-row" data-toggle="collapse" aria-expanded="false" aria-controls="scrypt-row">
                                    <span class="fa fa-angle-down"></span>
                                    <span class="fa fa-angle-right"></span>
                                    Scrypt </a>
                                    <a class="purchase-toggle" data-target=".purchase-scrypt" data-toggle="collapse" aria-expanded="false" aria-controls="purchase-scrypt">
                                    <i class="fa fa-shopping-cart text-success"></i>
                                    </a>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="collapse in" id="scrypt-row">
                    <div class="row collapse " id="collapse-scrypt-pool">
                        <div class="col-xs-12">
                            <div class="widget slider-widget" style="border: 1px solid #dee1e2;">
                                <div class="row m-l-xs m-r-xs m-t-n-md m-b-sm">
                                    <div class="col-xs-12">
                                        <h3 class="text-center p-sm" style="color:#5a93c4;">
                                            Пулы 
                                        </h3>
                                        <form action="/panel" class="form-horizontal form-pools scrypt" method="post" accept-charset="utf-8">
                                            <div style="display:none;"><input type="hidden" name="_method" value="POST"><input type="hidden" name="data[_Token][key]" value="cdf8da62297914199ce25a14b8096fb4f25c63d1" id="Token1615785198"></div>
                                            <div class="pools"></div>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <button type="button" class="btn btn-primary add m-b-sm" style="display:none;"><span class="glyphicon glyphicon-plus-sign"></span> Добавить пул</button><button type="submit" class="btn btn-success save m-b-sm" data-loading-text="Saving..." style="display:none;"><span class="glyphicon glyphicon-ok"></span> Сохранить</button><button type="button" class="btn btn-link m-b-sm cancel" data-target="#collapse-scrypt-pool" data-toggle="collapse" aria-expanded="true" aria-controls="" collapse-scrypt"=""><span class="glyphicon glyphicon-remove"></span> Отменить</button> 
                                                </div>
                                            </div>
                                            <div class="template" style="display:none;">
                                                <div class="form-group pool">
                                                    <div class="col-xs-10">
                                                        <select class="form-control"></select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <button type="button" class="btn btn-danger remove">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                        </button>
                                                    </div>
                                                    <div class="col-xs-10">
                                                        <div class="slider"></div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <div class="slider-value"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="a216e1f52b9731e9efd39a2b1a1d3c8a6c9f8a4f%3A" id="TokenFields104851559"><input type="hidden" name="data[_Token][unlocked]" value="custom.type%7Cpools.0.id%7Cpools.0.percentage%7Cpools.1.id%7Cpools.1.percentage%7Cpools.2.id%7Cpools.2.percentage" id="TokenUnlocked647089369"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="widget lazur-bg p-lg text-center m-n b-t b-b" style="background-color:#9ad1ed;margin-bottom:25px !important;">
                                <div class="m-b-xl m-t-md">
                                    <br><br>
                                    <i class="fa fa-flash fa-4x"></i>
                                    <h1 class="m-xs">0 MH/s</h1>
                                    <h3 class="font-bold no-margins">
                                        Хэшрейт<br><br>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Пулы</h5>
                                    <div class="ibox-tools">
                                        <a data-target="#collapse-scrypt-pool" data-toggle="collapse" aria-expanded="false" class="pull-right" aria-controls="collapse-scrypt-pool">
                                        <i class="fa fa-cog fa-lg"></i>
                                        </a>
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Хэшрейт" data-content="Выберите до 3-х пулов, по которым хотите распределить мощность. Различные комбинации обеспечивают различные значения прибыльности. Вы можете изменить распределение один раз в день.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="flot-chart small">
                                        <div class="flot-chart-content" id="flot-pools-scrypt"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>
                                        Доходность за 1 MH/s 
                                    </h5>
                                    <div class="ibox-tools">
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Прибыльность" data-content="Это число показывает, какую примерную выплату вы получаете в день с данным распределением хэшрейта. Значение обновляется ежедневно.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div style="height: 170px;">
                                        <p class="well m-xs">
                                            У Вас еще не было начислений, невозможно отобразить доходность.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Прогноз доходности</h5>
                                    <div class="ibox-tools">
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Прогноз доходности" data-content="If the BTC price and mining difficulty do not change, then this is how big would be your payout.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <p class="p-n m-t-n-xxs">
                                        <span class="pull-right badge badge-info" style="background-color: #9ad1ed" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 день">
                                        1д </span>
                                        0 BTC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-primary" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 неделя">
                                        1н </span>
                                        0 BTC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-success" style="background-color: #A0F3BB" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 месяц">
                                        1м </span>
                                        0 BTC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-danger" rel="tooltip" data-toggle="tooltip" data-placement="left" title="6 месяцев">
                                        6м </span>
                                        0 BTC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n m-b-n-xxs">
                                        <span class="pull-right badge badge-warning" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 год">
                                        1г </span>
                                        0 BTC = 0.00 USD
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>ETH Баланс </h5>
                                <span class="btn btn-white btn-xs m-l-sm pull-right" data-toggle="popover" data-placement="left" data-container="body" tabindex="0" data-trigger="focus" data-html="true" data-content="Чтобы успешно вывести средства, необходимо добавить адрес ETH кошелька в профиле">
                                Вывод </span>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <ul class="stat-list">
                                            <li>
                                                <div class="m-t-n-sm" style="position:absolute;top:50%;">
                                                    <span class="btn btn-xs p-xxs" style="background-color:#5A93c4;cursor:default;"></span>
                                                </div>
                                                <h2 class="m-l-md m-b-none" style="font-weight:400;">0 ETH</h2>
                                                <small class="m-l-md">Баланс</small>
                                            </li>
                                            <li>
                                                <hr>
                                            </li>
                                            <li>
                                                <div class="m-t-n-sm" style="position:absolute;top:50%;">
                                                    <span class="btn btn-xs p-xxs" style="background-color:#c2dfe9;cursor:default;"></span>
                                                </div>
                                                <h3 class="m-l-md m-b-none" style="font-weight:400;">0 ETH</h3>
                                                <small class="m-l-md">Последняя ETHASH Выплата</small>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-9 hidden-xxs">
                                        <div class="flot-chart">
                                            <div class="flot-chart-content" id="flot-eth-balance"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="0.0140" id="etherprice">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="widget style1">
                            <div class="row">
                                <h2 id="ether">
                                    <a class="header-toggle" data-target="#ether-row" data-toggle="collapse" aria-expanded="false" aria-controls="ether-row">
                                    <span class="fa fa-angle-down"></span>
                                    <span class="fa fa-angle-right"></span>
                                    ETHASH (ETH) </a>
                                    <a class="purchase-toggle" data-target=".purchase-ether" data-toggle="collapse" aria-expanded="false" aria-controls="purchase-ether">
                                    <i class="fa fa-shopping-cart text-success"></i>
                                    </a>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="collapse in" id="ether-row">
                    <div class="row collapse  purchase-ether">
                        <div class="col-xs-12">
                            <div class="widget slider-widget" style="border: 1px solid #dee1e2;">
                                <div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h3 class="text-center p-xs" style="color:#5a93c4;">
                                                Выберите хэшрейт 
                                            </h3>
                                            <div class="col-xs-12">
                                                <div id="slider-ether" class="uislider m-t m-b" data-step="100" data-start="0" data-min="0" data-max="1000000" data-unit="kilo" data-type="ether"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row hidden-xs hidden-sm">&nbsp;</div>
                                    <div class="row hidden-xs hidden-sm">&nbsp;</div>
                                    <div class="row purchase-ether" id="collapse-ether">
                                        <div class="col-xs-12">
                                            <div class="widget style1">
                                                <form action="/panel/purchase" class="form-horizontal one-submit" id="PurchaseIndexForm" method="post" accept-charset="utf-8">
                                                    <div style="display:none;"><input type="hidden" name="_method" value="POST"><input type="hidden" name="data[_Token][key]" value="cdf8da62297914199ce25a14b8096fb4f25c63d1" id="Token3551194"></div>
                                                    <input type="hidden" name="data[Purchase][product]" value="5" id="PurchaseProduct"><input type="hidden" name="data[Purchase][amount]" class="slider-ether-total" value="0" id="PurchaseAmount"><input type="hidden" name="data[Purchase][years]" id="product_years" value="1"> 
                                                    <div class="hidden-md hidden-lg text-center well well-sm m-b-sm" style="padding: 7px;">
                                                        <span class="slider-ether-usd">0</span> USD
                                                        или <span class="slider-ether-btc">0</span> BTC
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-2 col-md-2 m-b-sm">
                                                            <div class="input-group">
                                                                <input name="data[Purchase][amount]" class="form-control slider-ether-total" value="0" id="ether_amount" type="text" required="required">
                                                                <span class="input-group-addon">
                                                                KH/s </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-offset-1 col-md-3 m-b-sm pull-right">
                                                            <button class="btn btn-success btn-block" type="submit">Дальше</button> 
                                                        </div>
                                                        <div class="hidden-xs hidden-sm col-md-offset-1 col-md-3 text-center well well-sm pull-right" style="padding: 7px;">
                                                            <span class="slider-ether-usd">0</span> USD
                                                            или <span class="slider-ether-btc">0</span> BTC
                                                        </div>
                                                    </div>
                                                    <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="dfaf77c8fc342ffbd025b0bf835e40196d1220f9%3APurchase.product" id="TokenFields1008988769"><input type="hidden" name="data[_Token][unlocked]" value="amount%7Ccustom.type%7Cpools.0.id%7Cpools.0.percentage%7Cpools.1.id%7Cpools.1.percentage%7Cpools.2.id%7Cpools.2.percentage%7Cyears" id="TokenUnlocked2006840755"></div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row collapse " id="collapse-ether-pool">
                        <div class="col-xs-12">
                            <div class="widget slider-widget" style="border: 1px solid #dee1e2;">
                                <div class="row m-l-xs m-r-xs m-t-n-md m-b-sm">
                                    <div class="col-xs-12">
                                        <h3 class="text-center p-sm" style="color:#5a93c4;">
                                            Пулы 
                                        </h3>
                                        <form action="/panel" class="form-horizontal form-pools ether" method="post" accept-charset="utf-8">
                                            <div style="display:none;"><input type="hidden" name="_method" value="POST"><input type="hidden" name="data[_Token][key]" value="cdf8da62297914199ce25a14b8096fb4f25c63d1" id="Token252510785"></div>
                                            <div class="pools"></div>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <button type="button" class="btn btn-primary add m-b-sm" style="display:none;"><span class="glyphicon glyphicon-plus-sign"></span> Добавить пул</button><button type="submit" class="btn btn-success save m-b-sm" data-loading-text="Saving..." style="display:none;"><span class="glyphicon glyphicon-ok"></span> Сохранить</button><button type="button" class="btn btn-link m-b-sm cancel" data-target="#collapse-ether-pool" data-toggle="collapse" aria-expanded="true" aria-controls="" collapse-ether"=""><span class="glyphicon glyphicon-remove"></span> Отменить</button> 
                                                </div>
                                            </div>
                                            <div class="template" style="display:none;">
                                                <div class="form-group pool">
                                                    <div class="col-xs-10">
                                                        <select class="form-control"></select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <button type="button" class="btn btn-danger remove">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                        </button>
                                                    </div>
                                                    <div class="col-xs-10">
                                                        <div class="slider"></div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <div class="slider-value"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="70ec839d3b037956cefdd1f74cfe4c0ad4e85561%3A" id="TokenFields944628717"><input type="hidden" name="data[_Token][unlocked]" value="amount%7Ccustom.type%7Cpools.0.id%7Cpools.0.percentage%7Cpools.1.id%7Cpools.1.percentage%7Cpools.2.id%7Cpools.2.percentage%7Cyears" id="TokenUnlocked1003116392"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="widget lazur-bg p-lg text-center m-n b-t b-b" style="background-color:#9ad1ed;margin-bottom:25px !important;">
                                <div class="m-b-xl m-t-md">
                                    <br><br>
                                    <i class="fa fa-flash fa-4x"></i>
                                    <h1 class="m-xs">0 MH/s</h1>
                                    <h3 class="font-bold no-margins">
                                        Хэшрейт<br><br>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Пулы</h5>
                                    <div class="ibox-tools">
                                        <a data-target="#collapse-ether-pool" data-toggle="collapse" aria-expanded="false" class="pull-right" aria-controls="collapse-ether-pool">
                                        <i class="fa fa-cog fa-lg"></i>
                                        </a>
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Хэшрейт" data-content="Выберите до 3-х пулов, по которым хотите распределить мощность. Различные комбинации обеспечивают различные значения прибыльности. Вы можете изменить распределение один раз в день.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="flot-chart small">
                                        <div class="flot-chart-content" id="flot-pools-ether"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>
                                        Доходность за 1 MH/s 
                                    </h5>
                                    <div class="ibox-tools">
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Прибыльность" data-content="Это число показывает, какую примерную выплату вы получаете в день с данным распределением хэшрейта. Значение обновляется ежедневно.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div style="height: 170px;">
                                        <p class="well m-xs">
                                            У Вас еще не было начислений, невозможно отобразить доходность.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Прогноз доходности</h5>
                                    <div class="ibox-tools">
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Прогноз доходности" data-content="If the ETH price and mining difficulty do not change, then this is how big would be your payout.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <p class="p-n m-t-n-xxs">
                                        <span class="pull-right badge badge-info" style="background-color: #9ad1ed" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 день">
                                        1д </span>
                                        0 ETH = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-primary" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 неделя">
                                        1н </span>
                                        0 ETH = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-success" style="background-color: #A0F3BB" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 месяц">
                                        1м </span>
                                        0 ETH = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-danger" rel="tooltip" data-toggle="tooltip" data-placement="left" title="6 месяцев">
                                        6м </span>
                                        0 ETH = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n m-b-n-xxs">
                                        <span class="pull-right badge badge-warning" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 год">
                                        1г </span>
                                        0 ETH = 0.00 USD
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>ZEC Баланс </h5>
                                <span class="btn btn-white btn-xs m-l-sm pull-right" data-toggle="popover" data-placement="left" data-container="body" tabindex="0" data-trigger="focus" data-html="true" data-content="Чтобы успешно вывести средства, необходимо добавить адрес ZEC кошелька в профиле">
                                Вывод </span>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <ul class="stat-list">
                                            <li>
                                                <div class="m-t-n-sm" style="position:absolute;top:50%;">
                                                    <span class="btn btn-xs p-xxs" style="background-color:#5A93c4;cursor:default;"></span>
                                                </div>
                                                <h2 class="m-l-md m-b-none" style="font-weight:400;">0 ZEC</h2>
                                                <small class="m-l-md">Баланс</small>
                                            </li>
                                            <li>
                                                <hr>
                                            </li>
                                            <li>
                                                <div class="m-t-n-sm" style="position:absolute;top:50%;">
                                                    <span class="btn btn-xs p-xxs" style="background-color:#c2dfe9;cursor:default;"></span>
                                                </div>
                                                <h3 class="m-l-md m-b-none" style="font-weight:400;">0 ZEC</h3>
                                                <small class="m-l-md">Последняя ZEC Выплата</small>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-9 hidden-xxs">
                                        <div class="flot-chart">
                                            <div class="flot-chart-content" id="flot-zec-balance"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="0.0014" id="zcashprice">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="widget style1">
                            <div class="row">
                                <h2 id="zcash">
                                    <a class="header-toggle" data-target="#zcash-row" data-toggle="collapse" aria-expanded="false" aria-controls="zcash-row">
                                    <span class="fa fa-angle-down"></span>
                                    <span class="fa fa-angle-right"></span>
                                    EQUIHASH (ZEC) </a>
                                    <a class="purchase-toggle" data-target=".purchase-zcash" data-toggle="collapse" aria-expanded="false" aria-controls="purchase-zcash">
                                    <i class="fa fa-shopping-cart text-success"></i>
                                    </a>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="collapse in" id="zcash-row">
                    <div class="row collapse " id="collapse-zcash-pool">
                        <div class="col-xs-12">
                            <div class="widget slider-widget" style="border: 1px solid #dee1e2;">
                                <div class="row m-l-xs m-r-xs m-t-n-md m-b-sm">
                                    <div class="col-xs-12">
                                        <h3 class="text-center p-sm" style="color:#5a93c4;">
                                            Пулы 
                                        </h3>
                                        <form action="/panel" class="form-horizontal form-pools zcash" method="post" accept-charset="utf-8">
                                            <div style="display:none;"><input type="hidden" name="_method" value="POST"><input type="hidden" name="data[_Token][key]" value="cdf8da62297914199ce25a14b8096fb4f25c63d1" id="Token1432095756"></div>
                                            <div class="pools"></div>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <button type="button" class="btn btn-primary add m-b-sm" style="display:none;"><span class="glyphicon glyphicon-plus-sign"></span> Добавить пул</button><button type="submit" class="btn btn-success save m-b-sm" data-loading-text="Saving..." style="display:none;"><span class="glyphicon glyphicon-ok"></span> Сохранить</button><button type="button" class="btn btn-link m-b-sm cancel" data-target="#collapse-zcash-pool" data-toggle="collapse" aria-expanded="true" aria-controls="" collapse-zcash"=""><span class="glyphicon glyphicon-remove"></span> Отменить</button> 
                                                </div>
                                            </div>
                                            <div class="template" style="display:none;">
                                                <div class="form-group pool">
                                                    <div class="col-xs-10">
                                                        <select class="form-control"></select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <button type="button" class="btn btn-danger remove">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                        </button>
                                                    </div>
                                                    <div class="col-xs-10">
                                                        <div class="slider"></div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <div class="slider-value"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="70ec839d3b037956cefdd1f74cfe4c0ad4e85561%3A" id="TokenFields1972060717"><input type="hidden" name="data[_Token][unlocked]" value="amount%7Ccustom.type%7Cpools.0.id%7Cpools.0.percentage%7Cpools.1.id%7Cpools.1.percentage%7Cpools.2.id%7Cpools.2.percentage%7Cyears" id="TokenUnlocked1064155639"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="widget lazur-bg p-lg text-center m-n b-t b-b" style="background-color:#9ad1ed;margin-bottom:25px !important;">
                                <div class="m-b-xl m-t-md">
                                    <br><br>
                                    <i class="fa fa-flash fa-4x"></i>
                                    <h1 class="m-xs">0 H/s </h1>
                                    <h3 class="font-bold no-margins">
                                        Хэшрейт<br><br>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Пулы</h5>
                                    <div class="ibox-tools">
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Хэшрейт" data-content="Выберите до 3-х пулов, по которым хотите распределить мощность. Различные комбинации обеспечивают различные значения прибыльности. Вы можете изменить распределение один раз в день.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="flot-chart small">
                                        <div class="flot-chart-content" id="flot-pools-zcash"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>
                                        Доходность за 1 H/s 
                                    </h5>
                                    <div class="ibox-tools">
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Прибыльность" data-content="Это число показывает, какую примерную выплату вы получаете в день с данным распределением хэшрейта. Значение обновляется ежедневно.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div style="height: 170px;">
                                        <p class="well m-xs">
                                            У Вас еще не было начислений, невозможно отобразить доходность.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Прогноз доходности</h5>
                                    <div class="ibox-tools">
                                        <a data-container="body" data-toggle="popover" data-placement="top" tabindex="0" data-trigger="focus" title="Прогноз доходности" data-content="If the ZEC price and mining difficulty do not change, then this is how big would be your payout.">
                                        <i class="fa fa-info fa-lg"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <p class="p-n m-t-n-xxs">
                                        <span class="pull-right badge badge-info" style="background-color: #9ad1ed" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 день">
                                        1д </span>
                                        0 ZEC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-primary" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 неделя">
                                        1н </span>
                                        0 ZEC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-success" style="background-color: #A0F3BB" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 месяц">
                                        1м </span>
                                        0 ZEC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n">
                                        <span class="pull-right badge badge-danger" rel="tooltip" data-toggle="tooltip" data-placement="left" title="6 месяцев">
                                        6м </span>
                                        0 ZEC = 0.00 USD
                                    </p>
                                    <hr class="m-t-xxs m-b-sm">
                                    <p class="p-n m-b-n-xxs">
                                        <span class="pull-right badge badge-warning" rel="tooltip" data-toggle="tooltip" data-placement="left" title="1 год">
                                        1г </span>
                                        0 ZEC = 0.00 USD
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="reinvestModal" tabindex="-1" role="dialog" aria-labelledby="reinvestModalLabel" aria-hidden="true" data-replace="true">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Реинвестировать</h4>
                            </div>
                            <div class="modal-body">
                                <p>Функция Реинвест позволяет Вам автоматически докупать мощность, если на Вашем балансе достаточно средств.</p>
                                <p>При включении этой функции, все средства на Вашем балансе будут использованы для покупки мощности.</p>
                                <p>После каждой выплаты, система проверит, достаточно ли средств на Балансе для покупки минимального количества мощности (10 GH/s для SHA-256 или 1 MH/s для Scrypt). Если средств достаточно, покупка будет создана и подтверждена автоматически.</p>
                                <form action="/panel/reinvest" id="reinvest-form" method="post" accept-charset="utf-8">
                                    <div style="display:none;"><input type="hidden" name="_method" value="POST"><input type="hidden" name="data[_Token][key]" value="cdf8da62297914199ce25a14b8096fb4f25c63d1" id="Token712849025"></div>
                                    <div class="form-group">
                                        <label class="sr-only" for="reinvest">Реинвестировать</label>
                                        <select name="data[reinvest]" id="reinvest" class="form-control">
                                            <option value="">Do not reinvest</option>
                                            <option value="sha">Reinvest in SHA-256</option>
                                            <option value="scrypt">Reinvest in Scrypt</option>
                                        </select>
                                    </div>
                                    <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="adb244fe48d60b927230abfc801e511cf4ca74f4%3A" id="TokenFields426028075"><input type="hidden" name="data[_Token][unlocked]" value="amount%7Ccustom.type%7Cpools.0.id%7Cpools.0.percentage%7Cpools.1.id%7Cpools.1.percentage%7Cpools.2.id%7Cpools.2.percentage%7Cyears" id="TokenUnlocked1633976760"></div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                <button type="button" class="btn btn-primary" id="reinvest-save">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="row">
                <div class="col-md-5">
                    Авторские права © HashCoins OÜ, Все права защищены. 2014-2018 
                </div>
                <div class="col-md-4">
                    Время на сервере: 13.10.2018 21:08:35 UTC 00:00 
                </div>
                <div class="col-md-3" style="text-align:right">
                    Последнее обновление: 07.09.2018 
                </div>
            </div>
        </div>
    </div>
	
<?php get_footer('panel'); ?>


