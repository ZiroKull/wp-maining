<?php
/**
 * Template Name: Panel History
 */
 
get_header('panel'); 
?>
        
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize btn btn-primary " href="#">
                    <i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/logout" class="no-mobile hidden-xs">
                        <i class="fa fa-sign-out"></i> Выйти </a>
                    </li>
                </ul>
            </nav>
        </div>
        
        <div class="wrapper wrapper-content">
            <div class="row animated fadeInRight">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Контракты</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Тип</th>
                                            <th>Сумма</th>
                                            <th>запуск/начало</th>
                                            <th>окончание</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Тип</th>
                                            <th>Сумма</th>
                                            <th>запуск/начало</th>
                                            <th>окончание</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row animated fadeInRight">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Покупки</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Товар</th>
                                            <th>Количество</th>
                                            <th>Итого</th>
                                            <th>Оплата</th>
                                            <th>Время</th>
                                            <th>Статус</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Товар</th>
                                            <th>Количество</th>
                                            <th>Итого</th>
                                            <th>Оплата</th>
                                            <th>Время</th>
                                            <th>Статус</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row animated fadeInRight">
                <div class="col-xs-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Выводы</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Время</th>
                                            <th>Сумма</th>
                                            <th style="width: 60px">Статус</th>
                                            <th>TxID</th>
                                            <th>TxID (alt)</th>
                                            <th style="width: 30px;"></th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Время</th>
                                            <th>Сумма</th>
                                            <th style="width: 60px">Статус</th>
                                            <th>TxID</th>
                                            <th>TxID (alt)</th>
                                            <th style="width: 30px;"></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row animated fadeInRight">
                <div class="col-xs-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Журнал</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTable dataTableLog">
                                    <thead>
                                        <tr>
                                            <th>Запись</th>
                                            <th>Время</th>
                                            <th>Сумма (BTC)</th>
                                            <th>Баланс (BTC)</th>
                                            <th>Сумма (ETH)</th>
                                            <th>Баланс (ETH)</th>
                                            <th>Сумма (DASH)</th>
                                            <th>Баланс (DASH)</th>
                                            <th>Сумма (ZEC)</th>
                                            <th>Баланс (ZEC)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Updated ETHASH pool allocation</td>
                                            <td data-order="1539593527">15.10.18 08:52</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>Изменение распределения мощности SHA-256</td>
                                            <td data-order="1539593509">15.10.18 08:51</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>Изменение распределения мощности SHA-256</td>
                                            <td data-order="1539465960">13.10.18 21:26</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>Updated ETHASH pool allocation</td>
                                            <td data-order="1539465910">13.10.18 21:25</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Запись</th>
                                            <th>Время</th>
                                            <th>Сумма (BTC)</th>
                                            <th>Баланс (BTC)</th>
                                            <th>Сумма (ETH)</th>
                                            <th>Баланс (ETH)</th>
                                            <th>Сумма (DASH)</th>
                                            <th>Баланс (DASH)</th>
                                            <th>Сумма (ZEC)</th>
                                            <th>Баланс (ZEC)</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="col-md-2 user_logs_history_link" style="float: right; text-align: right; display: none;">
                                    <a href="/panel/user_logs_history" target="_blank">Show more history</a> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="footer">
            <div class="row">
                <div class="col-md-5">
                    Авторские права © HashCoins OÜ, Все права защищены. 2014-2018 
                </div>
                <div class="col-md-4">
                    Время на сервере: 13.10.2018 21:08:35 UTC 00:00 
                </div>
                <div class="col-md-3" style="text-align:right">
                    Последнее обновление: 07.09.2018 
                </div>
            </div>
        </div>
    </div>
	
<?php get_footer('hpanel'); ?>


