<?php
/**
 * Template Name: How it work
 */
 
get_header(); 
?>
        
        <div style="min-height:100%;min-height:54vh;">
            <div id="home">
                <header class="intro">
                    <div class="headerbg rubg" style="background:#002626 url(http://telekotp.beget.tech/wp-content/themes/maining/img/bigbg.png) no-repeat center;background-size:cover;">
                        <div class="intro-body">
                            <div class="container">
                                <h1 class="fronthead">Облачный Майнинг</h1>
                            </div>
                        </div>
                    </div>
                    <div class="scroll-trigger"></div>
                </header>
            </div>
           <div id="static">
              <div class="scroll-trigger"></div>
              <div class="container">
                 <h1>Как это работает?</h1>
                 <div class="row how-it-works-items">
                    <div class="col-md-4 item">
                       <h4>Оборудование</h4>
                       <div class="image">
                          <img class="img-circle" src="http://telekotp.beget.tech/wp-content/themes/maining/img/infogr_devices.jpg" alt="Оборудование">
                       </div>
                       <div class="desc">
                          Майнер – это высокоэффективное оборудование специально созданное для получения криптовалют. В наших дата-центрах находятся сотни таких устройств.
                       </div>
                    </div>
                    <div class="col-md-4 item">
                       <h4>Пулы</h4>
                       <div class="image">
                          <img class="img-circle" src="http://telekotp.beget.tech/wp-content/themes/maining/img/infogr_pools.jpg" alt="Пулы">
                       </div>
                       <div class="desc">
                          Майнеры подключаются к пулам, их довольно много, поэтому HashFlare дает Вам возможность выбрать к каким пулам подключаться. Это позволяет найти самую прибыльную комбинацию.
                       </div>
                    </div>
                    <div class="col-md-4 item">
                       <h4>Начисления</h4>
                       <div class="image">
                          <img class="img-circle" src="http://telekotp.beget.tech/wp-content/themes/maining/img/infogr_rewards.jpg" alt="Начисления">
                       </div>
                       <div class="desc">
                          Далее, вся добытая криптовалюта разделяется между всеми клиентами HashFlare, в зависимости от их вклада в общую мощность системы.
                       </div>
                    </div>
                 </div>
                 <div class="start">
                    <a href="/#plans" class="btn btn-green btn-lg btn-xl">Start</a>
                 </div>
              </div>
           </div>
        </div>


    </div>
	
<?php get_footer(); ?>


