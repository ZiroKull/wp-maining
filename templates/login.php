<?php
/**
 * Template Name: Login
 */
get_header('crlr'); 
?>

	<section id="wrapper">
        <div class="login-register guest">
            <div class="login-box guest-logo">
				<a href="<?php echo site_url(); ?>">
                    <img src="<?php bloginfo('template_directory');?>/img/logosmblack.png" class="" alt="WPC">
				</a>
            </div>
            <div class="login-box card">
                <div class="card-body">
					<form id="wpcrlLoginForm" class="form-horizontal guest-form" method="post" role="form" novalidate="novalidate">
						<input type="hidden" name="task" value="login" />
						<div class="col-12">
							<h3 class="box-title m-b-20"><?php echo __( 'Войти', 'preico' ) ?></h3>
						</div> 
						<div class="form-group">
							<div class="col-12">
								<label for="Email"><?php echo __( 'Email', 'preico' ) ?></label>
								<input class="form-control" id="Email" name="Email" type="text" value="">
							</div>
						</div>
						<div class="form-group">
							<div class="col-12">
								<label for="Password"><?php echo __( 'Пароль', 'preico' ) ?></label>
								<input class="form-control" data-val="true"id="Password" name="Password" type="password">
							</div>
						</div>						
						<div class="form-group guest-form-additional">
							<div class="col-12">
								<div class="checkbox checkbox-primary float-left p-t-0">
									<input id="RememberMe" name="RememberMe" type="checkbox" value="true">
									<label for="RememberMe"><?php echo __( 'Запомнить меня?', 'preico' ) ?></label>
								</div>
								<a href="<?php echo site_url(); ?>/reset/" id="to-recover" class="text-info float-right"><b><?php echo __( 'Забыли пароль?', 'preico' ) ?></b></a>
							</div>
						</div>						
						<div class="form-group text-center m-t-20">
							<div class="col-12">
								<input type="submit" value="<?php echo __( 'Войти', 'preico' ) ?>" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light">
							</div>
						</div>		
						<div class="form-group m-b-0">
							<div class="col-12 text-center">
								<p><?php echo __( 'Еще не зарегистрированы? ', 'preico' ) ?> <a href="<?php echo site_url(); ?>/register/" class="text-info m-l-5"><b><?php echo __( 'Зарегистрироваться', 'preico' ) ?></b></a></p>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>
    </section>
	
<?php get_footer('crlr'); ?>


