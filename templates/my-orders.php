<?php
/**
 * Template Name: Dashboard My Orders
 */
 
get_header('dash'); 
?>

    
    <div id="main-container">
        <div class="container">
            <div id="my-orders" class="mma mma-bc1">
                <div class="row">
                    <h2 class="col-sm-12"><span></span>Мои заказы в ожидании</h2>
                    <div class="col-sm-12 col-xs-12">
                        <div class="table-responsive trans-usr">
                            <table class="table table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th>Информация о Вашем заказе</th>
                                        <th>Вычислительная мощность</th>
                                        <th>Алгоритм</th>
                                        <th>Код инвойса (счёта на оплату)</th>
                                        <th>Дата создания</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="my-active-orders" class="mma mma-bc1">
                <div class="row">
                    <h2 class="col-sm-12"><span></span>Мои активные заказы</h2>
                    <div class="col-sm-12 col-xs-12">
                        <div class="table-responsive trans-usr">
                            <table class="table table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th>Заказ создан</th>
                                        <th>Дата начала</th>
                                        <th>Дата окончания</th>
                                        <th>Вычислительная мощность</th>
                                        <th>Техобслуживание оплачено до</th>
                                        <th>Тип</th>
                                        <th>Цена</th>
                                        <th>Статус заказа</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="my-completed-orders" class="mma mma-bc1">
                <div class="row">
                    <h2 class="col-sm-12"><span></span>Мои завершенные заказы</h2>
                    <div class="col-sm-12 col-xs-12">
                        <div class="table-responsive trans-usr">
                            <table class="table table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th>Заказ создан</th>
                                        <th>Дата начала</th>
                                        <th>Дата окончания</th>
                                        <th>Вычислительная мощность</th>
                                        <th>Тип</th>
                                        <th>Цена</th>
                                        <th>Статус заказа</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
	
<?php get_footer('dash'); ?>


