<?php
/**
 * Template Name: Forgot password
 */
get_header('crlr'); 

	global $wpdb;

	$err = '';
	$success = '';

	// check if we're in reset form
	if( isset( $_POST['action'] ) && 'reset' == $_POST['action'] )
	{
		$email = trim($_POST['Email']);

		if( ! email_exists( $email ) ) {
			$err = 'Данный email не зарегистрирован.';
		} else {

			$random_password = wp_generate_password( 12, false );
			$user = get_user_by( 'email', $email );

			$update_user = wp_update_user( array (
					'ID' => $user->ID,
					'user_pass' => $random_password
				)
			);

			
			if( $update_user ) 
			{
				$to = $email;
				$subject = 'Восстановление пароля';

				$message = 'Ваш новый пароль: '.$random_password;

				$headers = array('Content-Type: text/html; charset=UTF-8');

				$mail = wp_mail( $to, $subject, $message, $headers );
				
				if( $mail )
				{
					$success = 'Проверьте вашу почту.';
				}
				else 
				{
					$err = 'Ошибка отправки сообщения.';
				}

			} else {
				$err = 'Упс, возникла ошибка, попробуйте еще раз.';
			}

		}
	}

?>

	<section id="wrapper">
        <div class="login-register guest">
            <div class="login-box guest-logo">
				<a href="<?php echo site_url(); ?>">
                    <img src="<?php bloginfo('template_directory');?>/img/logosmblack.png" class="" alt="WPC">
				</a>
            </div>
            <div class="login-box card">
                <div class="card-body">
					<form id="wpcrlResetPasswordForm" class="form-horizontal guest-form" method="post" role="form" novalidate="novalidate">
						<input type="hidden" name="action" value="reset" />
						<div class="col-12">
							<h3 class="box-title m-b-20"><?php echo __( 'Забыли пароль?', 'preico' ) ?></h3>
						</div> 
						<div class="form-group">
							<div class="col-12">
								<label for="Email"><?php echo __( 'Email', 'preico' ) ?></label>
								<input class="form-control" id="Email" name="Email" type="text" value="">
							</div>
						</div>
										
						<div class="form-group text-center m-t-20">
							<div class="col-12">
								<input type="submit" value="<?php echo __( 'Отправить', 'preico' ) ?>" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light">
							</div>
						</div>						
						<div class="form-group m-b-0">
							<div class="col-12 text-center">
								<p><a href="<?php echo site_url(); ?>/login/" class="text-info m-l-5"><b><?php echo __( 'Назад', 'preico' ) ?></b></a></p>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>
    </section>
	
<?php get_footer('crlr'); ?>


