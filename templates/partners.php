<?php
/**
 * Template Name: Partners
 */
 
get_header(); 
?>
        
        <div style="min-height:100%;min-height:54vh;">
           <div id="partners">
              <div class="header">
                 <div class="container">
                    <div class="promo">
                       <div class="text">
                          <p>HashFlare меняет мир криптовалют!</p>
                          <p>Стань партнёром сегодня!</p>
                       </div>
                       <div class="button">
                          <a href="/register" class="btn btn-primary btn-lg btn-big-blue">Стань партнером!</a>
                       </div>
                    </div>
                 </div>
              </div>
              <div class="why">
                 <div class="container">
                    <div class="ribbon">
                       Партнерская программа HashFlare
                    </div>
                    <h1>Почему выгодно работать с HashFlare?</h1>
                    <div class="logo-separator"></div>
                    <div class="items">
                       <div class="row">
                          <div class="col-md-4 item">
                             <div class="image">
                                <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/t1.png" alt="Зарабатывай 10% комиссии">
                             </div>
                             <h2>
                                Зарабатывай 10% комиссии
                             </h2>
                             <p>
                                Наши партнеры получают процент от каждой покупки привлеченного пользователя.
                             </p>
                          </div>
                          <div class="col-md-4 item">
                             <div class="image">
                                <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/t2.png" alt="Программа лояльности">
                             </div>
                             <h2>
                                Программа лояльности
                             </h2>
                             <p>
                                Индивидуальные условия для партнеров с хорошим трафиком, в том числе, возможность заказывать уникальные промо-материалы.
                             </p>
                          </div>
                          <div class="col-md-4 item">
                             <div class="image">
                                <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/t3.png" alt="Выплаты без холда">
                             </div>
                             <h2>
                                Выплаты без холда
                             </h2>
                             <p>
                                Моментальные выплаты на ваш биткоин-кошелек.
                             </p>
                          </div>
                       </div>
                       <div class="row">
                          <div class="col-md-4 item">
                             <div class="image">
                                <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/t4.png" alt="Больше аналитики">
                             </div>
                             <h2>
                                Больше аналитики
                             </h2>
                             <p>
                                Панель с расширенным функционалом для опытных веб-мастеров.
                             </p>
                          </div>
                          <div class="col-md-4 item">
                             <div class="image">
                                <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/t5.png" alt="Онлайн статистика">
                             </div>
                             <h2>
                                Онлайн статистика
                             </h2>
                             <p>
                                Данные по всем конверсиям в реальном времени.
                             </p>
                          </div>
                          <div class="col-md-4 item">
                             <div class="image">
                                <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/t6.png" alt="Промо-материалы">
                             </div>
                             <h2>
                                Промо-материалы
                             </h2>
                             <p>
                                Мы стараемся, чтобы у наших партнеров всегда был большой выбор рекламных материалов.
                             </p>
                          </div>
                       </div>
                       <div class="row">
                          <div class="col-md-4 item">
                             <div class="image">
                                <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/t7.png" alt="Качественный суппорт">
                             </div>
                             <h2>
                                Качественный суппорт
                             </h2>
                             <p>
                                Команда тех.поддержи сможет решить любой самый сложный технический вопрос.
                             </p>
                          </div>
                          <div class="col-md-4 item">
                             <div class="image">
                                <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/t8.png" alt="Высокий процент продаж">
                             </div>
                             <h2>
                                Высокий процент продаж
                             </h2>
                             <p>
                                В среднем 26%* пользователей совершают покупку хешрейта. Из них 67%* делают повторные покупки.
                             </p>
                          </div>
                          <div class="col-md-4 item">
                             <div class="image">
                                <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/t9.png" alt="Надежность">
                             </div>
                             <h2>
                                Надежность
                             </h2>
                             <p>
                                Опыт майнинга с 2013 года позволяет нам предоставить партнерам лучший вариант для заработка.
                             </p>
                          </div>
                       </div>
                    </div>
                    <div class="button">
                       <a href="/register" class="btn btn-primary btn-lg btn-big-blue">Стань партнером!</a>
                    </div>
                    <div class="footnotes">
                       <p>* Кроме вознаграждения за покупку хешрейта с банковской карты. В этом случае время холда может достигать до 30 дней.</p>
                       <p>** Показатели зависят от вида привлекаемого трафика.</p>
                    </div>
                 </div>
              </div>
              <div class="notes">
                 <div class="container">
                    <div class="info">
                       <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/info.png" alt="Info">
                    </div>
                    <h1>Примечания для партнеров</h1>
                    <div class="row">
                       <div class="col-lg-8 col-lg-offset-2">
                          <div class="box">
                             <p>Мы не принимаем следующие виды трафика:</p>
                             <ul>
                                <li>Дорвеи</li>
                                <li>Редиректы</li>
                                <li>Любой вид спама, в том числе и реферальный</li>
                                <li>Контекстная реклама на бренд</li>
                             </ul>
                             <p class="text-danger">
                                За нарушение правил возможны санкции, вплоть до блокировки аккаунта со списанием всех средств.
                             </p>
                          </div>
                       </div>
                    </div>
                    <div class="monitor">
                       <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/monitor.png" alt="Monitor">
                    </div>
                 </div>
              </div>
           </div>
        </div>


    </div>
	
<?php get_footer(); ?>


