<?php
/**
 * Template Name: Registration
 */
get_header('crlr'); 
?>

	<section id="wrapper">
        <div class="login-register guest">
            <div class="login-box guest-logo">
				<a href="<?php echo site_url(); ?>">
                    <img src="<?php bloginfo('template_directory');?>/img/logosmblack.png" class="" alt="WPC">
				</a>
            </div>
            <div class="login-box card">
                <div class="card-body">
					<form id="wpcrlRegisterForm" class="form-horizontal guest-form" method="post" role="form" novalidate="novalidate">
						<input type="hidden" name="task" value="register" />
						<div class="col-12 m-b-20">
							<h3 class="box-title m-b-0"><?php echo __( 'Зарегистрироваться', 'preico' ) ?></h3>
							<small><?php echo __( 'Создать учетную запись', 'preico' ) ?></small>
						</div> 
						<div class="form-group">
							<div class="col-12">
								<label for="Email"><?php echo __( 'Email', 'preico' ) ?></label>
								<input class="form-control" id="Email" name="Email" type="text" value="">
							</div>
						</div>
						<div class="form-group">
							<div class="col-12">
								<label for="Password"><?php echo __( 'Пароль', 'preico' ) ?></label>
								<input class="form-control" data-val="true"id="Password" name="Password" type="password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-12">
								<label for="ConfirmPassword"><?php echo __( 'Подтвердите пароль', 'preico' ) ?></label>
								<input class="form-control" id="ConfirmPassword" name="ConfirmPassword" type="password">
							</div>
						</div>
						<div class="form-group text-center">
							<div class="col-12">
								<input type="submit" value="<?php echo __( 'Зарегистрироваться', 'preico' ) ?>" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light">
							</div>
						</div>
						<div class="form-group m-b-0">
							<div class="col-12 text-center">
								<p><?php echo __( 'Уже зарегистрированы?', 'preico' ) ?> <a href="<?php echo site_url(); ?>/login/" class="text-info m-l-5"><b><?php echo __( 'Войти', 'preico' ) ?></b></a></p>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>
    </section>
	
<?php get_footer('crlr'); ?>


