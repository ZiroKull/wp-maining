<?php
/**
 * Template Name: Main
 */
get_header(); ?>
    
        <div style="min-height:100%;min-height:54vh;">
            <div id="home">
                <header class="intro">
                    <div class="headerbg rubg" style="background:#002626 url(http://telekotp.beget.tech/wp-content/themes/maining/img/bigbg.png) no-repeat center;background-size:cover;">
                        <div class="intro-body">
                            <div class="container">
                                <h1 class="fronthead">Облачный Майнинг</h1>
                            </div>
                        </div>
                    </div>
                    <div class="scroll-trigger"></div>
                </header>
                <section id="features" class="features" data-offset="50">
                    <div class="container">
                        <div class="center">
                            <h2>Невероятные возможности</h2>
                        </div>
                        <div class="row center">
                            <div class="col-xs-6 col-sm-4 feature fadeInLeft wow animated animated" style="visibility: visible;">
                                <h3>Моментальное подключение</h3>
                                <i class="fa fa-plug fa-3x uf pulse"></i>
                                <p class="descr">
                                    Работа оборудования начинается сразу после оплаты. Первые начисления Вы получите в течение суток. 
                                </p>
                            </div>
                            <div class="col-xs-6 col-sm-4 feature fadeInRight wow animated animated" style="visibility: visible;">
                                <h3>Моментальный вывод</h3>
                                <i class="fa fa-btc fa-3x uf pulse"></i>
                                <p class="descr">
                                    Выберите сумму вывода и получите его моментально 
                                </p>
                            </div>
                            <div class="col-xs-6 col-sm-4 feature fadeInLeft wow animated animated" style="visibility: visible;">
                                <h3>Подробная статистика</h3>
                                <i class="fa fa-area-chart fa-3x uf pulse"></i>
                                <p class="descr">
                                    Вы получаете полную информацию по работе вашего аккаунта в любое время дня и ночи. 
                                </p>
                            </div>
                            <!--<div class="col-xs-6 col-sm-4 feature fadeInUp wow animated animated" style="visibility: visible;">
                                <h3>Распределение Мощности</h3>
                                <i class="fa fa-cogs fa-3x uf pulse"></i>
                                <p class="descr">
                                    Вы можете самостоятельно распределить Вашу мощность по различным пулам, чтобы найти самую прибыльную комбинацию. 
                                </p>
                            </div>
                            <div class="col-xs-6 col-sm-4 feature fadeInRight wow animated animated" style="visibility: visible;">
                                <h3>Фиксированная комиссия</h3>
                                <i class="fa fa-calculator fa-3x uf pulse"></i>
                                <p class="descr">
                                    Отсутствие скрытых комиссий. Каждое изменение баланса видно в панели управления. 
                                </p>
                            </div>-->
                        </div>
                        <div class="center">
                            <p class="ps">
                                Наш сервис делает майнинг криптовалют доступным каждому пользователю. Больше не требуется покупать дорогостоящее оборудование и тратить свое время на настройку майнеров. Просто выберите желаемую мощность и получайте доход! 
                            </p>
                        </div>
                    </div>
                </section>
                <!--<section class="newuser" id="newuser">
                    <div class="center">
                       <h2>Новый пользователь?</h2>
                       <button type="button" class="btn btn-green btn-lg btn-xl" data-toggle="modal" data-target="#tv-modal">Видео</button>
                       <div id="tv-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-tv">
                             <div class="modal-content">
                                <div class="embed-responsive embed-responsive-16by9">
                                   <iframe class="embed-responsive-item" width="605" height="340" src="https://www.youtube.com/embed/4YFljaRq0AQ?rel=0" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                    </section>-->
                <section id="plans" class="plans" data-offset="50">
                    <div class="container">
                        <!--<div class="center">
                            <h2>Выберите подходящий тариф</h2>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-3 zoomIn wow animated animated" style="visibility: visible;">
                                        <ul class="pricing-table">
                                            <li class="title">Облачный Майнинг Litecoin</li>
                                            <li class="description">
                                                <strong>Scrypt</strong> майнер
                                            </li>
                                            <li class="bullet-item">Минимальный Хэшрейт:
                                                1 MH/s 
                                            </li>
                                            <li class="bullet-item">
                                                Плата за обслуживание:
                                                0.005 $ / 1 MH/s / 24h
                                            </li>
                                            <li class="bullet-item">Оборудование: HashCoins SCRYPT</li>
                                            <li class="bullet-item">Автоматические начисления в BTC</li>
                                            <li class="bullet-item out-of-stock">Нет в наличии</li>
                                            <li class="bullet-item">1 год контракт</li>
                                            <li class="price">
                                                <span>$1.80</span><br>
                                                за 1 MH/s 
                                            </li>
                                            <li class="cta-button">
                                                <p>
                                                    <span>
                                                    <a href="/register?target=scrypt" class="btn btn-green disabled">Купить сейчас</a> </span>
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3  zoomIn wow animated animated" style="visibility: visible;">
                                        <ul class="pricing-table">
                                            <li class="title">Облачный Майнинг Bitcoin</li>
                                            <li class="description">
                                                <strong>SHA-256</strong> майнер
                                            </li>
                                            <li class="bullet-item">Минимальный Хэшрейт:
                                                10 GH/s 
                                            </li>
                                            <li class="bullet-item">
                                                Плата за обслуживание:
                                                0.0035 $ / 10 GH/s / 24h
                                            </li>
                                            <li class="bullet-item">Оборудование: HashCoins SHA-256</li>
                                            <li class="bullet-item">Автоматические начисления в BTC</li>
                                            <li class="bullet-item out-of-stock">Нет в наличии</li>
                                            <li class="bullet-item">1 год контракт</li>
                                            <li class="price">
                                                <span>$0.60</span><br>
                                                за 10 GH/s 
                                            </li>
                                            <li class="cta-button">
                                                <p>
                                                    <span>
                                                    <a href="/register?target=sha" class="btn btn-green disabled">Купить сейчас</a> </span>
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3  zoomIn wow animated animated" style="visibility: visible;">
                                        <ul class="pricing-table">
                                            <li class="title">Облачный Майнинг Ethereum</li>
                                            <li class="description">
                                                <strong>ETHASH</strong> майнер
                                            </li>
                                            <li class="bullet-item">Минимальный Хэшрейт:
                                                100 KH/s 
                                            </li>
                                            <li class="bullet-item">
                                                Плата за обслуживание:
                                                <strong>Нет</strong>
                                            </li>
                                            <li class="bullet-item">Оборудование: GPU Rigs</li>
                                            <li class="bullet-item">Автоматические начисления в ETH</li>
                                            <li class="bullet-item limited">Количество ограничено</li>
                                            <li class="bullet-item">1 год контракт</li>
                                            <li class="price">
                                                <span>$1.40</span><br>
                                                за 100 KH/s 
                                            </li>
                                            <li class="cta-button">
                                                <p>
                                                    <span>
                                                    <a href="/register?target=ether" class="btn btn-green">Купить сейчас</a> </span>
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3  zoomIn wow animated animated" style="visibility: visible;">
                                        <ul class="pricing-table">
                                            <li class="title">Облачный Майнинг DASH</li>
                                            <li class="description">
                                                <strong>X11</strong> майнер
                                            </li>
                                            <li class="bullet-item">Минимальный Хэшрейт:
                                                1 MH/s 
                                            </li>
                                            <li class="bullet-item">
                                                Плата за обслуживание:
                                                <strong>Нет</strong>
                                            </li>
                                            <li class="bullet-item">Оборудование: Multi-Factor</li>
                                            <li class="bullet-item">Автоматические начисления в DASH</li>
                                            <li class="bullet-item out-of-stock">Нет в наличии</li>
                                            <li class="bullet-item">1 год контракт</li>
                                            <li class="price">
                                                <span>$3.20</span><br>
                                                за 1 MH/s 
                                            </li>
                                            <li class="cta-button">
                                                <p>
                                                    <span>
                                                    <a href="/register?target=dash" class="btn btn-green disabled">Купить сейчас</a> </span>
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="text-center">
                            <p class="ps">
                                Выберите нужный тариф и начните зарабатывать уже в течение 24 часов! 
                            </p>
                        </div>-->
                        
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 ">
                                <div class="widget-col-inner">
                                    <div class="panel panel-primary panel-contract">
                                        <div class="panel-heading text-center">
                                            <h2 class="color-orange"> Настройка Условий Контракта</h2>
                                            <span>
                                            Бессрочный контракт
                                            </span>
                                        </div>
                                        <div class="panel-body">
                                            <form class="tariff-block tariff-custom" action="/register/" method="POST" contract-type="lifetime">
                                                <input type="hidden" name="type" value="lifetime">
                                                <input type="hidden" name="ghs" value="1" class="j-custom-tariff-input">
                                                <div class="widget-big-price-wrapper">
                                                    <div class="widget-big-price-block">
                                                        <div class="widget-big-price-wrapper-currency j-widget-big-price-wrapper-currency"></div>
                                                        <div class="widget-big-price-wrapper-value j-widget-big-price-wrapper-value"></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix tariff-block-blue-text">
                                                    <div class="bc-label">
                                                        <ul class="tariff-block-currency-list">
                                                            <li>
                                                                <input type="radio" name="currency" id="custom-tariff-currency-BTC-HASH(0x7db6378)" value="BTC">
                                                                <label for="custom-tariff-currency-BTC-HASH(0x7db6378)">
                                                                    RUB
                                                                    <span class="j-custom-tariff-price-span" currency="BTC"></span>
                                                                    <div class="custom-tariff-currency-check"></div>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" checked="" name="currency" id="custom-tariff-currency-USD-HASH(0x7db6378)" value="USD">
                                                                <label for="custom-tariff-currency-USD-HASH(0x7db6378)">
                                                                    USD
                                                                    <span class="j-custom-tariff-price-span" currency="USD"></span>
                                                                    <div class="custom-tariff-currency-check"></div>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="currency" id="custom-tariff-currency-EUR-HASH(0x7db6378)" value="EUR">
                                                                <label for="custom-tariff-currency-EUR-HASH(0x7db6378)">
                                                                    EUR
                                                                    <span class="j-custom-tariff-price-span" currency="EUR"></span>
                                                                    <div class="custom-tariff-currency-check"></div>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="bc-input-wrapper bc-input-wrapper-blue widget-ghs-wrapper">
                                                    <input name="bc-power" class="bc-input j-custom-tariff-power-new" value="100 GH/s">
                                                    <div class="bc-input-steps">
                                                        <div class="bc-input-step bc-input-step-up" step="100"></div>
                                                        <div class="bc-input-step bc-input-step-down" step="-100"></div>
                                                    </div>
                                                </div>
                                                <div class="tariff-block-label-wapper clearfix">
                                                    <div class="bc-label pull-left"><span>Доход в месяц</span> <span class="j-widget-label-tooltip fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Примерный доход по текущему курсу BTC/USD 3560.65  "></span></div>
                                                    <div class="bc-label pull-right"> $50</div>
                                                </div>
                                                <!--<div class="tariff-block-label-wapper clearfix">
                                                    <div class="bc-label pull-left"><span>Обслуживание</span> <span class="j-widget-label-tooltip fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Автоматически выплачивается из ежедневно намайненого объема BTC  "></span></div>
                                                    <div class="bc-label pull-right"> $0.00017 за GH/s в день</div>
                                                </div>-->
                                                <div class="tariff-block-button-wrapper text-center">
                                                    <button type="submit" class="btn btn-lg btn-success">Купить</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
                <!-- <section id="features2">
            		<div class="container">
            			<div class="row">
            				<div class="center">
                                <h2>ПРЕИМУЩЕСТВА:</h2>
                            </div>
            				
            				<div class="bxslider">
            					<div>
            						<img src="<?php bloginfo('template_directory');?>/img/icons/fimg1.png">
            						<h3>Качественное постпродажное обслуживание</h3>
            						<p>Наши специалисты по настройке  виртуальной АТС привезут в ваш офис все необходимое оборудование и настроят его, если вы решите, что это необходимо.</p>
            					</div>
            					
            					<div>
            						<img src="<?php bloginfo('template_directory');?>/img/icons/fimg2.png">
            						<h3>Персональный личный кабинет</h3>
            						<p>Личный кабинет сотрудника «оборудован» встроенным вебфоном, с которого совершаются и принимаются звонки. </p>
            					</div>
            					
            					<div>
            						<img src="<?php bloginfo('template_directory');?>/img/icons/fimg3.png">
            						<h3>Телефонизация офиса за 5 мин</h3>
            						<p>Интеграция стационарных офисных или IP-AТС, как правило, занимает не один день. </p>
            					</div>
            					
            					<div>
            						<img src="<?php bloginfo('template_directory');?>/img/icons/fimg1.png">
            						<h3>Качественное постпродажное обслуживание</h3>
            						<p>Наши специалисты по настройке  виртуальной АТС привезут в ваш офис все необходимое оборудование и настроят его, если вы решите, что это необходимо.</p>
            					</div>
            				</div>
            			</div>
            		</div>
            	</section>-->
                
                
               <!-- <section class="monitoring">
                    <div class="center">
                        <h2>Мониторинг в реальном времени</h2>
                        <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/imac.png" class="fadeInUp wow" alt="panel" style="visibility: hidden; animation-name: none;"> 
                    </div>
                </section>-->
                <!--<div class="partners">
                    <div class="container">
                        <img src="http://telekotp.beget.tech/wp-content/themes/maining/img/partners2.jpg" alt="Partners" class="img-responsive">
                    </div>
                </div>
                <div class="testimonials">
                    <div class="testimonialsslider">
                        <div id="carousel-testi" class="carousel slide" data-ride="carousel">
                            <div class="container">
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-testi" data-slide-to="0" class=""></li>
                                    <li data-target="#carousel-testi" data-slide-to="1" class=""></li>
                                    <li data-target="#carousel-testi" data-slide-to="2" class="active"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="item">
                                        <div class="whoclient"><span>Jason Osmond</span></div>
                                        <div class="testimonial-content">
                                            <div class="testimonialimg"><img src="http://telekotp.beget.tech/wp-content/themes/maining/img/jo-200x200.jpg" alt=""></div>
                                            <p>
                                                Работал с HashCoins с начала 2014 года, поэтому с радостью решил поучаствовать в тестировании нового сервиса. Несмотря на мелкие недочеты, в целом сервис очень понравился. Надеюсь, что после старта качество будет только расти. Рекомендую. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="whoclient"><span>Rita Burunova</span></div>
                                        <div class="testimonial-content">
                                            <div class="testimonialimg"><img src="http://telekotp.beget.tech/wp-content/themes/maining/img/hn-200x200.jpg" alt=""></div>
                                            <p>
                                                Сервис произвел приятные впечатления. Чувствуется, что разработчики вложили душу в свою работу. Думаю, главный плюс в том, что можно взять на пробу минимальную мощность за небольшие деньги. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item active">
                                        <div class="whoclient"><span>Tadeusz Wielisław</span></div>
                                        <div class="testimonial-content">
                                            <div class="testimonialimg"><img src="http://telekotp.beget.tech/wp-content/themes/maining/img/sm-200x200.jpg" alt=""></div>
                                            <p>
                                                После тестирования остались только положительные эмоции. Очень понравилось как реализован выбор пулов. В целом система работает интуитивно понятно, все на своем месте и в тоже время ничего лишнего. После выхода из беты однозначно продолжу сотрудничество с HashFlare. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>

    </div>

<?php get_footer(); ?>