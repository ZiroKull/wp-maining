<?php
/**
 * Template Name: Dashboard
 */
 
get_header('dash'); 
?>
        
    <div id="main-container">
        <div class="container">
            <div id="mma-flash" class="mma">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-warning fade in" style="margin-bottom:35px;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times-circle"></i></button>
                            <h4>Важное уведомление</h4>
                            <p>Мы изменили порядок выплат за майнинг на ваш кошелек!                                <br> Начиная с сегодняшнего дня, выплаты за майнинг криптовалюты одного вида по различным контрактам (алгоритмам) будут суммироваться и отправляться на кошелек одним переводом (ID одной транзакции).                                <br> Помните, что суммы общего баланса в ожидании по-прежнему учитываются раздельно для различных контрактов (алгоритмов) и что суммироваться и отправляться одним переводом будут только выплаты, достигшие минимального порога.                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12"></div>
                </div>
            </div>
            <!-- hash power -->
            <div id="current-mining" class="mma mma-bc1">
                
                <div class="row">
                    
                    <div class="col-sm-3">
                        <div class="cmg cmg-1" style="
    height: 205px;
    overflow: hidden;
">
                            
                            <div class="cmc">
    <p>Пакет</p>
                                <b>10.0000 TH/s за 100$</b>
                                <p>Bitcoin                        <small>Ваш хешрейт</small>
                                </p>
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="cmg cmg-1" style="
    height: 205px;
    overflow: hidden;
">
                            
                            <div class="cmc">
                                <b>0.0000 TH/s</b>
                                <p>Bitcoin                        <small>Ваш хешрейт</small>
                                </p>
                                <a href="#" role="button" class="btn btn-warning order-hp-dash">Вывести средства</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                </div>
            </div>
            <!-- hashing power -->
            <!-- button boxes
            <div id="current-mining-boxes" class="mma mma-bc1">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="cmb-allocate">
                            <h3>Распределение мощности</h3>
                            <p>Распределите свою мощность</p>
                            <a href="/panel/buy-power/" role="button" class="btn btn-primary mining-hp-allocation pull-right"><span class="fa fa-signal"></span>Настроить майнеры</a>
                            <svg class="icon gm-icon-dashboard-mining-allocation">
                                <use xlink:href="#gm-icon-dashboard-mining-allocation"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="cmb-upgrade">
                            <h3>купить генерируемую мощность</h3>
                            <p>Купите больше мощности</p>
                            <a href="/panel/buy-power/" role="button" class="btn btn-warning order-hp-dash pull-right"><span class="fa fa-bolt"></span>купить генерируемую мощность</a>
                            <svg class="icon gm-icon-dashboard-upgrade-hashpower">
                                <use xlink:href="#gm-icon-dashboard-upgrade-hashpower"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- button boxes -->
            <div id="my-earnings-sha256" class="mma mma-bc1">
                <div class="row">
                    <h2 class="col-sm-12"><span></span>Диаграмма мощности - SHA-256 (Майнинг биткойнов)</h2>
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>Выработка в USD</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="pull-right panel-btns"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="hero-graph" id="hero-graph-sha256" style="position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                    <svg height="342" version="1.1" width="939" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;">
                                        <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc>
                                        <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                        <text x="49.921875" y="305" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0 USD</tspan>
                                        </text>
                                        <path fill="none" stroke="#aaaaaa" d="M62.421875,305H914" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                        <text x="49.921875" y="235" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0 USD</tspan>
                                        </text>
                                        <path fill="none" stroke="#aaaaaa" d="M62.421875,235H914" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                        <text x="49.921875" y="165" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">1 USD</tspan>
                                        </text>
                                        <path fill="none" stroke="#aaaaaa" d="M62.421875,165H914" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                        <text x="49.921875" y="95" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">1 USD</tspan>
                                        </text>
                                        <path fill="none" stroke="#aaaaaa" d="M62.421875,95H914" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                        <text x="49.921875" y="25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">1 USD</tspan>
                                        </text>
                                        <path fill="none" stroke="#aaaaaa" d="M62.421875,25H914" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                        <text x="883.6316864784546" y="317.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,8.5)">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018-10-31</tspan>
                                        </text>
                                        <text x="760.8930859955424" y="317.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,8.5)">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018-10-27</tspan>
                                        </text>
                                        <text x="639.419831909361" y="317.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,8.5)">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018-10-23</tspan>
                                        </text>
                                        <text x="517.9465778231797" y="317.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,8.5)">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018-10-19</tspan>
                                        </text>
                                        <text x="396.4733237369985" y="317.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,8.5)">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018-10-15</tspan>
                                        </text>
                                        <text x="275.00006965081724" y="317.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,8.5)">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018-10-11</tspan>
                                        </text>
                                        <text x="183.89512908618127" y="317.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,8.5)">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018-10-8</tspan>
                                        </text>
                                        <text x="92.79018852154532" y="317.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 12px sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,8.5)">
                                            <tspan dy="4.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018-10-5</tspan>
                                        </text>
                                        <path fill="none" stroke="#0b62a4" d="M62.421875,305C70.01395338038633,305,85.19811014115899,305,92.79018852154532,305C100.38226690193164,305,115.56642366270431,305,123.15850204309064,305C130.75058042347698,305,145.93473718424963,305,153.52681556463597,305C161.1188939450223,305,176.30305070579493,305,183.89512908618127,305C191.4872074665676,305,206.67136422734026,305,214.2634426077266,305C221.85552098811291,305,237.0396777488856,305,244.6317561292719,305C252.22383450965825,305,267.4079912704309,305,275.00006965081724,305C282.5921480312036,305,297.7763047919762,305,305.36838317236254,305C312.9604615527489,305,328.14461831352156,305,335.7366966939079,305C343.32877507429424,305,358.51293183506687,305,366.1050102154532,305C373.69708859583955,305,388.88124535661217,305,396.4733237369985,305C404.06540211738485,305,419.2495588781575,305,426.8416372585438,305C434.43371563893015,305,449.61787239970283,305,457.2099507800892,305C464.8020291604755,305,479.98618592124814,305,487.5782643016345,305C495.17034268202076,305,510.35449944279344,305,517.9465778231797,305C525.538656203566,305,540.7228129643388,305,548.3148913447251,305C555.9069697251114,305,571.0911264858842,305,578.6832048662704,305C586.2752832466567,305,601.4594400074295,305,609.0515183878158,305C616.6435967682021,305,631.8277535289748,305,639.419831909361,305C647.0119102897474,305,662.19606705052,305,669.7881454309064,305C677.3802238112928,305,692.5643805720654,305,700.1564589524518,305C707.748537332838,305,722.9326940936107,305,730.524772473997,305C738.1168508543833,305,753.3010076151561,305,760.8930859955424,305C768.4851643759287,305,783.6693211367013,305,791.2613995170876,305C799.1698144966567,305,814.9866444557949,305,822.895059435364,305C830.4871378157504,305,845.671294576523,305,853.2633729569094,305C860.8554513372957,305,876.0396080980684,305,883.6316864784546,305C891.2237648588409,305,906.4079216196137,305,914,305" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                        <circle cx="62.421875" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="92.79018852154532" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="123.15850204309064" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="153.52681556463597" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="183.89512908618127" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="214.2634426077266" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="244.6317561292719" cy="305" r="7" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="275.00006965081724" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="305.36838317236254" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="335.7366966939079" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="366.1050102154532" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="396.4733237369985" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="426.8416372585438" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="457.2099507800892" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="487.5782643016345" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="517.9465778231797" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="548.3148913447251" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="578.6832048662704" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="609.0515183878158" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="639.419831909361" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="669.7881454309064" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="700.1564589524518" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="730.524772473997" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="760.8930859955424" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="791.2613995170876" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="822.895059435364" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="853.2633729569094" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="883.6316864784546" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                        <circle cx="914" cy="305" r="4" fill="#0b62a4" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    </svg>
                                    <div class="morris-hover morris-default-style" style="left: 176.632px; top: 243px;">
                                        <div class="morris-hover-row-label">2018-10-10</div>
                                        <div class="morris-hover-point" style="color: #0b62a4">
                                            Bitcoin mining:
                                            0 USD
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display:none;">
                <div id="algos-view" data-value="{&quot;sha256&quot;:{&quot;name&quot;:&quot;sha256&quot;,&quot;nname&quot;:&quot;Bitcoin&quot;,&quot;description&quot;:&quot;Pure Bitcoin mining&quot;,&quot;active&quot;:1,&quot;active_admin&quot;:1,&quot;out_of_stock&quot;:0,&quot;nice_name&quot;:&quot;Bitcoin mining&quot;},&quot;equihash&quot;:{&quot;name&quot;:&quot;equihash&quot;,&quot;nname&quot;:&quot;Zcash&quot;,&quot;description&quot;:&quot;Pure Zcash mining&quot;,&quot;active&quot;:1,&quot;active_admin&quot;:1,&quot;out_of_stock&quot;:1,&quot;nice_name&quot;:&quot;Zcash mining&quot;},&quot;scrypt&quot;:{&quot;name&quot;:&quot;Scrypt&quot;,&quot;nname&quot;:&quot;Litecoin&quot;,&quot;description&quot;:&quot;Pure Litecoin mining&quot;,&quot;active&quot;:1,&quot;active_admin&quot;:1,&quot;out_of_stock&quot;:1,&quot;nice_name&quot;:&quot;Litecoin mining&quot;},&quot;x11&quot;:{&quot;name&quot;:&quot;X11&quot;,&quot;nname&quot;:&quot;Dash&quot;,&quot;description&quot;:&quot;Pure Dash mining&quot;,&quot;active&quot;:1,&quot;active_admin&quot;:1,&quot;out_of_stock&quot;:0,&quot;nice_name&quot;:&quot;Dash mining&quot;},&quot;dagger-hashimoto&quot;:{&quot;name&quot;:&quot;dagger-hashimoto&quot;,&quot;nname&quot;:&quot;Ethash&quot;,&quot;description&quot;:&quot;Pure Ether mining&quot;,&quot;active&quot;:1,&quot;active_admin&quot;:1,&quot;out_of_stock&quot;:0,&quot;nice_name&quot;:&quot;Ether mining&quot;},&quot;cryptonight&quot;:{&quot;name&quot;:&quot;Monero&quot;,&quot;nname&quot;:&quot;Monero&quot;,&quot;description&quot;:&quot;Monero mining&quot;,&quot;active&quot;:1,&quot;active_admin&quot;:1,&quot;out_of_stock&quot;:1,&quot;nice_name&quot;:&quot;Monero mining&quot;}}"></div>
                <div id="algos-chart-data" data-value=""></div>
                <div id="algos-chart-data-all" data-value="{&quot;sha256&quot;:[{&quot;period&quot;:&quot;2018-10-04&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-05&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-06&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-07&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-08&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-09&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-10&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-11&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-12&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-13&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-14&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-15&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-16&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-17&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-18&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-19&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-20&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-21&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-22&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-23&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-24&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-25&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-26&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-27&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-28&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-29&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-30&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-31&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-11-01&quot;,&quot;hash&quot;:0}],&quot;equihash&quot;:[{&quot;period&quot;:&quot;2018-10-04&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-05&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-06&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-07&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-08&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-09&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-10&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-11&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-12&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-13&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-14&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-15&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-16&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-17&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-18&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-19&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-20&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-21&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-22&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-23&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-24&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-25&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-26&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-27&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-28&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-29&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-30&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-31&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-11-01&quot;,&quot;hash&quot;:0}],&quot;scrypt&quot;:[{&quot;period&quot;:&quot;2018-10-04&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-05&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-06&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-07&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-08&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-09&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-10&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-11&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-12&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-13&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-14&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-15&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-16&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-17&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-18&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-19&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-20&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-21&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-22&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-23&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-24&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-25&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-26&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-27&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-28&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-29&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-30&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-31&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-11-01&quot;,&quot;hash&quot;:0}],&quot;x11&quot;:[{&quot;period&quot;:&quot;2018-10-04&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-05&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-06&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-07&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-08&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-09&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-10&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-11&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-12&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-13&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-14&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-15&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-16&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-17&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-18&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-19&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-20&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-21&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-22&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-23&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-24&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-25&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-26&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-27&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-28&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-29&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-30&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-31&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-11-01&quot;,&quot;hash&quot;:0}],&quot;dagger-hashimoto&quot;:[{&quot;period&quot;:&quot;2018-10-04&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-05&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-06&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-07&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-08&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-09&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-10&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-11&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-12&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-13&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-14&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-15&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-16&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-17&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-18&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-19&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-20&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-21&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-22&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-23&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-24&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-25&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-26&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-27&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-28&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-29&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-30&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-31&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-11-01&quot;,&quot;hash&quot;:0}],&quot;cryptonight&quot;:[{&quot;period&quot;:&quot;2018-10-04&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-05&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-06&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-07&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-08&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-09&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-10&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-11&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-12&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-13&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-14&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-15&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-16&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-17&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-18&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-19&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-20&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-21&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-22&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-23&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-24&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-25&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-26&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-27&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-28&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-29&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-30&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-10-31&quot;,&quot;hash&quot;:0},{&quot;period&quot;:&quot;2018-11-01&quot;,&quot;hash&quot;:0}]}"></div>
                <div id="algos-req-ip" data-value="&quot;217.19.216.247&quot;"></div>
            </div>
        </div>
    </div>
	
<?php get_footer('dash'); ?>


