<?php
/**
 * Template Name: Dashboard Buy Power
 */
 
get_header('dash'); 
?>
        
    <div id="main-container">
        <div class="container">
            <h2 class="color-orange"> Настройка Условий Контракта</h2>
            <div id="uhp-expert">
                <div class="row uhpe-row">
                    <div class="col-lg-8 uhpe-rowl">
                        <div class="col-md-12">
                            <div class="widget-col-inner">
                                <div class="panel panel-primary panel-contract">
                                    <div class="panel-heading text-center" style="height: 41px;">
                                        
                                        <span>
                                        Бессрочный контракт
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <form class="tariff-block tariff-custom" action="/register/" method="POST" contract-type="lifetime">
                                            <input type="hidden" name="type" value="lifetime">
                                            <input type="hidden" name="ghs" value="100" class="j-custom-tariff-input">
                                            <div class="widget-big-price-wrapper">
                                                <div class="widget-big-price-block">
                                                    <div class="widget-big-price-wrapper-currency j-widget-big-price-wrapper-currency">USD</div>
                                                    <div class="widget-big-price-wrapper-value j-widget-big-price-wrapper-value">12.50</div>
                                                </div>
                                            </div>
                                            <div class="clearfix tariff-block-blue-text">
                                                <div class="bc-label">
                                                    <ul class="tariff-block-currency-list">
                                                        <li>
                                                            <input type="radio" name="currency" id="custom-tariff-currency-BTC-HASH(0x7db6378)" value="BTC">
                                                            <label for="custom-tariff-currency-BTC-HASH(0x7db6378)">
                                                                RUB
                                                                <span class="j-custom-tariff-price-span" currency="BTC">775.00</span>
                                                                <div class="custom-tariff-currency-check"></div>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" checked="" name="currency" id="custom-tariff-currency-USD-HASH(0x7db6378)" value="USD">
                                                            <label for="custom-tariff-currency-USD-HASH(0x7db6378)">
                                                                USD
                                                                <span class="j-custom-tariff-price-span" currency="USD">12.50</span>
                                                                <div class="custom-tariff-currency-check"></div>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" name="currency" id="custom-tariff-currency-EUR-HASH(0x7db6378)" value="EUR">
                                                            <label for="custom-tariff-currency-EUR-HASH(0x7db6378)">
                                                                EUR
                                                                <span class="j-custom-tariff-price-span" currency="EUR">10.70</span>
                                                                <div class="custom-tariff-currency-check"></div>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="bc-input-wrapper bc-input-wrapper-blue widget-ghs-wrapper">
                                                <input name="bc-power" class="bc-input j-custom-tariff-power-new" value="100 GH/s">
                                                <div class="bc-input-steps">
                                                    <div class="bc-input-step bc-input-step-up" step="100"></div>
                                                    <div class="bc-input-step bc-input-step-down" step="-100"></div>
                                                </div>
                                            </div>
                                            <div class="tariff-block-label-wapper clearfix">
                                                <div class="bc-label pull-left"><span>Доход в месяц</span> <span class="j-widget-label-tooltip fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Примерный доход по текущему курсу BTC/USD 3560.65  "></span></div>
                                                <div class="bc-label pull-right month-get"> $50</div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 uhpe-rowr">
                        <div id="price-reg" class="mma mma-bc1">
                            <div class="row">
                                <h3 class="col-sm-12">Сумма заказов                    <span>Обзор контракта</span></h3>
                                <div class="col-sm-12 prr-3">
                                    <h3>Способ оплаты</h3>
                                    <p>Пожалуйста, выберите способ платежа, например: Webmoney (USD)</p>
                                    <div class="prc">
                                        <div class="bfh-selectbox bfh-currencies payment" data-currency="CC" data-available="BTC,LTC,DOGE,DRK,ZEC,CC" data-blank="false" data-name="data[User][Order][currency]" data-flags="false">
                                            <input type="hidden" name="data[User][Order][currency]" value="CC"><a class="bfh-selectbox-toggle form-control" role="button" data-toggle="bfh-selectbox" href="#"><span class="bfh-selectbox-option">Webmoney (USD)</span><span class="caret selectbox-caret"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 prr-2">
                                    <div class="prc">
                                        <a class="btn btn-warning uhp" href="#">
                                        Продолжить<i class="fa fa-caret-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="/upgrade-hashpower/universal" id="upgrade-hashpower" method="post" accept-charset="utf-8">
                    <div style="display:none;"><input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="data[_Token][key]" value="3ae369a3a95a799c4713e84a03358b139e37d6e7" id="Token472281913"></div>
                    <input type="hidden" name="data[User][Order][currency]" id="order-currency" value="BTC">
                    <input type="hidden" name="data[User][Order][contract_type]" id="order-contract-type" value="lifetime">
                    <input type="hidden" name="data[User][Order][price_init]" id="order-price" value="0.00">
                    <input type="hidden" name="data[User][Order][cc]" id="order-ccpayment" value="1">
                    <div style="display:none;">
                        <input type="hidden" name="data[_Token][fields]" value="bdf4545fa4f88149a2148674a7a5b9dcd5bf4970%3AUser.Order.cc%7CUser.Order.contract_type%7CUser.Order.currency%7CUser.Order.price_init" id="TokenFields841092662">
                        <input type="hidden" name="data[_Token][unlocked]" value="" id="TokenUnlocked382236372"></div>
                </form>
                <div class="row" style="display:none;">
                    <div id="ohp-btn" class="col-sm-12">
                        <a class="btn btn-warning uhp" href="">Продолжить            <i class="fa fa-caret-right"></i></a>
                    </div>
                </div>
                <div style="display:none;">
                    <div id="package-limits" data-value="{&quot;scrypt&quot;:{&quot;1&quot;:14,&quot;2&quot;:13,&quot;3&quot;:12},&quot;sha256&quot;:{&quot;1&quot;:255,&quot;2&quot;:250,&quot;3&quot;:245},&quot;x11&quot;:{&quot;1&quot;:0.0255,&quot;2&quot;:0.02475,&quot;3&quot;:0.024},&quot;dagger-hashimoto&quot;:{&quot;1&quot;:21,&quot;2&quot;:20,&quot;3&quot;:19},&quot;equihash&quot;:{&quot;1&quot;:2.65,&quot;2&quot;:2.6,&quot;3&quot;:2.55},&quot;cryptonight&quot;:{&quot;1&quot;:0.83,&quot;2&quot;:0.82,&quot;3&quot;:0.8},&quot;limits&quot;:{&quot;scrypt&quot;:{&quot;1&quot;:2,&quot;2&quot;:50,&quot;3&quot;:200},&quot;sha256&quot;:{&quot;1&quot;:1,&quot;2&quot;:5,&quot;3&quot;:25},&quot;sha256_2year&quot;:{&quot;1&quot;:1,&quot;2&quot;:5,&quot;3&quot;:25},&quot;x11&quot;:{&quot;1&quot;:3000,&quot;2&quot;:30000,&quot;3&quot;:300000},&quot;dagger-hashimoto&quot;:{&quot;1&quot;:25,&quot;2&quot;:75,&quot;3&quot;:350},&quot;equihash&quot;:{&quot;1&quot;:200,&quot;2&quot;:1000,&quot;3&quot;:3000},&quot;cryptonight&quot;:{&quot;1&quot;:1000,&quot;2&quot;:3000,&quot;3&quot;:9000}},&quot;limits_max&quot;:{&quot;scrypt&quot;:5000,&quot;sha256&quot;:200,&quot;sha256_2year&quot;:200,&quot;x11&quot;:1000000,&quot;dagger-hashimoto&quot;:500,&quot;equihash&quot;:5000,&quot;cryptonight&quot;:15000},&quot;limits_scale&quot;:{&quot;scrypt&quot;:{&quot;1&quot;:&quot;0 MH\/s&quot;,&quot;2&quot;:&quot;&quot;,&quot;3&quot;:&quot;5000 MH\/s&quot;},&quot;sha256&quot;:{&quot;1&quot;:&quot;GH\/s&quot;,&quot;2&quot;:&quot;&quot;,&quot;3&quot;:&quot;200,000 GH\/s&quot;},&quot;sha256_2year&quot;:{&quot;1&quot;:&quot;GH\/s&quot;,&quot;2&quot;:&quot;&quot;,&quot;3&quot;:&quot;200,000 GH\/s&quot;},&quot;x11&quot;:{&quot;1&quot;:&quot;0 MH\/s&quot;,&quot;2&quot;:&quot;&quot;,&quot;3&quot;:&quot;1,000,000 MH\/s&quot;},&quot;dagger-hashimoto&quot;:{&quot;1&quot;:&quot;0 MH\/s&quot;,&quot;2&quot;:&quot;&quot;,&quot;3&quot;:&quot;500 MH\/s&quot;},&quot;equihash&quot;:{&quot;1&quot;:&quot;0 H\/s&quot;,&quot;2&quot;:&quot;&quot;,&quot;3&quot;:&quot;5,000 H\/s&quot;},&quot;cryptonight&quot;:{&quot;1&quot;:&quot;0 H\/s&quot;,&quot;2&quot;:&quot;&quot;,&quot;3&quot;:&quot;15,000 H\/s&quot;}},&quot;unit&quot;:{&quot;scrypt&quot;:&quot;kH\/s&quot;,&quot;sha256&quot;:&quot;GH\/s&quot;,&quot;x11&quot;:&quot;kH\/s&quot;,&quot;dagger-hashimoto&quot;:&quot;kH\/s&quot;,&quot;equihash&quot;:&quot;H\/s&quot;,&quot;cryptonight&quot;:&quot;H\/s&quot;},&quot;unit_db&quot;:{&quot;scrypt&quot;:&quot;MH\/s&quot;,&quot;sha256&quot;:&quot;TH\/s&quot;,&quot;sha256_2year&quot;:&quot;TH\/s&quot;,&quot;x11&quot;:&quot;MH\/s&quot;,&quot;dagger-hashimoto&quot;:&quot;MH\/s&quot;,&quot;equihash&quot;:&quot;H\/s&quot;,&quot;cryptonight&quot;:&quot;H\/s&quot;,&quot;cryptonightv0&quot;:&quot;H\/s&quot;},&quot;divide&quot;:{&quot;scrypt&quot;:1000,&quot;sha256&quot;:1000,&quot;x11&quot;:1000,&quot;dagger-hashimoto&quot;:1000,&quot;equihash&quot;:1000,&quot;cryptonight&quot;:1000},&quot;multiply_hash_contract&quot;:{&quot;scrypt&quot;:1000,&quot;sha256&quot;:1000,&quot;x11&quot;:1000,&quot;dagger-hashimoto&quot;:1000,&quot;equihash&quot;:1,&quot;cryptonight&quot;:1},&quot;deduct&quot;:{&quot;scrypt&quot;:0,&quot;sha256&quot;:0,&quot;x11&quot;:0,&quot;dagger-hashimoto&quot;:0,&quot;equihash&quot;:0,&quot;cryptonight&quot;:0},&quot;slider&quot;:{&quot;scrypt&quot;:{&quot;min&quot;:0,&quot;max&quot;:1000,&quot;step&quot;:0.005,&quot;steps&quot;:[0,2,7.5,10,15,20,40,50,60,70,80,100,200,300,400,500,1000,5000],&quot;steps_cc&quot;:[0,2,7.5,10,15,20,40,50,60,70,80,100,200,300,400,500,1000,5000],&quot;steps_1year&quot;:[0,2,7.5,10,15,20,40,50,60,70,80,100,200,300,400,500,1000,5000],&quot;steps_2year&quot;:[0,2,7.5,10,15,20,40,50,60,70,80,100,200,300,400,500,1000,5000],&quot;steps_lifetime&quot;:[0,5,7.5,10,15,20,40,50,60,70,80,100,200,300,400,500,1000,5000],&quot;default_lifetime&quot;:0,&quot;default_1year&quot;:0,&quot;default_2year&quot;:0,&quot;default_pos_lifetime&quot;:0,&quot;default_pos_1year&quot;:0,&quot;default_pos_2year&quot;:0,&quot;default&quot;:0,&quot;unit&quot;:&quot;MH\/s&quot;},&quot;sha256&quot;:{&quot;min&quot;:0,&quot;max&quot;:500,&quot;step&quot;:0.005,&quot;steps&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;steps_cc&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;steps_1year&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;steps_5year&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;steps_lifetime&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;default_lifetime&quot;:0,&quot;default_1year&quot;:0,&quot;default_5year&quot;:0,&quot;default_pos_lifetime&quot;:0,&quot;default_pos_1year&quot;:0,&quot;default_pos_5year&quot;:0,&quot;default&quot;:0,&quot;unit&quot;:&quot;TH\/s&quot;},&quot;sha256_2year&quot;:{&quot;min&quot;:0,&quot;max&quot;:500,&quot;step&quot;:0.005,&quot;steps&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;steps_cc&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;steps_1year&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;steps_2year&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;steps_5year&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;steps_lifetime&quot;:[0,1,1.5,2,2.25,2.5,5,7.5,10,15,20,25,38,40,62,100,200],&quot;default_lifetime&quot;:0,&quot;default_1year&quot;:0,&quot;default_2year&quot;:0,&quot;default_5year&quot;:0,&quot;default_pos_lifetime&quot;:0,&quot;default_pos_1year&quot;:0,&quot;default_pos_5year&quot;:0,&quot;default&quot;:0,&quot;unit&quot;:&quot;TH\/s&quot;},&quot;x11&quot;:{&quot;min&quot;:0,&quot;max&quot;:1000,&quot;step&quot;:0.005,&quot;steps&quot;:[0,3000,7500,10000,15000,20000,30000,40000,50000,60000,70000,80000,100000,200000,300000,400000,500000,1000000],&quot;steps_cc&quot;:[0,3000,7500,10000,15000,20000,30000,40000,50000,60000,70000,80000,100000,200000,300000,400000,500000,1000000],&quot;steps_1year&quot;:[0,3000,7500,10000,15000,20000,30000,40000,50000,60000,70000,80000,100000,200000,300000,400000,500000,1000000],&quot;steps_2year&quot;:[0,3000,7500,10000,15000,20000,30000,40000,50000,60000,70000,80000,100000,200000,300000,400000,500000,1000000],&quot;steps_lifetime&quot;:[0,3000,7500,10000,15000,20000,30000,40000,50000,60000,70000,80000,100000,200000,300000,400000,500000,1000000],&quot;default_lifetime&quot;:0,&quot;default_1year&quot;:0,&quot;default_2year&quot;:0,&quot;default_pos_lifetime&quot;:0,&quot;default_pos_1year&quot;:0,&quot;default_pos_2year&quot;:0,&quot;default&quot;:0,&quot;unit&quot;:&quot;MH\/s&quot;},&quot;dagger-hashimoto&quot;:{&quot;min&quot;:0,&quot;max&quot;:500,&quot;step&quot;:0.005,&quot;steps&quot;:[0,25,50,55,75,80,90,100,120,175,250,350,500],&quot;steps_cc&quot;:[0,25,50,55,75,80,90,100,120,175,250,350,500],&quot;steps_1year&quot;:[0,25,50,55,75,80,90,100,120,175,250,350,500],&quot;steps_2year&quot;:[0,25,50,55,75,80,90,100,120,175,250,350,500],&quot;steps_lifetime&quot;:[0,25,50,55,75,80,90,100,120,175,250,350,500],&quot;default_lifetime&quot;:0,&quot;default_1year&quot;:0,&quot;default_2year&quot;:0,&quot;default_pos_lifetime&quot;:0,&quot;default_pos_1year&quot;:0,&quot;default_pos_2year&quot;:0,&quot;default&quot;:0,&quot;unit&quot;:&quot;MH\/s&quot;},&quot;equihash&quot;:{&quot;min&quot;:0,&quot;max&quot;:5000,&quot;step&quot;:0.005,&quot;steps_cc&quot;:[0,200,400,600,1000,1500,2000,3000,4000,5000],&quot;steps_1year&quot;:[0,200,400,600,1000,1500,2000,3000,4000,5000],&quot;steps_2year&quot;:[0,200,400,600,1000,1500,2000,3000,4000,5000],&quot;steps_lifetime&quot;:[0,200,400,600,1000,1500,2000,3000,4000,5000],&quot;default_lifetime&quot;:0,&quot;default_1year&quot;:0,&quot;default_2year&quot;:0,&quot;default_pos_lifetime&quot;:0,&quot;default_pos_1year&quot;:0,&quot;default_pos_2year&quot;:0,&quot;default&quot;:0,&quot;unit&quot;:&quot;H\/s&quot;},&quot;cryptonight&quot;:{&quot;min&quot;:0,&quot;max&quot;:15000,&quot;step&quot;:0.005,&quot;steps&quot;:[0,1000,1300,1700,2300,2700,3000,5000,7500,9000,10000,15000],&quot;steps_cc&quot;:[0,1000,1300,1700,2300,2700,3000,5000,7500,9000,10000,15000],&quot;steps_1year&quot;:[0,1000,1300,1700,2300,2700,3000,5000,7500,9000,10000,15000],&quot;steps_2year&quot;:[0,1000,1300,1700,2300,2700,3000,5000,7500,9000,10000,15000],&quot;steps_lifetime&quot;:[0,1000,1300,1700,2300,2700,3000,5000,7500,9000,10000,15000],&quot;default_lifetime&quot;:0,&quot;default_1year&quot;:0,&quot;default_2year&quot;:0,&quot;default_pos_lifetime&quot;:0,&quot;default_pos_1year&quot;:0,&quot;default_pos_2year&quot;:0,&quot;default&quot;:0,&quot;unit&quot;:&quot;H\/s&quot;}},&quot;year_limits&quot;:{&quot;1&quot;:1,&quot;2&quot;:1.5,&quot;3&quot;:2,&quot;4&quot;:2.5,&quot;5&quot;:3},&quot;year_prices_5year&quot;:{&quot;sha256&quot;:{&quot;1&quot;:{&quot;1&quot;:245,&quot;2&quot;:250,&quot;3&quot;:255}}},&quot;year_prices_2year&quot;:{&quot;sha256_2year&quot;:{&quot;1&quot;:{&quot;1&quot;:50,&quot;2&quot;:50,&quot;3&quot;:50}}},&quot;year_prices_1year&quot;:{&quot;sha256&quot;:{&quot;1&quot;:{&quot;1&quot;:290}},&quot;x11&quot;:{&quot;1&quot;:{&quot;1&quot;:0.024,&quot;2&quot;:0.02475,&quot;3&quot;:0.0255},&quot;2&quot;:{&quot;1&quot;:6,&quot;2&quot;:7,&quot;3&quot;:8}},&quot;dagger-hashimoto&quot;:{&quot;1&quot;:{&quot;1&quot;:19,&quot;2&quot;:20,&quot;3&quot;:21}},&quot;scrypt&quot;:{&quot;1&quot;:{&quot;1&quot;:12,&quot;2&quot;:13,&quot;3&quot;:14}},&quot;equihash&quot;:{&quot;1&quot;:{&quot;1&quot;:2.55,&quot;2&quot;:2.6,&quot;3&quot;:2.65},&quot;2&quot;:{&quot;1&quot;:10.38,&quot;2&quot;:11.12,&quot;3&quot;:11.5},&quot;3&quot;:{&quot;1&quot;:13.5,&quot;2&quot;:14.45,&quot;3&quot;:14.95},&quot;4&quot;:{&quot;1&quot;:26.99,&quot;2&quot;:28.9,&quot;3&quot;:29.9},&quot;5&quot;:{&quot;1&quot;:34.67,&quot;2&quot;:37.33,&quot;3&quot;:38.67},&quot;6&quot;:{&quot;1&quot;:43.333,&quot;2&quot;:46.667,&quot;3&quot;:48.333},&quot;7&quot;:{&quot;1&quot;:86.667,&quot;2&quot;:93.337,&quot;3&quot;:96.667},&quot;8&quot;:{&quot;1&quot;:81.557,&quot;2&quot;:91.557,&quot;3&quot;:101.557},&quot;9&quot;:{&quot;1&quot;:90.217,&quot;2&quot;:100.217,&quot;3&quot;:110.217}},&quot;cryptonight&quot;:{&quot;1&quot;:{&quot;1&quot;:0.8,&quot;2&quot;:0.82,&quot;3&quot;:0.83}}},&quot;year_prices&quot;:{&quot;scrypt&quot;:{&quot;1&quot;:{&quot;1&quot;:12,&quot;2&quot;:13,&quot;3&quot;:14},&quot;2&quot;:{&quot;1&quot;:46.569,&quot;2&quot;:51.569,&quot;3&quot;:56.569},&quot;3&quot;:{&quot;1&quot;:52.245,&quot;2&quot;:57.245,&quot;3&quot;:62.245},&quot;4&quot;:{&quot;1&quot;:57.33,&quot;2&quot;:62.33,&quot;3&quot;:67.33},&quot;5&quot;:{&quot;1&quot;:62.652,&quot;2&quot;:67.652,&quot;3&quot;:72.652}},&quot;sha256&quot;:{&quot;1&quot;:{&quot;1&quot;:245,&quot;2&quot;:250,&quot;3&quot;:255},&quot;2&quot;:{&quot;1&quot;:160,&quot;2&quot;:170,&quot;3&quot;:190},&quot;3&quot;:{&quot;1&quot;:2650,&quot;2&quot;:3000,&quot;3&quot;:3400},&quot;4&quot;:{&quot;1&quot;:3024,&quot;2&quot;:3374,&quot;3&quot;:3774},&quot;5&quot;:{&quot;1&quot;:275,&quot;2&quot;:280,&quot;3&quot;:285},&quot;6&quot;:{&quot;1&quot;:3711,&quot;2&quot;:4061,&quot;3&quot;:4461}},&quot;sha256_2year&quot;:{&quot;1&quot;:{&quot;1&quot;:50,&quot;2&quot;:50,&quot;3&quot;:50},&quot;2&quot;:{&quot;1&quot;:160,&quot;2&quot;:170,&quot;3&quot;:190},&quot;3&quot;:{&quot;1&quot;:2650,&quot;2&quot;:3000,&quot;3&quot;:3400},&quot;4&quot;:{&quot;1&quot;:3024,&quot;2&quot;:3374,&quot;3&quot;:3774},&quot;5&quot;:{&quot;1&quot;:275,&quot;2&quot;:280,&quot;3&quot;:285},&quot;6&quot;:{&quot;1&quot;:3711,&quot;2&quot;:4061,&quot;3&quot;:4461}},&quot;x11&quot;:{&quot;1&quot;:{&quot;1&quot;:0.024,&quot;2&quot;:0.02475,&quot;3&quot;:0.0255},&quot;2&quot;:{&quot;1&quot;:6,&quot;2&quot;:7,&quot;3&quot;:8},&quot;3&quot;:{&quot;1&quot;:71.89,&quot;2&quot;:81.89,&quot;3&quot;:91.89},&quot;4&quot;:{&quot;1&quot;:81.557,&quot;2&quot;:91.557,&quot;3&quot;:101.557},&quot;5&quot;:{&quot;1&quot;:99.28,&quot;2&quot;:109.28,&quot;3&quot;:119.28}},&quot;dagger-hashimoto&quot;:{&quot;1&quot;:{&quot;1&quot;:19,&quot;2&quot;:20,&quot;3&quot;:21},&quot;2&quot;:{&quot;1&quot;:71.89,&quot;2&quot;:81.89,&quot;3&quot;:91.89},&quot;3&quot;:{&quot;1&quot;:81.557,&quot;2&quot;:91.557,&quot;3&quot;:101.557},&quot;4&quot;:{&quot;1&quot;:90.217,&quot;2&quot;:100.217,&quot;3&quot;:110.217},&quot;5&quot;:{&quot;1&quot;:99.28,&quot;2&quot;:109.28,&quot;3&quot;:119.28}},&quot;equihash&quot;:{&quot;1&quot;:{&quot;1&quot;:2.55,&quot;2&quot;:2.6,&quot;3&quot;:2.65},&quot;2&quot;:{&quot;1&quot;:10.38,&quot;2&quot;:11.12,&quot;3&quot;:11.5},&quot;3&quot;:{&quot;1&quot;:13.5,&quot;2&quot;:14.45,&quot;3&quot;:14.95},&quot;4&quot;:{&quot;1&quot;:26.99,&quot;2&quot;:28.9,&quot;3&quot;:29.9},&quot;5&quot;:{&quot;1&quot;:34.67,&quot;2&quot;:37.33,&quot;3&quot;:38.67},&quot;6&quot;:{&quot;1&quot;:43.333,&quot;2&quot;:46.667,&quot;3&quot;:48.333},&quot;7&quot;:{&quot;1&quot;:86.667,&quot;2&quot;:93.337,&quot;3&quot;:96.667},&quot;8&quot;:{&quot;1&quot;:81.557,&quot;2&quot;:91.557,&quot;3&quot;:101.557},&quot;9&quot;:{&quot;1&quot;:90.217,&quot;2&quot;:100.217,&quot;3&quot;:110.217}},&quot;cryptonight&quot;:{&quot;1&quot;:{&quot;1&quot;:0.8,&quot;2&quot;:0.82,&quot;3&quot;:0.83},&quot;2&quot;:{&quot;1&quot;:46.569,&quot;2&quot;:51.569,&quot;3&quot;:56.569},&quot;3&quot;:{&quot;1&quot;:52.245,&quot;2&quot;:57.245,&quot;3&quot;:62.245},&quot;4&quot;:{&quot;1&quot;:57.33,&quot;2&quot;:62.33,&quot;3&quot;:67.33},&quot;5&quot;:{&quot;1&quot;:62.652,&quot;2&quot;:67.652,&quot;3&quot;:72.652}}},&quot;year_prices_spondoolies&quot;:{&quot;scrypt&quot;:{&quot;1&quot;:{&quot;1&quot;:9.49,&quot;2&quot;:9.75,&quot;3&quot;:9.99},&quot;2&quot;:{&quot;1&quot;:46.569,&quot;2&quot;:51.569,&quot;3&quot;:56.569},&quot;3&quot;:{&quot;1&quot;:52.245,&quot;2&quot;:57.245,&quot;3&quot;:62.245},&quot;4&quot;:{&quot;1&quot;:57.33,&quot;2&quot;:62.33,&quot;3&quot;:67.33},&quot;5&quot;:{&quot;1&quot;:62.652,&quot;2&quot;:67.652,&quot;3&quot;:72.652}},&quot;sha256&quot;:{&quot;1&quot;:{&quot;1&quot;:160,&quot;2&quot;:170,&quot;3&quot;:190},&quot;2&quot;:{&quot;1&quot;:2650,&quot;2&quot;:3000,&quot;3&quot;:3400},&quot;3&quot;:{&quot;1&quot;:3024,&quot;2&quot;:3374,&quot;3&quot;:3774},&quot;4&quot;:{&quot;1&quot;:3360,&quot;2&quot;:3710,&quot;3&quot;:4110},&quot;5&quot;:{&quot;1&quot;:3711,&quot;2&quot;:4061,&quot;3&quot;:4461}},&quot;x11&quot;:{&quot;1&quot;:{&quot;1&quot;:0.024,&quot;2&quot;:0.02475,&quot;3&quot;:0.0255},&quot;2&quot;:{&quot;1&quot;:71.89,&quot;2&quot;:81.89,&quot;3&quot;:91.89},&quot;3&quot;:{&quot;1&quot;:81.557,&quot;2&quot;:91.557,&quot;3&quot;:101.557},&quot;4&quot;:{&quot;1&quot;:90.217,&quot;2&quot;:100.217,&quot;3&quot;:110.217},&quot;5&quot;:{&quot;1&quot;:99.28,&quot;2&quot;:109.28,&quot;3&quot;:119.28}},&quot;equihash&quot;:{&quot;1&quot;:{&quot;1&quot;:1.76,&quot;2&quot;:1.84,&quot;3&quot;:1.92},&quot;2&quot;:{&quot;1&quot;:10.38,&quot;2&quot;:11.12,&quot;3&quot;:11.5},&quot;3&quot;:{&quot;1&quot;:13.5,&quot;2&quot;:14.45,&quot;3&quot;:14.95},&quot;4&quot;:{&quot;1&quot;:26.99,&quot;2&quot;:28.9,&quot;3&quot;:29.9},&quot;5&quot;:{&quot;1&quot;:34.67,&quot;2&quot;:37.33,&quot;3&quot;:38.67},&quot;6&quot;:{&quot;1&quot;:43.333,&quot;2&quot;:46.667,&quot;3&quot;:48.333},&quot;7&quot;:{&quot;1&quot;:86.667,&quot;2&quot;:93.337,&quot;3&quot;:96.667},&quot;8&quot;:{&quot;1&quot;:81.557,&quot;2&quot;:91.557,&quot;3&quot;:101.557},&quot;9&quot;:{&quot;1&quot;:90.217,&quot;2&quot;:100.217,&quot;3&quot;:110.217}}}}"></div>
                    <div id="conversion-rates" data-value="{&quot;USD&quot;:1,&quot;CC&quot;:1,&quot;BTC&quot;:3577.0482,&quot;CNY&quot;:0.14767970764729,&quot;CC_QWIPI_UNIONPAY&quot;:0.14767970764729,&quot;EUR&quot;:1.1396000000011,&quot;LTC&quot;:30.327144672132,&quot;BCH&quot;:254.85313038431,&quot;DOGE&quot;:0.002003146992,&quot;DRK&quot;:70.138725335118,&quot;TBTC&quot;:70.138725335118,&quot;ZEC&quot;:52.242788961}"></div>
                    <div id="currency-symbols" data-value="{&quot;USD&quot;:&quot;$&quot;,&quot;EUR&quot;:&quot;\u20ac&quot;,&quot;BTC&quot;:&quot;\u0e3f&quot;,&quot;BCH&quot;:&quot;\u0e3f&quot;,&quot;LTC&quot;:&quot;\u0141&quot;,&quot;DOGE&quot;:&quot;\u0110&quot;,&quot;DRK&quot;:&quot;DRK&quot;,&quot;CC&quot;:&quot;$&quot;}"></div>
                    <div id="defaults" data-value="{&quot;x11_discount&quot;:false,&quot;currency&quot;:&quot;CC&quot;,&quot;hash_sha256&quot;:0,&quot;ct_sha256&quot;:&quot;5year&quot;,&quot;hash_sha256_2year&quot;:0,&quot;ct_sha256_2year&quot;:&quot;2year&quot;,&quot;hash_equihash&quot;:0,&quot;ct_equihash&quot;:&quot;2year&quot;,&quot;hash_scrypt&quot;:0,&quot;ct_scrypt&quot;:&quot;2year&quot;,&quot;hash_x11&quot;:0,&quot;ct_x11&quot;:&quot;2year&quot;,&quot;hash_dagger-hashimoto&quot;:0,&quot;ct_dagger-hashimoto&quot;:&quot;2year&quot;,&quot;hash_cryptonight&quot;:0,&quot;ct_cryptonight&quot;:&quot;2year&quot;,&quot;zero_message&quot;:&quot;\u041c\u043e\u0449\u043d\u043e\u0441\u0442\u044c \u0434\u043e\u043b\u0436\u043d\u0430 \u0431\u044b\u0442\u044c \u0431\u043e\u043b\u044c\u0448\u0435 \u043d\u0443\u043b\u044f!&quot;}"></div>
                    <div id="active-algos" data-value="[]"></div>
                </div>
            </div>
        </div>
    </div>
	
<?php get_footer('dash'); ?>


