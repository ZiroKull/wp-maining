<!doctype html>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <link rel="icon" type="image/ico" href="http://telekotp.beget.tech/wp-content/themes/maining/img/faviconpmr.ico" sizes="32x32">
    <link rel="icon" type="image/ico" href="http://telekotp.beget.tech/wp-content/themes/maining/img/faviconpmr.ico" sizes="16x16">
    <link rel="shortcut icon" href="http://telekotp.beget.tech/wp-content/themes/maining/img/faviconpmr.ico?v=5">
    
    <meta name="theme-color" content="#1a1ee7">
	<?php wp_head(); ?>
	<!--[if lt IE 9]>
	    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body <?php body_class(); ?>>
    <div class="wrapper" style="min-height:100%;position:relative;">
        <div class="inner-wrapper">
            <nav class="navbar scrollable navbar-custom" role="navigation">
               <div class="container">
                  <div class="navbar-header scrollable">
                     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                     <i class="fa fa-bars"></i>
                     </button>
                     <a class="navbar-brand" href="/">
                     <i class="fa fa-cloud"></i>
                     HashFlare </a>
                  </div>
                  <div class="collapse navbar-collapse navbar-right navbar-main-collapse scrollable">
                     <ul class="nav navbar-nav">
                        <li class="hidden">
                           <a href="#page-top"></a>
                        </li>
                        <li>
                           <a class="btn-nav page-scroll" href="#features"> Возможности</a>
                        </li>
                        <li>
                           <a class="btn-nav page-scroll" href="#plans"> Цена</a>
                        </li>
                        <li class="dropdown">
                           <a class="btn-nav dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                           Что это? <span class="caret"></span>
                           </a>
                           <ul class="dropdown-menu" role="menu">
                              <li><a href="/how-it-works" class="btn-nav">Как это работает?</a></li>
                              <li><a href="/what-is-bitcoin" class="btn-nav">Что такое Bitcoin?</a></li>
                              <li><a href="/what-is-mining" class="btn-nav">Что такое майнинг?</a></li>
                           </ul>
                        </li>
                        <?php if (!is_user_logged_in()) { ?>
                            <li>
                               <a href="/singin/" class="btn-nav">Войти</a>
                            </li>
                            <li class="register">
                               <a href="/register/" class="btn-nav">Регистрация</a>
                            </li>
                        <?php } else {  ?>
                            <li>
                               <a href="/panel/" class="btn-nav">Личный кабинет</a>
                            </li>
                            <li>
                               <a href="<?php echo wp_logout_url( home_url() ); ?>" class="btn-nav">Выход</a>
                            </li>
                        <?php } ?>
                     </ul>
                  </div>
               </div>
            </nav>
            <div class="nav-placeholder-stat"></div>

