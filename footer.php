        <footer>
           <div class="container">
              <div class="menu">
                 <div class="row">
                    <div class="col-sm-4 col-xs-6">
                       <div class="header">
                          Компания 
                       </div>
                       <ul class="list-unstyled">
                          <li>
                             <a href="/company/">О нас</a>
                          </li>
                          <li>
                             <a href="/terms/">Условия Пользования</a>
                          </li>
                          <li>
                             <a href="/privacy-policy/">Privacy Policy</a>
                          </li>
                       </ul>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                       <div class="header">
                          Помощь 
                       </div>
                       <ul class="list-unstyled">
                          <li>
                             <a href="/what-is-bitcoin/">Что такое Bitcoin?</a>
                          </li>
                          <li>
                             <a href="/what-is-mining/">Что такое майнинг?</a>
                          </li>
                          <li>
                             <a href="/how-it-works/">Как это работает?</a>
                          </li>
                       </ul>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                       <div class="header">
                          Контакты 
                       </div>
                       <ul class="list-unstyled">
                          <li>
                             <a href="mailto:info@hashflare.io">info@hashflare.io</a>
                          </li>
                          <li>
                             HashFlare LP
                          </li>
                          <li>
                             Company number SL024271
                          </li>
                          <li>
                             44/46 Morningside Road, Edinburgh
                          </li>
                          <li>
                             Scotland, UK, EH10 4BF
                          </li>
                       </ul>
                    </div>
                 </div>
              </div>
              <div class="copyright">
                Сайт под управлением PMRMaining<br><br>
              </div>
           </div>
        </footer>    
    
    </div>
    <?php wp_footer(); ?>
    <script>
        function formatCurrency( value, currency, decimals ) {
            /*if ( currency === 'RUB' ) {
                return formatBTC( value, true );
            }*/
        
            var s = parseFloat(value);
            s = s.toFixed(decimals || 2)
            return decimals ? s.replace(/0+$/g, '') : s;
        }
        
        function formatBTC( value, noTriads ) {
            var s = parseFloat(value).toFixed(8);
            s = s.replace(/0+$/, '');
            s = s.replace(/\.$/, '');
        
            return noTriads || +s < 1000 ? s : triads(s);
        }
        
        function triads( src_str, ch ) {
            if ( ! $.isNumeric(src_str) ) return src_str;
        
            var n_str = '' + src_str, x = n_str.split('.'), x1 = x[0];
            if ( x1.length < 4 ) return src_str;
            var rgx = /(\d+)(\d{3})/, x2 = x.length > 1 ? '.' + x[1] : '';
        
            ch = ch || '&nbsp;';
            while ( rgx.test(x1) ) {
                x1 = x1.replace( rgx, '$1' + ch + '$2' );
            }
            return x1 + x2;
        }
        
        var Obj = { };
        
        (function(){
        Obj.apply = function( dst, src, defaults ) {
            if (defaults) {
                Obj.apply( dst, defaults );
            }
            if ( src && 'object' === typeof src && dst ) {
                for ( var prop in src ) {
                    dst[prop] = src[prop];
                }
            }
            return dst;
        };
        
        Obj.apply( Function.prototype, {
            createCallback: function(/* some args... */) {
                var args = arguments, meth = this;
                return function() {
                    return meth.apply( window, args );
                };
            },
            createDelegate: function( scope, args, append_args ) {
                var meth = this;
                return function() {
                    var call_args = args || arguments;
                    if ( append_args === true ) {
                        call_args = Array.prototype.slice.call( arguments, 0 );
                        call_args = call_args.concat(args);
                    }
                    else if ( $.isNumeric(append_args) ) {
                        // copy arguments first
                        call_args = Array.prototype.slice.call( arguments, 0 );
                        // create method call params
                        var apply_args = [ append_args, 0 ].concat(args);
                        // splice them in
                        Array.prototype.splice.apply( call_args, apply_args );
                    }
                    return meth.apply( scope || window, call_args );
                };
            }
        } );
        
        Obj.apply( String, {
        
            format: function() {
                var s = arguments[0];
                for ( var i = 0; i < arguments.length - 1; i++ ) {
                    var reg = new RegExp( '\\{' + i + '\\}', 'gm' );
                    s = s.replace( reg, arguments[i + 1] );
                }
                return s;
            }
        
        } );
        
        window.sformat = String.format;
        
        })();
    
        var minHosts = 1,
            convertMultiplier = 1000,
            ghsPerHost = 100;
        
        var pricePerThs = {},
            pricePerThsDefault = {},
            maxHosts = {};
        maxHosts["lifetime"] = 10000;
        pricePerThs["lifetime"] = {};
        pricePerThs["lifetime"]["BTC"] = 7750.0;
        pricePerThs["lifetime"]["USD"] = 125;
        pricePerThs["lifetime"]["EUR"] = 107;
        
        pricePerThsDefault["BTC"] = 7750.0;
        pricePerThsDefault["USD"] = 125;
        pricePerThsDefault["EUR"] = 107;
        
        recalcCustomTariffValues(minHosts, $('form[contract-type="lifetime"]'));
        
        function recalcCustomTariffValues(hostsAmount, $form) {
            var $context = $form || $('.j-tariffs-wrapper'),
                contractType = $form.attr('contract-type');
            $context.find('.j-custom-tariff-input').val(hostsAmount * ghsPerHost);
            $context.find('.j-custom-tariff-hosts-amount').val(hostsAmount);
            $context.find('.j-custom-tariff-hosts-amount-span').text(hostsAmount);
            $context.find('.j-custom-tariff-power').val(hostsAmount * ghsPerHost);
            var currency = $context.find('.j-trade-currency').find(":selected").val();
            $context.find('.j-custom-tariff-price').val( formatCurrency(hostsAmount * pricePerThs[contractType][currency] * ghsPerHost / 1000, currency) );
            $.each(pricePerThs[contractType], function(currency, price) {
                var $priceElement = $context.find('.j-custom-tariff-price-span[currency="'+currency+'"]');
                if ($priceElement) {
                    $priceElement.text( formatCurrency(hostsAmount * price * ghsPerHost / 1000, currency) );
        //            $priceElement.text('--.--');
                }
            });
            $context.find('.j-custom-tariff-power-new').val((hostsAmount * ghsPerHost) + ' GH/s');
        
            // big price
            var curr = $context.find('[name="currency"]:checked').val();
            var rub = curr;
            if (curr == 'BTC') rub = 'RUB';
            $context.find('.j-widget-big-price-wrapper-currency').text(rub);
            $context.find('.j-widget-big-price-wrapper-value').text( formatCurrency(hostsAmount * pricePerThs[contractType][curr] * ghsPerHost / 1000, curr) );
        
            $context.find('.j-widget-big-price-wrapper-value-old').text( formatCurrency(hostsAmount * pricePerThsDefault[curr] * ghsPerHost / 1000, curr) );

            //console.log( formatCurrency(hostsAmount * pricePerThs[contractType][curr] * ghsPerHost / 1000, curr) );
            
            $context.find('.month-get').html( '$' +  Number( (formatCurrency(hostsAmount * pricePerThs[contractType][curr] * ghsPerHost / 1000, curr) * 0.05) ).toFixed(2) );
            //$context.find('.j-widget-big-price-wrapper-value').text('--.--');
            //$context.find('.j-widget-big-price-wrapper-value-old').text('--.--');
        }
        
        
        $('input[type=radio][name="currency"]').on('change', function(){
            var value = $('.j-custom-tariff-power').val(),
                hostsAmount = extractAmount(value, $(this).closest('form'));
        
            recalcCustomTariffValues(hostsAmount, $(this).closest('form'));
        });
        
        $('.j-custom-tariff-hosts-amount').on('change', function(){
            var value = $(this).val(),
                hostsAmount = extractAmount(value, $(this).closest('form'));
        
            recalcCustomTariffValues(hostsAmount, $(this).closest('form'));
        });
        
        $('.j-custom-tariff-power').on('change', function(){
            var value = $(this).val(),
                hostsAmount = extractAmount((parseInt(value) || ghsPerHost) / ghsPerHost, $(this).closest('form'));
        
            recalcCustomTariffValues(hostsAmount, $(this).closest('form'));
        });
        
        $('.j-custom-tariff-price').on('change', function(){
            var value = $(this).val(),
                k = pricePerThs[$('.j-trade-currency').find(":selected").val()] * ghsPerHost / 1000,
                hostsAmount = extractAmount((value || k) / k, $(this).closest('form'));
        
            recalcCustomTariffValues(hostsAmount, $(this).closest('form'));
        });
        
        $('.j-trade-currency').on('change', function(){
            recalcCustomTariffValues($('.j-custom-tariff-hosts-amount').val(), $(this).closest('form'));
        });
        
        $('.j-custom-tariff-power-new').on('change', function(){
            var value = $(this).val(),
                hostsAmount = extractAmount((parseInt(value) || ghsPerHost) / ghsPerHost, $(this).closest('form'));
        
            recalcCustomTariffValues(hostsAmount, $(this).closest('form'));
        });
        
        function extractAmount(value, $form) {
            var contractType = $form.attr('contract-type');
            return Math.max(minHosts, Math.min(maxHosts[contractType], (parseInt(value) || 1)))
        }
        
        $('.bc-input-step').on('click', function(){
            var step = parseInt($(this).attr('step')) || 0,
                $input = $(this).closest('.bc-input-wrapper').find('.bc-input');
        
            $input.val(parseFloat($input.val()) + step).change();
        });
        
        $('.tariffs-wrapper [name="currency"]').on('change', function() {
            $(this).closest('form').find('.j-custom-tariff-power-new').change();
        });
        
        $('.j-widget-label-tooltip[data-toggle="tooltip"]').tooltip();
        
        $('.j-new-price-open').on('click', function() { $('.j-new-price-wrapper').addClass('opened'); return false; });
        $('.j-new-price-close').on('click', function() { $('.j-new-price-wrapper').removeClass('opened'); return false; });
    </script>
    <style>
    .modal-backdrop {
      z-index: 0;
    }
    </style>
    <script type="text/javascript">
    	$(".replace-dropdown li a").click(function(){
    		$(this).closest(".replace-dropdown").find("a").first().html($(this).html() + ' <span class="caret"></span>');
    	});
    
    	new WOW().init();
    	$('#loginTabs a').click(function (e) {
    		e.preventDefault();
    		$(this).tab('show')
    	})
    </script>
</body>
</html>